import { CurrencyPipe } from '@angular/common';

export class CurrencyEuroPipe extends CurrencyPipe {
    public transform(value): any {
        return super.transform(value, 'EUR');
    }
}
