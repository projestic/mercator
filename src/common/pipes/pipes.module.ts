import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

//Pipes
import { KeysPipe } from './keys/keys.pipe';
import { FlatRatePipe } from './flat-rate/flat-rate.pipe';
import { RejectedUserPipe } from './rejected-users/rejected-user.pipe';
import { TrimmerPipe } from './trimmer/trimmer.pipe';

@NgModule({
    imports: [],
    declarations: [
        KeysPipe,
        FlatRatePipe,
        RejectedUserPipe,
        TrimmerPipe,
    ],
    exports: [
        KeysPipe,
        FlatRatePipe,
        RejectedUserPipe,
        TrimmerPipe,
    ]
})

export class PipesModule {}
