import { Pipe, PipeTransform, WrappedValue } from '@angular/core';

@Pipe({
  name: 'trimmer'
})
export class TrimmerPipe implements PipeTransform {

  transform(value: string): any {
      if (!value) return '';
      if (value.indexOf(' ') === -1 ) return value;
      value = value.toString();
      value = value.split(' ').filter(item => item !== '').join('');
      return value;
  }
}
