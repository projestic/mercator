import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rejectedUser'
})
export class RejectedUserPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (args == 'Admin Rejected') return `${value} <br> <small class="text-danger">${args}</small>`;
        return value;
    }

}
