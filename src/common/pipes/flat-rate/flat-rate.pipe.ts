import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'flatRate'
})
export class FlatRatePipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (value == args) return 'Flat rate';
        return value;
    }

}
