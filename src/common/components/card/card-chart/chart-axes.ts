export class ChartAxes {
    create(type) {
        let employee;
        switch (type) {
            case 'company_and_employees':
                employee = new CompanyAndEmployees();
                break;
            case 'contracts_created':
                employee = new ContractsCreated();
                break;
            case 'leasing_inquires':
                employee = new LeasingInquires();
                break;
            case 'no_of_orders':
                employee = new NoOfOrders();
                break;
            case 'portal_no_of_orders':
                employee = new PortalNoOfOrders();
                break;
            case 'no_of_suppliers':
                employee = new NoOfSuppliers();
                break;
            case 'no_of_suppliers_company':
                employee = new NoOfSuppliersCompany();
                break;
            case 'products_per_supplier':
                employee = new ProductsPerSupplier();
                break;
            case 'no_of_employees':
                employee = new NoOfEmployees();
                break;
            case 'orders_per_company':
                employee = new NoOfOrdersPerCompany();
                break;
            case 'offers_rejected':
                employee = new OffersRejected();
                break;
            case 'offers_accepted':
                employee = new OffersAccepted();
                break;
            case 'no_of_orders_and_status':
                employee = new OrdersAndStatus();
                break;
            case 'no_of_employee_contracts':
                employee = new EmployeeContracts();
                break;
            default:
                return false;
        }
        employee.type = type;
        return employee;
    }
}

class CompanyAndEmployees {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of company';
        this.yAxes = 'No. of employees';
    }
}

class ContractsCreated {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Weeks';
        this.yAxes = 'No. of contracts';
    }
}

class LeasingInquires {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Weeks';
        this.yAxes = 'No. of Inquiries';
    }
}

class NoOfOrders {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of company';
        this.yAxes = 'No. of orders';
    }
}

class PortalNoOfOrders {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Weeks';
        this.yAxes = 'No. of orders';
    }
}

class NoOfSuppliers {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Weeks';
        this.yAxes = 'No. of suppliers';
    }
}

class NoOfSuppliersCompany {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of company';
        this.yAxes = 'No. of suppliers';
    }
}

class ProductsPerSupplier {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of supplier';
        this.yAxes = 'No. of products';
    }
}

class NoOfEmployees {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of company';
        this.yAxes = 'No. of employees';
    }
}

class NoOfOrdersPerCompany {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of company';
        this.yAxes = 'No. of Order';
    }
}

class OffersRejected {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Weeks';
        this.yAxes = 'No. of Offers';
    }
}

class OffersAccepted {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Weeks';
        this.yAxes = 'No. of Offers';
    }
}

class OrdersAndStatus {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = '';
        this.yAxes = 'No. of Orders';
    }
}

class EmployeeContracts {
    public xAxes;
    public yAxes;
    constructor() {
        this.xAxes = 'Name of Employee';
        this.yAxes = 'No. of Contract';
    }
}
