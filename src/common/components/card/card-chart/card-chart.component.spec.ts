import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPreloadComponent } from './card-chart.component';

describe('CardPreloadComponent', () => {
  let component: CardPreloadComponent;
  let fixture: ComponentFixture<CardPreloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardPreloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPreloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
