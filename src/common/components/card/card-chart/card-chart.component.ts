import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { SettingsService } from '../../../../services/settings/settings.service';
import { ChartAxes } from './chart-axes';

@Component({
  selector: 'mrc-card-chart',
  templateUrl: './card-chart.component.html',
  styleUrls: ['./card-chart.component.scss']
})
export class CardChartComponent implements OnInit, OnChanges {
    @Input() data: Array<any> = [];
    @Input() style: string;
    @Input() source: string;
    @Input() index: number;
    @ViewChild('chart') canvas: ElementRef;
    public primaryColor: string;
    public lineChartData: object[] = [];
    public lineChartLabels: Array<any> = [];
    public lineChartOptions: any = {
        responsive: true,
        maintainAspectRatio: false,
        elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    suggestedMax: 5,
                },
                scaleLabel: {
                    display: true
                }
            }],
            xAxes: [{
                offset: false,
                ticks: {
                    drawTicks: true,
                    drawOnChartArea: false,

                },
                gridLines: {
                    display: false
                },
                scaleLabel: {
                    display: true
                }
            }]
        },
        animation: {
            duration: 1000, // general animation time
        },
    };
    public lineChartColors: Array<any>;
    public lineChartLegend = false;
    public axes;

    constructor(public settingsService: SettingsService) {
    }

    ngOnInit() {
        this.axes = new ChartAxes().create(this.source);
        this.lineChartOptions.scales.yAxes[0].scaleLabel.labelString = this.axes.yAxes;
        this.lineChartOptions.scales.xAxes[0].scaleLabel.labelString = this.axes.xAxes;
        this.primaryColor = this.settingsService.settings.color;
        this.getData(
            (dataArr, labelArr) => {
                this.lineChartOptions.scales.yAxes[0].ticks.suggestedMax = (Math.max(...dataArr) + 1);
                this.lineChartOptions.scales.yAxes[0].ticks.maxTicksLimit = Math.min(11, 1 + Math.max.apply(null, dataArr));
                this.lineChartOptions.scales.xAxes[0].offset = this.style === 'line' ? false : true;
                /*this.lineChartData = [...dataArr];*/
                this.lineChartData = [{
                    data: dataArr,
                    label: this.axes.yAxes
                }];
                this.lineChartLabels = [...labelArr];
            }
        );
        const gradient = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 250);
        gradient.addColorStop(0, this.hexToRgbA(this.primaryColor, .5));
        gradient.addColorStop(1, this.hexToRgbA(this.primaryColor, 0));


        const gradientBar = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
        gradientBar.addColorStop(0, this.primaryColor);
        gradientBar.addColorStop(1, this.hexToRgbA(this.primaryColor, .5));
        this.lineChartColors = [
            { // primary
                backgroundColor: this.style === 'line' ? gradient : gradientBar,
                hoverBackgroundColor: this.primaryColor,
                borderColor: this.primaryColor,
                borderWidth: this.style === 'line' ? 2 : 1,
                pointBackgroundColor: this.primaryColor,
                pointRadius: 3,
                pointBorderWidth: 0,
                pointBorderColor: this.primaryColor,
                pointHoverBackgroundColor: gradient,
                pointHoverBorderColor: this.primaryColor
            }
        ];
    }

    ngOnChanges() {

    }

    getData(callback) {
        const dataArr = [];
        const labelArr = [];
        this.data.forEach((item, index) => {
            dataArr[index] = item.total;
            if (item.hasOwnProperty('week')) {
                labelArr[index] = item.week;
            } else {
                labelArr[index] = item.name || item.status;
            }

        });
        callback(dataArr, labelArr);
    }

    private hexToRgbA(hex: string, alpha: number = 1): string {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('');
            if (c.length === 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',' + alpha + ')';
        }
        throw new Error('Bad Hex');
    }

    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }

}
