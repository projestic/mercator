import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
    selector: 'mrc-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
    @Input() title;
    @Input() widgetTitles;
    @Input() item: any;
    @Input() preloader: boolean = false;
    @Input() menuOff = false;
    @Output() deleteWidgetTrigger = new EventEmitter<string>();
    @Output() updateWidgetTrigger = new EventEmitter<object>();
    public showSource = false;
    public showStyle = false;
    public sourceArray = [];
    public sourceValue = '';

    constructor() {
    }

    ngOnInit() {

    }

    ngOnChanges() {
        this.sourceArray = this.getSourceArray(this.widgetTitles);
        this.sourceValue = this.item ? this.item.source : '';
    }

    getSourceArray(data: object): Array<any> {
        const keys = data ? Object.keys(data) : [];
        return keys.map(item => {
            return {
                value: item,
                label: data[item]
            };
        });
    }

    deleteWidget(event) {
        event.preventDefault();
        this.deleteWidgetTrigger.emit(this.item.id);
    }

    changeSource(event) {
        event.preventDefault();
        this.showSource = true;
        this.showStyle = false;
    }

    changeStyle(event) {
        event.preventDefault();
        this.showSource = false;
        this.showStyle = true;
    }

    onChangeSource(data) {
        this.showSource = false;
        this.updateWidgetTrigger.emit({
            id: this.item.id,
            position: this.item.position,
            source: data.value,
            style: this.item.style
        });
    }

    onChangeStyle(event, data) {
        event.preventDefault();
        this.showStyle = false;
        this.updateWidgetTrigger.emit({
            id: this.item.id,
            position: this.item.position,
            source: this.item.source,
            style: data
        });
    }

    onClickOutside(event, id) {
        if (event && event.target.className !== 'dropdown-item') {
            this.showSource = false;
            this.showStyle = false;
        }
    }
}
