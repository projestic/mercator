import {Component, Input, OnChanges} from '@angular/core';

@Component({
    selector: 'mrc-card-list',
    templateUrl: './card-list.component.html',
    styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnChanges {
    @Input() dataTable = [];
    public dataHead: Array<string> = [];

    constructor() {
    }

    ngOnChanges() {
        this.dataHead = this.dataTable && this.dataTable[0] ? Object.keys(this.dataTable[0]) : [];
    }

}
