import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'mrc-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit, OnChanges {

    @Input() statusCode: string;
    @Input() statusIcons: boolean;

    public status;
    public icoClass: string;

    constructor() { }

    ngOnInit() {

    }

    ngOnChanges() {
        let status;

        switch (this.statusCode) {
            case 'Aktiv':
                status = new ActiveStatus(this.statusCode);
                break;

            case 'Abgelaufen':
                status = new ExpiredStatus(this.statusCode);
                break;

            case 'Inaktive':
                status = new InactiveStatus(this.statusCode);
                break;

            case 'In Bearbeitung':
                status = new ProgressStatus(this.statusCode);
                break;

            case 'Erfolgreich':
                status = new SuccessStatus(this.statusCode);
                break;

            case 'Angefragt':
                status = new RequestedStatus(this.statusCode);
                break;

            case 'Abgebrochen':
                status = new CanceledStatus(this.statusCode);
                break;

            case 'Angebot Erhalten':
                status = new OfferReceived(this.statusCode);
                break;

            case 'Akzeptiert':
                status = new Akzeptiert(this.statusCode);
                break;

            case 'Abgelehnt':
                status = new Abgelehnt(this.statusCode);
                break;

            case 'Bereit':
                status = new DoneStatus(this.statusCode);
                break;

            case 'Offen':
                status = new OfferReceived(this.statusCode);
                break;

            default:
                status = new DefaultStatus(this.statusCode);
        }

        this.status = status;

        this.icoClass = this.statusIcons ? 'big' : '';
    }
}

class DefaultStatus {
    public code: string;
    public ico: string;
    public label: string = 'Unbekannt';
    public type: string;

    constructor(code: string) {
        this.label = code;
    }
}

class ActiveStatus extends DefaultStatus {

    constructor(code) {
        super(code);

        this.type = 'success';
    }
}

class ExpiredStatus extends DefaultStatus {

    constructor(code) {
        super(code);

        this.type = 'danger';
    }
}

class InactiveStatus extends DefaultStatus {

    constructor(code) {
        super(code);

        this.type = 'default';
    }
}

class ProgressStatus extends DefaultStatus {

    constructor(code) {
        super(code);

        this.type = 'progress';
    }
}

class RequestedStatus extends DefaultStatus {

    constructor(code) {
        super(code);

        this.type = 'warning';
    }
}

class SuccessStatus extends DefaultStatus {

    constructor(code) {
        super(code);

        this.ico = 'done';
        this.type = 'success';
    }
}

class CanceledStatus extends DefaultStatus {
    constructor(code) {
        super(code);

        this.type = 'cancel';
    }
}

class OfferReceived extends DefaultStatus {
    constructor(code) {
        super(code);

        this.ico = 'hourglass_empty';
        this.type = 'warning';
    }
}

class Abgelehnt extends DefaultStatus {
    constructor(code) {
        super(code);

        this.ico = 'block';
        this.type = 'danger';
    }
}

class Akzeptiert extends DefaultStatus {
    constructor(code) {
        super(code);

        this.ico = 'done';
        this.type = 'success';
    }
}

class DoneStatus extends DefaultStatus {
    constructor(code) {
        super(code);

        this.ico = 'store';
        this.type = 'progress';
    }
}
