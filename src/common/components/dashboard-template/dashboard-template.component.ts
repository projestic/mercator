import { Component, ChangeDetectorRef, OnDestroy, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ToggleSidebarService } from '../../../services/toggle-sidebar/toggle-sidebar.service';
import { Subscription } from 'rxjs/Rx';
import { SettingsService } from '../../../services/settings/settings.service';
import { Router, NavigationEnd } from '@angular/router';
import { ProfileService } from '../../../services/profile/profile.service';
import { Profile } from '../../models/profile';
import { ProfileInterface } from '../../models/profile.interface';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard-template.component.html',
    styleUrls: ['./dashboard-template.component.scss'],
    providers: [ToggleSidebarService]
})

export class DashboardTemplateComponent implements OnInit, OnDestroy, AfterViewInit {
    user: ProfileInterface = new Profile();
    xxlQuery: MediaQueryList;
    sidebarSubscription: Subscription;
    profileSubscription: Subscription;
    data: any;
    private _xxlQueryListener: () => void;

    @ViewChild('sidebarNav') public sidebarNav: HTMLElement;

    constructor(changeDetectorRef: ChangeDetectorRef,
                media: MediaMatcher,
                public toggleSidebar: ToggleSidebarService,
                public settingsService: SettingsService,
                private profileService: ProfileService,
                private router: Router) {
        this.xxlQuery = media.matchMedia('(min-width: 1680px)');
        this._xxlQueryListener = () => changeDetectorRef.detectChanges();
        this.xxlQuery.addListener(this._xxlQueryListener);

        // sidebar event
        this.sidebarSubscription = this.toggleSidebar.getSidenav().subscribe(data => {
            this.data = data;
        });

        // get user profile data
        this.profileService.getProfile();

        this.profileSubscription = this.profileService.getProfileObj().subscribe(
            res => {
                this.user = new Profile(res);
            }
        );
    }

    ngOnInit() {
        this.toggleSidebar.updateSidenav(this.sidebarNav);
        this.settingsService.getSettings();

        // close menu after click by link
        this.router.events.subscribe((val) => {
            if ( (val instanceof NavigationEnd) && !this.xxlQuery.matches) {
                this.toggleSidebar.sidenav.close();
            }
        });
    }

    ngAfterViewInit() {
        this.toggleSidebar.updateSidenav(this.sidebarNav);
    }

    ngOnDestroy(): void {
        this.xxlQuery.removeListener(this._xxlQueryListener);
        this.sidebarSubscription.unsubscribe();
        this.profileSubscription.unsubscribe();
    }
}
