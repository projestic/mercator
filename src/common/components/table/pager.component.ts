import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Page } from './models/page';

@Component({
    selector: 'mrc-pager',
    templateUrl: './pager.component.html',
    styleUrls: ['./pager.component.scss']
})

export class PagerComponent implements OnInit {
    @Output() changeSize = new EventEmitter<number | string>();

    constructor() { }

    ngOnInit() {
    }

    //pager
    public pager: Array<string | number> = [
        5,
        10,
        20,
        50,
        'All'
    ];

    //page size
    public pageSize: number | string = 20;

    setPageSize(): void {
        let size;

        if (this.pageSize === 'All') {
            size = 999999;
        } else {
            size = this.pageSize;
        }

        this.changeSize.emit(size);
    }
}
