import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpConfigService } from '../../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';

@Component({
    selector: 'mrc-table-actions',
    templateUrl: './table-actions.component.html',
    styleUrls: ['./table-actions.component.scss']
})
export class TableActionsComponent implements OnInit {
    @Input() id: string | number;

    @Output() refreshTable: EventEmitter<any> = new EventEmitter();

    constructor(
        private http: HttpConfigService,
        private toasterService: ToasterService) {
    }

    ngOnInit() {
    }

    public userAction(id, path) {
        this.http.postRequest(`users/${id}/${path}`, {})
            .subscribe(
                res => {
                    this.refreshTable.emit();
                },
                error => {
                    console.log(error);
                }
            );
    }

}
