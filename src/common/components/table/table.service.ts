import { Injectable } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import fileSaver from 'file-saver';
import { ToasterService } from 'angular5-toaster';
import { map } from 'rxjs/operators';
import {ReplaySubject} from 'rxjs/index';

@Injectable()
export class TableService extends HttpConfigService {

    public params = new HttpParams()
        .set('page', '1')
        .set('per_page', '20');

    public columns;

    public tableColumns = new ReplaySubject<Array<any>>();

    public updateColumns(value) {
        this.columns = value;
        this.tableColumns.next(value);
    }

    reset():void {

        let params = new HttpParams().set('page', '1').set('per_page', '20');

        this.params = params;
    }

    getData(route: string) {

        let headers = new HttpHeaders();
        let params = this.params;

        const func = this.http.get(route, {
            headers,  params
        }).pipe(map(
            (res: any) => {
                const result = res.payload;
                return result;
            }
            )
        )

        return this.httpRequestStrategy(func);
    }

    export(route: string, filename?: string) {

        const url = `${route}/export`;

        const options = {
            responseType: 'arraybuffer',
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8',
            },
            observe: 'response'
        };

        const type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

        this.getFileRequest(url, options, type).subscribe(res => {
            filename = `${filename}.${res.headers.get('content-disposition').match(/filename=(.+)/)[1].slice(0, -1).split('.')[1]}`;
            fileSaver.saveAs(res.body, filename);
        });
    }
}
