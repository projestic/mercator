import {
    Component,
    OnInit,
    Input,
    Output,
    ViewChild,
    TemplateRef,
    OnChanges,
    EventEmitter,
    ChangeDetectorRef,
    OnDestroy
} from '@angular/core';
import { Page } from './models/page';
import { map, uniq, forEach, filter } from 'lodash';
import { TableService } from './table.service';
import { ToasterContainerComponent, ToasterService, ToasterConfig } from 'angular5-toaster';
import { FilterInterface } from './models/filter.interface';
import { MediaMatcher } from '@angular/cdk/layout';
import { ConstantsService } from '../../../services/constants/constants.service';
import { MatTabChangeEvent } from '@angular/material';
import { NavigationEnd, Router } from '@angular/router';
import { SearchService } from '../../../services/search/search.service';
import { Subscription } from 'rxjs/index';
import { AuthSettingsService } from '../../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
    providers: [
        ConstantsService
    ]
})
export class TableComponent implements OnInit, OnChanges, OnDestroy {

    @ViewChild('status') status: TemplateRef<any>;
    @ViewChild('remove') remove: TemplateRef<any>;
    @ViewChild('view') view: TemplateRef<any>;
    @ViewChild('cell') cell: TemplateRef<any>;
    @ViewChild('name') name: TemplateRef<any>;
    @ViewChild('row') row: TemplateRef<any>;
    @ViewChild('action') action: TemplateRef<any>;
    @ViewChild('loginAs') loginAs: TemplateRef<any>;

    @Input() columns: Array<TableCol>;
    @Input() pager: boolean = true;
    @Input() rows: Array<any>;
    @Input() statuses: Array<string> = [];
    @Input() actions;
    @Input() route: string;
    @Input() removeItemCol: boolean;
    @Input() filterItems: Array<any>;
    @Input() category: string;
    @Input() search: string;
    @Input() statusIcons: boolean = false;

    @Output() currentStatus = new EventEmitter<object>();

    public xlQuery: MediaQueryList;
    private _xlQueryListener: (e) => void;

    public mobileQuery: MediaQueryList;
    public url: string;
    private tableColumns: object;
    private tableColSubscr: Subscription;

    constructor(
        private constantsService: ConstantsService,
        private tableService: TableService,
        private media: MediaMatcher,
        private router: Router,
        public searchService: SearchService,
        private authSettings: AuthSettingsService) {

        this.tableColSubscr = this.tableService.tableColumns.subscribe(
            res => {
                this.tableColumns = res;
            }
        );
        this.url = `${this.authSettings.path}/`;

        window.onafterprint = () => {
            this.compileCols();
            this.updateTable();
        };
    }

    public sortFilterParams = {};
    public tableRows: Array<any> = [];
    public tableCols: Array<any> = [];

    public cols: Array<any> = [];

    public selectedStatus: string = 'Alle';
    public selectedStatusIndex: number;

    public loading: boolean = false;

    public categories: Array<any> = [];

    ngOnInit() {

    }

    ngOnChanges() {
        this.initTable();
    }

    initTable() {
        this.tableService.reset();

        this.compileCols();
        this.xlQuery = this.media.matchMedia('(min-width: 992px)');

        this._xlQueryListener = (e) => {
            this.compileCols();
        };

        this.xlQuery.addListener(this._xlQueryListener);

        this.mobileQuery = this.media.matchMedia('(max-width: 767px)');

        this.page.size = this.tableService.params.get('per_page');

        if (this.statuses && this.statuses[0]) {
            this.statuses.push('Alle');

            let params = this.tableService.params.set('status', this.statuses[0]);
            this.selectedStatusIndex = 0;
            this.selectedStatus = this.statuses[0];

            this.tableService.params = params;
        }

        if (this.search) {
            let params = this.tableService.params.set('search', this.search).set('page', '1');

            this.tableService.params = params;
        }

        if (this.category) {

            if (this.searchService.searchCategory) {
                this.category = this.searchService.searchCategory;
            }

            let params = this.tableService.params.set('category', this.category).set('full', '1');

            this.tableService.params = params;
        }

        this.tableRows = [];

        this.updateTable();
    }

    ngOnDestroy() {
        this.tableColSubscr.unsubscribe();
    }

    private compileCols(): void {
        this.columns = this.tableColumns[this.category] || [];
        forEach(this.columns, (col, i) => {
            switch (col.prop) {
                case 'name':
                    col.cellTemplate = this.name;
                    break;
                case 'status':
                    col.cellTemplate = this.status;
                    break;
                case 'remove':
                    col.cellTemplate = this.remove;
                    break;
                case 'actions':
                    col.cellTemplate = this.action;
                    break;
                case 'loginAs':
                    col.cellTemplate = this.loginAs;
                    break;
                default:
                    col.cellTemplate = this.cell;
            }
        });

        let cols: Array<TableCol> = [];

        if (this.media.matchMedia('(min-width: 992px)').matches) {
            cols = this.columns;
        } else {
            cols = this.columns.filter(col => !col.hideOnTablet);

            cols.push({
                cellClass: 'justify-content-center justify-content-md-end cell-view',
                cellTemplate: this.view,
                flexGrow: 5,
                name: ' ',
                prop: 'view',
                sortable: false,
                minWidth: 70,
                maxWidth: 70
            });
        }

        if (!this.search && this.selectedStatus !== 'Alle') {
            cols = cols.filter(col => col.prop !== 'status');
        }

        if (this.category === 'orders') {
            if (!this.search && this.selectedStatus !== 'Alle') {
                cols = cols.filter(col => col.prop !== 'pickup_code');
            } else {
                cols = cols.filter(col => col.prop !== 'agreed_purchase_price');
                cols = cols.filter(col => col.prop !== 'leasing_rate');
            }
        }

        if (this.category === 'users') {
            if (this.selectedStatus !== 'Offen') {
                cols = cols.filter(col => col.prop !== 'actions');
            } else {
                cols = cols.filter(col => col.prop !== 'role');
            }

            if (this.selectedStatus !== 'Aktiv') {
                cols = cols.filter(col => col.prop !== 'loginAs');
            }
        }


        if (this.selectedStatus !== 'Inaktiv') {
            cols = cols.filter(col => col.prop !== 'remove');
        }

        this.tableCols = cols;
    }

    public page: Page = new Page();

    setPage(pageInfo) {

        this.page.pageNumber = pageInfo.offset;
        let params = this.tableService.params.set('page', pageInfo.offset + 1);

        this.tableService.params = params;

        this.updateTable();
    }

    changeStatus(e): void {

        let status = e.tab.textLabel;

        this.selectedStatus = status;

        if (status === 'Alle') {

            let params = this.tableService.params.delete('status').set('page', '1');

            this.tableService.params = params;
        } else {

            let params = this.tableService.params.set('status', status).set('page', '1');

            this.tableService.params = params;
        }

        this.updateTable();
    }

    selectStatus(): void {
        this.selectedStatusIndex = this.statuses.indexOf(this.selectedStatus);
    }


    //Cnange category
    changeCategory(e): void {

        if (e instanceof MatTabChangeEvent) {
            this.category = this.categories[e.index].slug;
        }

        this.searchService.searchCategory = this.category;

        //update params
        let params = this.tableService.params
            .set('category', this.category) //set category
            //reset filters
            .set('page', '1')
            .delete('order')
            .delete('order_by')
            .delete('search[value]')
            .delete('search[column]')
            .delete('filter[value]')
            .delete('filter[column]');

        this.tableService.params = params;

        this.updateTable();
    }

    sort(event) {
        // event was triggered, start sort sequence

        let params = this.tableService.params.set('order_by', event.sorts[0].prop).set('order', event.sorts[0].dir).set('page', '1');

        this.tableService.params = params;

        this.updateTable();
    }

    onActivate(e) {
        switch (e.type) {
            case 'click':
                if (e.column.prop === 'remove' || e.column.prop === 'actions' || e.column.prop === 'loginAs') return;
                if (this.category) {

                    switch (this.category) {
                        case 'suppliers':
                            this.router.navigate([`${this.url}admin/supplier`, e.row.id]);
                            break;

                        case 'users':
                            if (this.authSettings.currentModule === 'company') {
                                break;
                            }
                            if (e.row.type) this.router.navigate([`${this.url}admin/user`, e.row.id, e.row.type, 'edit']);
                            break;

                        case 'contracts':
                            switch (this.authSettings.currentModule) {

                                case 'employee':
                                    this.router.navigate([`${this.url}/contract`, e.row.id, e.row.number]);
                                    break;

                                default:
                                    this.router.navigate([`${this.url}admin/contract`, e.row.id]);
                                    break;
                            }

                            break;

                        case 'orders':
                            switch (this.authSettings.currentModule) {
                                case 'supplier':
                                    this.router.navigate([`${this.url}admin/order`, e.row.id, e.row.number]);
                                    break;

                                case 'employee':
                                    this.router.navigate([`${this.url}/order`, e.row.id, e.row.number]);
                                    break;

                                default:
                                    this.router.navigate([`${this.url}admin/order`, e.row.id]);
                                    break;
                            }

                            break;

                        case 'portals':
                            this.router.navigate([`${this.url}admin/portal`, e.row.id, 'edit']);
                            break;

                        case 'companies':
                            this.router.navigate([`${this.url}admin/company`, e.row.id, 'edit']);
                            break;

                        case 'offers':

                            switch (this.authSettings.currentModule) {
                                case 'supplier':
                                    this.router.navigate([`${this.url}admin/offer`, e.row.id, e.row.number]);
                                    break;

                                case 'employee':
                                    this.router.navigate([`${this.url}/offer`, e.row.id, e.row.number]);
                                    break;

                                default:
                                    this.router.navigate([`${this.url}admin/offer`, e.row.id]);
                                    break;
                            }

                            break;

                        default:
                            console.log('Unknown type');
                    }

                } else {
                    this.actions && this.actions.click && this.actions.click(e);
                }

                break;
        }

    }

    changeSize(size: number) {

        let params = this.tableService.params.set('per_page', '' + size).set('page', '1');

        this.tableService.params = params;
        this.page.size = size;
        this.page.pageNumber = 0;

        this.updateTable();
    }

    private setLoading(state: boolean): void {
        this.loading = state;
    }

    private updateTable(): void {
        this.setLoading(true);

        this.tableService.getData(this.route).subscribe(res => {
            this.compileCols();

            let meta, data;
            if (res.categories) {
                meta = res.categories[this.category].meta;
                data = res.categories[this.category].data;
                this.categories = this.createCategoriesList(res.categories);
            } else {
                meta = res.meta;
                data = res.data;
            }

            const params = this.tableService.params.set('full', '0');
            this.tableService.params = params;

            this.tableRows = data;

            this.page.totalElements = meta.total;
            this.page.totalPages = meta.total_pages;
            this.page.pageNumber = meta.current_page - 1;

            //call recalculate table sizes
            setTimeout(function () {
                window.dispatchEvent(new Event('resize'));
            }, 300);

            this.setLoading(false);
        }, error => {
            this.setLoading(false);
        });
    }

    private createCategoriesList(items): Array<TableCategory> {
        const categories: any = [];
        for (let key of Object.keys(items)) {

            if (typeof items[key] === 'object') {

                items[key].meta.total && categories.unshift({
                    slug: key,
                    label: this.constantsService.categories[key],
                    labelDropdown: `${this.constantsService.categories[key]} (${items[key].meta.total})`,
                    count: items[key].meta.total
                });

            } else {

                items[key] && categories.push({
                    slug: key,
                    label: this.constantsService.categories[key],
                    labelDropdown: `${this.constantsService.categories[key]} (${items[key]})`,
                    count: items[key]
                });
            }
        }

        return categories;
    }

    public submitFilter(filter: FilterInterface): void {

        if (filter.type === 'status') {
            this.selectedStatus = filter.value;
            this.selectedStatusIndex = this.statuses.indexOf(this.selectedStatus);

            if (filter.value === 'Alle') {

                const params = this.tableService.params.delete('status').set('page', '1');

                this.tableService.params = params;
            } else {

                const params = this.tableService.params.set('status', filter.value).set('page', '1');

                this.tableService.params = params;
            }
        } else {
            const params = this.tableService.params.set('filter[column]', filter.column).set('filter[value]', filter.value);
            this.tableService.params = params;
        }

        this.updateTable();
    }

    public resetFilter(): void {
        this.tableService.params = this.tableService.params.delete('filter[column]').delete('filter[value]');
        if (filter.type === 'status') {
            this.selectedStatus = null;
            this.selectedStatusIndex = this.statuses.indexOf(this.selectedStatus);
            let params = this.tableService.params.delete('status').set('page', '1');
            this.tableService.params = params;
        }
        this.updateTable();
    }

    public print(v) {
        console.log(v);
    }
}

interface TableCol {
    cellClass?: string;
    cellTemplate?: TemplateRef<any>;
    flexGrow?: number;
    hideOnTablet?: boolean;
    maxWidth?: number;
    minWidth?: number;
    name: string;
    prop: string;
    sortable?: boolean;
    width?: number;
}

interface TableCategory {
    slug?: string;
    label?: string;
    count?: number;
}
