import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FilterInterface } from './models/filter.interface';
import { DatePipe } from '@angular/common';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
    selector: 'mrc-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
    providers: [ DatePipe, MediaMatcher]
})
export class FilterComponent implements OnInit {
    @Input() statuses: Array<string> = [];
    @Input() filterItems: Array<any> = [];
    @Output() submitFilter = new EventEmitter<FilterInterface>();
    @Output() resetFilter = new EventEmitter<object>();
    @ViewChild('filterDropdown') public filterDropdown;
    public filter: FilterInterface;
    public mobileQuery: MediaQueryList;

    constructor(private datePipe: DatePipe,
                public media: MediaMatcher) {}

    public defaulltStatuses = [
        'Aktiv',
        'Inaktiv',
        'Alle'
    ];

    public roles = [];

    public currentStatuses: Array<string>;

    ngOnInit() {
        this.filter = new Filter({
            column: this.filterItems[0].value,
            value: '',
            type: ''
        });
    }

    public submit(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;

        let { value } = this.filter;

        let data: FilterInterface;

        if (value instanceof Array) {
            const valueArray = [];
            value.forEach(item => {
                valueArray.push(new Date(item.setHours(-new Date().getTimezoneOffset() / 60)).getTime() / 1000);
            });
            data = {
                column: this.filter.column,
                value: valueArray.join('|')
            };
        } else if (value instanceof Date) {
            const date = new Date(value.setHours(-new Date().getTimezoneOffset() / 60)).getTime() / 1000;
            data = {
                column: this.filter.column,
                value: date
            };
        } else {
            data = {...this.filter};
        }
        this.submitFilter.emit(data);
        this.filterDropdown.hide();
    }

    public reset(form: NgForm) {
        this.resetFilter.emit();
        this.filter = new Filter({
            column: this.filterItems[0].value,
            value: '',
            type: ''
        });
        form.resetForm({
            column: this.filterItems[0].value,
            value: '',
            type: ''
        });
    }

    public preventClose(event: MouseEvent) {
        event.stopImmediatePropagation();
    }

    public onChange(data) {
        this.filterItems.forEach(item => {
            if (this.filter.column === item.value) this.filter.type = item.type;
            if (item.type === 'role') {
                this.roles = item.options;
                this.filter.value = item.options[0];
            }
        });
        if (this.filter.type === 'status') {
            this.filter.value = 'Alle';
        } else {
            this.filter.value = null;
        }

        this.currentStatuses = this.statuses.length === 0 ? this.defaulltStatuses : this.statuses;
    }

    replaceDatepicker(value) {
        const parent = document.getElementById('datepicker-wrapper');
        parent.appendChild(document.querySelector('bs-daterangepicker-container'));
    }
}

class Filter implements FilterInterface {
    public column: string;
    public value: any;
    public type: string;

    constructor(filter?: FilterInterface) {
        if (filter) {
            this.column = filter.column;
            this.value = filter.value;
            this.type = filter.type;
        }
    }
}
