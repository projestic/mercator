export interface FilterInterface {
    column: string;
    value: any;
    type?: string;
}
