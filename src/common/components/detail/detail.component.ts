import { Component, OnInit, Input } from '@angular/core';
import { ContractInterface } from '../../models/contract.interface';
import { LeasingInterface } from '../../models/leasing.interface';
import { OrderInterface } from '../../models/order.interface';
import { ProductInterface } from '../../models/product.interface';

@Component({
    selector: 'mrc-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
    @Input() contract: ContractInterface;
    @Input() leasing: LeasingInterface;
    @Input() order: OrderInterface;
    @Input() product: ProductInterface;

    constructor() {
    }

    ngOnInit() {
    }

}
