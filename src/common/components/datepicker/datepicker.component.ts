import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'mrc-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.scss'],
    providers: [DatePipe]
})
export class DatepickerComponent implements OnInit {
    @Input() titleDatepicker: string;
    bsRangeValue: Date[];
    datePickerValue = '';
    currentDate = new Date();
    minDate: Date;
    maxDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate());
    @Output() resDates = new EventEmitter<any>();

    constructor(private datePipe: DatePipe) {
    }

    ngOnInit() {
    }

    datePickerChange(value: Date) {
        if (value) {
            const from = new Date(value[0].getTime() - new Date().getTimezoneOffset() * 60 * 1000);
            const to = new Date(value[1].getTime() - new Date().getTimezoneOffset() * 60 * 1000);
            const fromMonth = this.datePipe.transform(from, 'LLL');
            const toMonth = this.datePipe.transform(to, 'LLL');
            const fromYear = this.datePipe.transform(from, 'yyyy');
            const toYear = this.datePipe.transform(to, 'yyyy');
            const currentYear = new Date().getUTCFullYear().toString();
            const year = fromYear === toYear && fromYear === currentYear ? '' :
                ` (${fromYear}${fromYear === toYear ? '' : ` - ${toYear}`})`;
            this.datePickerValue =
                `${fromMonth} ${from.getDate()} ${from.getDate() !== to.getDate() ? '-' : ''} ${fromMonth === toMonth ?
                    '' : toMonth + ' '}${from.getDate() !== to.getDate() ? to.getDate() : ''}${year}`;
            this.resDates.emit([from, to]);
        }
    }

    replaceDatepicker(value) {
        const parent = document.getElementById('datepicker');
        parent.appendChild(document.querySelector('bs-daterangepicker-container'));
    }
}
