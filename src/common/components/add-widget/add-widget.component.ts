import { Component, OnChanges, Input, TemplateRef, Output, EventEmitter, ViewChild } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular5-toaster';

@Component({
    selector: 'mrc-add-widget',
    templateUrl: './add-widget.component.html',
    styleUrls: ['./add-widget.component.scss']
})
export class AddWidgetComponent implements OnChanges {
    @Input() sourceArr: Array<any> = [];
    @Input() widgetsTotal: number;
    @Output() addWidgetTrigger = new EventEmitter();
    @ViewChild('errorWidgetSource') errorWidgetSource: TemplateRef<any>;
    modalRef: BsModalRef;
    errorMessage: string;
    public newWidget;
    constructor(private httpService: HttpConfigService,
                private modalService: BsModalService,
                private toasterService: ToasterService) {
    }

    ngOnChanges() {
        this.newWidget = new Widget({
            style: 'line',
            position: this.widgetsTotal + 1
        });
    }

    public openModal(template: TemplateRef<any>) {
        const size = this.widgetsTotal < 3 ? 'modal-md' : 'modal-xs';
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: size })
        );
    }

    public setStyle(form: NgForm, template: TemplateRef<any>): void {
        this.openModal(template);
    }

    public addwidget(form: NgForm, template: TemplateRef<any>): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;

        this.httpService.postRequest('widgets', this.newWidget)
            .subscribe(
                res => {
                    let title;
                    this.sourceArr.forEach(item => {
                        if (item.value === res.payload.source) {
                            title = item.label;
                        }
                    });
                    this.modalRef.hide();
                    this.toasterService.pop('success', '', `'${title}' widget was added successfully!`)
                    this.addWidgetTrigger.emit();
                },
                error => {
                    form.resetForm();
                    this.modalRef.hide();
                    this.errorMessage = error.source;
                    this.modalRef = this.modalService.show(
                        this.errorWidgetSource,
                        Object.assign({}, { class: 'modal-xs' })
                    );
                }
            );
    }

    public openErrorModal(error) {
        this.errorMessage = error;
        this.modalRef = this.modalService.show(
            this.errorWidgetSource,
            Object.assign({}, { class: 'modal-xs' })
        );
    }

}

class Widget {
    public source: string;
    public position: number;
    public style: string = 'line';

    constructor(data?) {
        if (data) {
            this.source = data.source;
            this.position = data.position;
            this.style = data.style;
        }
    }
}
