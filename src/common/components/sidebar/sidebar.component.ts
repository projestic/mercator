import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings/settings.service';
import { ProfileService } from '../../../services/profile/profile.service';
import { ProfileInterface } from '../../models/profile.interface';
import { Profile } from '../../models/profile';
import { Subscription } from 'rxjs';
import { SidebarSettingsService } from './sidebar-settings.service';
import { AuthSettingsService } from '../../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit, OnDestroy {
    user: ProfileInterface = new Profile();
    public logo;
    public logoDefault: string;
    public accountLink: string;
    private profileSubscriber: Subscription;
    private sidebarSettingsSubscribtion: Subscription;

    constructor(public profileService: ProfileService,
                public settingsService: SettingsService,
                public authService: AuthSettingsService,
                private sidebarSettings: SidebarSettingsService) {
        this.sidebarSettingsSubscribtion =
            this.sidebarSettings.logoSettings.subscribe(
            (res: {image: string, link: string}) => {
                this.logoDefault = res.image;
                this.accountLink = res.link;
            }
        );
        this.settingsService.settingObj.subscribe(
            res => {
                this.logo = null;
                setTimeout(() => {
                    this.logo = res.logo;
                }, 250);
            }
        );

        this.profileSubscriber = this.profileService.getProfileObj().subscribe(
            res => {
                this.user = new Profile(res);
            }
        );
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.profileSubscriber.unsubscribe();
        this.sidebarSettingsSubscribtion.unsubscribe();
    }
}
