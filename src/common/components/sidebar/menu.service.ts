import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/Rx';

@Injectable()
export class MenuService {
    public menuSubject = new ReplaySubject<Array<any>>();

    public updateMenu(value) {
        this.menuSubject.next(value);
    }
}
