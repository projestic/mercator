import {Component, OnInit } from '@angular/core';
import { Profile } from '../../../models/profile';
import { MenuService } from '../menu.service';
import { Router } from '@angular/router';

@Component({
    selector: 'mrc-sidebar-menu',
    templateUrl: './sidebar-menu.component.html',
    styleUrls: ['./sidebar-menu.component.scss']
})
export class SidebarMenuComponent implements OnInit {
    menuArray: Array<object> = [];
    public user: Profile;

    constructor(private menuService: MenuService,
                private router: Router) {
        this.menuService.menuSubject.subscribe(res => {
            this.menuArray = res;
        });
    }

    ngOnInit() {}

    fireEvent(name: string, link): void {
        const event = new Event(name);
        document.dispatchEvent(event);
        if (this.router.url.indexOf(link) !== -1) {
            this.router.routeReuseStrategy.shouldReuseRoute = () => {
                return false;
            };
            this.router.navigated = false;
            this.router.navigate([this.router.url]);
        }
    }
}
