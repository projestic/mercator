import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    public footerTitle = '2018 © Mercator Leasing. All right reserved.';
    constructor() {
    }

    ngOnInit() {
    }

}
