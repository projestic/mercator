import { Component, Input, OnInit, DoCheck} from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'mrc-validation-errors',
  templateUrl: './validation-errors.component.html',
  styleUrls: ['./validation-errors.component.scss'],
  host: {
      '[class]' : 'className' 
  }
})
export class ValidationErrorsComponent implements OnInit, DoCheck {

    @Input() control: FormControl;

    @Input() direction: Array<string> = ['lg-right'];

    classList: Array<string>;
    className: string;
    errors: ValidationErrors;

    public getErrorMessage(error: Error): string {

        if (typeof error.value === 'boolean') {
            return `Invalid ${error.key}`;
        }

        return error.value;
    }

    ngOnInit() {
        
    }

    ngDoCheck() {
        this.classList = ['fade form-tooltip'];

        if (this.control) {
            this.errors = this.control.errors;

            if (this.errors && this.control.invalid && this.control.dirty) {
                this.classList.push('show');
            }

            this.direction && this.direction.forEach( dir => this.classList.push('form-tooltip-' + dir) );
            this.className = this.classList.join(' ');
        }
    }
}

interface Error {
    key: boolean | string;
    value: string;
}
