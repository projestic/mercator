import { Component, Input, OnInit } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';

@Component({
    selector: 'mrc-login-as-btn',
    templateUrl: './login-as-btn.component.html',
    styleUrls: ['./login-as-btn.component.scss']
})
export class LoginAsBtnComponent implements OnInit {
    @Input() userId: string | number;

    constructor(private http: HttpConfigService) {
    }

    ngOnInit() {
    }


    loginAs() {
        this.http.postRequest(`portal-users/${this.userId}/login-as`)
            .subscribe(
                res => {
                    window.open(`${res.payload.redirect_to}?access_token=${res.payload.token.access_token}&expire=${res.payload.token.expires_in}`, '_blank');
                }
            );
    }

}
