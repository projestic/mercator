import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginAsBtnComponent } from './login-as-btn.component';

describe('LoginAsBtnComponent', () => {
  let component: LoginAsBtnComponent;
  let fixture: ComponentFixture<LoginAsBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginAsBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAsBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
