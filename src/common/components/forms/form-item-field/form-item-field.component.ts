import {Component, Input, forwardRef, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'mrc-form-item-field',
    templateUrl: './form-item-field.component.html',
    styleUrls: ['./form-item-field.component.scss']
})
export class FormItemFieldComponent implements OnInit {
    @Input() control: FormControl;
    @Input() key: string;
    @Input() label: string;
    @Input() id: string;
    @Input() layout: string;
    @Input() direction: Array<string> = ['lg-right'];
    @Input() tooltipText: string;

    ngOnInit() {
        
    }
}
