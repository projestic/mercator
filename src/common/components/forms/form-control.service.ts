import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { FormItemBase } from './form-item-base';

@Injectable()
export class FormControlService {
    constructor() { }

    toFormGroup(questions: FormItemBase<any>[] ) {
        let group: any = {};

        questions.forEach(question => {
            group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
                : new FormControl(question.value || '');
        });
        return new FormGroup(group);
    }
}
