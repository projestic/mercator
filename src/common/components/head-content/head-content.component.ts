import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'mrc-head-content',
  templateUrl: './head-content.component.html',
  styleUrls: ['./head-content.component.scss']
})
export class HeadContentComponent implements OnInit {
    @Input() title: string;
    @Input() subtitle: string;
    @Input() link: string;
    @Input() titleId: string;

    constructor() { }

    ngOnInit() {

    }

}
