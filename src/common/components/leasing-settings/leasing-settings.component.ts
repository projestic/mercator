import {
    Component,
    OnInit,
    AfterContentInit,
    OnChanges,
    TemplateRef,
    ViewChild,
    Input,
    SimpleChanges,
    forwardRef, OnDestroy
} from '@angular/core';
import {
    AbstractControl,
    NgForm,
    NG_VALUE_ACCESSOR,
    ControlValueAccessor,
    NG_VALIDATORS,
    Validator,
    FormControl
} from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { LeasingCondition } from '../../models/leasing-condition';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatTabChangeEvent } from '@angular/material';
import { findIndex } from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterContainerComponent, ToasterService, ToasterConfig } from 'angular5-toaster';
import { Subscription } from 'rxjs/Rx';

const noop = () => {
};

@Component({
    selector: 'mrc-leasing-settings',
    templateUrl: './leasing-settings.component.html',
    styleUrls: ['./leasing-settings.component.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => LeasingSettingsComponent),
        multi: true
    },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => LeasingSettingsComponent),
            multi: true,
        }]
})
export class LeasingSettingsComponent implements OnInit, AfterContentInit, OnChanges, ControlValueAccessor, Validator, OnDestroy {

    @ViewChild('leasingModal') leasingModal: any;
    @ViewChild('deleteLeasingModal') deleteLeasingModal: any;
    @ViewChild('leasingForm') leasingForm: NgForm;
    @ViewChild('removeLeasing') removeLeasing: TemplateRef<any>;
    @ViewChild('editLeasing') editLeasing: TemplateRef<any>;
    @ViewChild('cellLeasing') cellLeasing: TemplateRef<any>;
    @ViewChild('cellLeasingMonth') cellLeasingMonth: TemplateRef<any>;
    @ViewChild('cellLeasingPercent') cellLeasingPercent: TemplateRef<any>;
    @ViewChild('cellLeasingRate') cellLeasingRate: TemplateRef<any>;
    @ViewChild('cellLeasingInsurance') cellLeasingInsurance: TemplateRef<any>;

    @Input() settings: Array<any>;
    @Input() leasingIcon: string;

    public mobileQuery: MediaQueryList;

    public leasingTerms: Array<any>;
    public modalServiceRate: string | number;
    public defaultInsurance = 0.35;
    public modalTitle: string;

    public productCategories: Array<IProductCategory> = [];
    public selectedProductType: number;
    public selectedProductTypeIndex: number = 0;

    public leasingTableCols = [];
    public isValidLeasingConditions = true;

    public modalAddLeasingConditionRef: BsModalRef;

    public leasing: LeasingCondition = new LeasingCondition();
    public leasingOld: LeasingCondition = new LeasingCondition();

    public selectedProductCategory: IProductCategory;

    public rows: Array<any> = [];
    public columns: Array<any> = [];

    public leasingConditionsDirty = false;
    public routeSubs: Subscription;

    constructor(private media: MediaMatcher,
                private httpService: HttpConfigService,
                private route: ActivatedRoute) {
        this.routeSubs = this.route.params.subscribe(params => {

            this.httpService.getRequest('product-categories/all').toPromise()
                .then(res => {
                    this.productCategories = res.payload;
                    this.selectedProductCategory = this.productCategories[0];
                    this.selectedProductType = this.productCategories[0].id;
                })
                .then(res => {
                    this.updateRows();
                });

            this.leasingTerms = this.getLeasingTerms();
        });
    }

    private getLeasingTerms(): Array<any> {
        const leasingTerms = [];
        for (let i = 0; i < 48; i++) {
            leasingTerms[i] = {
                value: i + 1,
                label: `${i + 1} months`
            };
        }
        return leasingTerms;
    }

    ngOnInit() {

        this.mobileQuery = this.media.matchMedia('(max-width: 767px)');

        document.addEventListener('click', e => {

            if ((event.target as any).classList.contains('j-show-leasing-modal')) {
                event.preventDefault();
                this.openAddLeasingModal();
            }

        });
    }

    ngOnChanges(changes: SimpleChanges) {
        this.updateRows();
    }

    ngAfterContentInit() {
        this.updateCols();
    }

    ngOnDestroy() {
        this.routeSubs.unsubscribe();
    }

    public getLeasingNames(): Array<string> {

        let names: Array<string> = this.settings ? this.settings.map(item => item.name) : [];

        if (this.leasingOld) {
            names = names.filter(item => item !== this.leasingOld.name);
        }

        return names;
    }

    public getLeasingCategoryTerms(): Array<string> {
        let catTerms: Array<string> = this.settings.map(item => item.product_category_id.toString() + item.period);

        if (this.leasingOld) {
            catTerms = catTerms.filter(item => item !== this.leasingOld.product_category_id.toString() + this.leasingOld.period);
        }

        return catTerms;
    }

    private validateLeasingConditions(): boolean {
        if (!this.settings) {
            return false;
        } else if (this.settings && this.settings.length > 0) {
            let catArray = this.settings.map(item => item.product_category_id);
            for (let i = 1; i <= 3; i++) {
                if (catArray.indexOf(i) === -1) {
                    return false;
                }
            }
        }
        ;
        return !!this.settings.length;
    }

    public getErrorMessages(): any {
        if (!this.isValidLeasingConditions) {
            return {
                emptyMessage: `<div class="empty-leading">
                <img src="assets/images/ico-empty-leasing.png" alt="empty" />
                There is no Leasing conditions for <span class="text-capitalize">
                ${this.selectedProductCategory.name}
                </span> for now, <a href class="j-show-leasing-modal">add first one</a>
                <div class="error-msg text-danger text-center mt-3 mb-3">
                    Fügen Sie mindestens eine Leasingbedingung hinzu
                </div>
             </div>`
            };
        }
        return {
            emptyMessage: `<div class="empty-leading">
                <img src="assets/images/ico-empty-leasing.png" alt="empty" />
                There is no Leasing conditions for <span class="text-capitalize">
                ${this.selectedProductCategory.name}
                </span> for now, <a href class="j-show-leasing-modal">add first one</a>
             </div>`
        };
    }

    public changeProductType(e) {

        // Update product type
        if (e instanceof MatTabChangeEvent) {
            this.selectedProductCategory = this.productCategories[e.index];
        } else {
            this.selectedProductCategory = this.productCategories.filter(item => item.id === this.selectedProductType)[0];
        }

        // Synchronize tabs and select
        this.selectedProductType = this.selectedProductCategory.id;
        this.selectedProductTypeIndex = findIndex(this.productCategories, o => o.id === this.selectedProductCategory.id);

        this.updateRows();
    }

    public updateRows(): void {

        if (this.settings !== null && this.selectedProductCategory) {
            this.rows = this.settings.filter(portal => {
                return portal.product_category_id === this.selectedProductCategory.id;
            });
        }

        this.rows = [...this.rows];
    }

    private updateCols(): void {

        this.columns = [{
            cellTemplate: this.cellLeasing,
            name: 'Name',
            prop: 'name',
            flexGrow: 30
        }, {
            cellTemplate: this.cellLeasingMonth,
            name: 'Leasingterms',
            prop: 'period',
            flexGrow: 20,
            sortable: false,
        }, {
            cellTemplate: this.cellLeasingPercent,
            name: 'Leasingfaktor',
            prop: 'factor',
            flexGrow: 20,
            sortable: false,
        }, {
            cellTemplate: this.cellLeasingRate,
            name: 'Service rate',
            prop: 'service_rate',
            flexGrow: 20,
            sortable: false,
        }, {
            cellTemplate: this.cellLeasingPercent,
            name: 'Residual value',
            prop: 'residual_value',
            flexGrow: 20,
            sortable: false,
        }, {
            cellTemplate: this.cellLeasingInsurance,
            name: 'Insurance',
            prop: 'insurance',
            flexGrow: 20,
            sortable: false,
        }, {
            cellClass: 'justify-content-center text-overflow-none w-50-mobile',
            cellTemplate: this.editLeasing,
            name: ' ',
            prop: 'edit',
            flexGrow: 5,
            sortable: false,
            minWidth: 60
        }, {
            cellClass: 'justify-content-center text-overflow-none w-50-mobile',
            cellTemplate: this.removeLeasing,
            name: ' ',
            prop: 'remove',
            flexGrow: 5,
            sortable: false,
            minWidth: 60
        }];

        let cols: Array<any> = [];

        if (this.media.matchMedia('(min-width: 992px)').matches) {
            cols = this.columns;
        } else {
            cols = this.columns.filter(col => !col.hideOnTablet);
        }

        this.leasingTableCols = cols;
    }

    // open modal
    public openAddLeasingModal(leasing?: LeasingCondition) {
        if (leasing) {
            this.modalTitle = 'Edit';
            this.leasingOld = leasing;
            this.leasing = new LeasingCondition(leasing);
        } else {
            this.modalTitle = 'New';
            this.leasingOld = null;
            this.leasing = new LeasingCondition();
            this.leasing.product_category_id = this.selectedProductCategory.id;
        }

        this.modalServiceRate = this.selectedProductCategory.service_rate;

        if (this.leasing.service_rate !== this.selectedProductCategory.service_rate) this.updateServiceRate(this.selectedProductCategory.service_rate, leasing);

        this.settings = [...this.settings];
        this.leasingModal.config.backdrop = false;
        this.leasingModal.show();
    }

    // open delete modal
    public openDeleteLeasingModal(leasing: LeasingCondition) {
        this.leasing = Object.assign({}, leasing);

        this.deleteLeasingModal.show();
    }

    // close modal
    public closeLeasingModal() {
        this.leasingModal.hide();
    }

    // reset form
    public resetLeasingForm() {
        this.leasingForm.reset();
    }

    public changeProductCategory(form: NgForm, category: IProductCategory): void {

        const categories: Array<string> = this.getLeasingCategoryTerms();
        const period: AbstractControl = form.controls.period;

        if (~categories.indexOf(category.id.toString() + period.value)) {
            period.setErrors({
                uniqueLeasingCategoryTerms: true
            });
        } else {
            period.setErrors(null);
        }

        this.updateServiceRate(category.service_rate);
    }

    public updateServiceRate(rate: number, leasing?: LeasingCondition): void {
        if (!leasing) this.leasing.service_rate = rate;
        this.modalServiceRate = rate;
    }


    public deleteLeasing(event): void {
        event.preventDefault();

        this.settings = this.settings.filter(item => item.key !== this.leasing.key);

        this.updateRows();

        this.writeValue(this.settings);

        this.deleteLeasingModal.hide();

        this.isValidLeasingConditions = this.validateLeasingConditions();

        this.leasingConditionsDirty = true;
    }

    public createLeasing(form: NgForm): any {

        if (form.invalid) {
            Object.keys(form.controls).forEach((key, val) => {
                form.controls[key].markAsDirty();
            });

            return false;
        }

        const leasing = new LeasingCondition(this.leasing);

        for (let item in leasing) {
            if (!leasing[item]) {
                leasing[item] = 0;
            }

            if (leasing[item] === 'Flat rate') {
                switch (item) {
                    case 'service_rate':
                        leasing[item] = this.modalServiceRate;
                        break;
                    case 'insurance':
                        leasing[item] = this.defaultInsurance;
                        break;
                    default:
                        return false;
                }
            }
        }

        if (leasing.key) {

            this.settings.forEach((item, index) => {
                if (item.key === leasing.key) {
                    this.settings[index] = leasing;
                }
            });

        } else {

            leasing.key = +new Date();

            this.settings.push(leasing);
        }


        this.settings = [...this.settings];

        this.updateRows();

        this.writeValue(this.settings);

        this.leasingModal.hide();

        this.isValidLeasingConditions = this.validateLeasingConditions();

        this.leasingConditionsDirty = form.dirty;
    }


    // The internal data model
    private innerValue: any = '';

    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    // get accessor
    get value(): any {
        return this.innerValue;
    }

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
            this.onChangeCallback(value);
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    public validate(c: FormControl) {
        if (c.value && c.value.length > 0) {
            let catArray = c.value.map(item => item.product_category_id);
            let unique = 0;
            for (let i = 1; i <= 3; i++) {
                if (catArray.indexOf(i) === -1) {
                    unique++;
                }
            }
            if (unique === 0) {
                this.isValidLeasingConditions = true;
                return null;
            }
        }
        this.isValidLeasingConditions = false;
        return {
            required: {
                valid: false,
            }
        };
    }
}

interface IProductCategory {
    id: number;
    name: string;
    service_rate: number;
}
