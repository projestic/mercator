import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeasingSettingsComponent } from './leasing-settings.component';

describe('LeasingSettingsComponent', () => {
  let component: LeasingSettingsComponent;
  let fixture: ComponentFixture<LeasingSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeasingSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeasingSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
