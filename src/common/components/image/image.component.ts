import {
    Component,
    OnInit,
    Input,
    OnChanges,
    SimpleChanges,
    Renderer2,
    ViewChild,
    ElementRef,
    AfterContentInit,
    AfterViewInit
} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { InterceptSettingsService } from '../../../services/http/intercept-settings.service';

@Component({
    selector: 'mrc-image',
    templateUrl: './image.component.html',
    styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() src: string;
    @Input() alt: string = '';
    @Input() data: string = '';

    @ViewChild('image') image: ElementRef;

    public path: string;

    constructor(private cookie: CookieService,
                public renderer: Renderer2,
                private intercept: InterceptSettingsService) {
    }

    ngOnInit() {
        const settings = this.intercept.settings;
        const token = this.cookie.get(settings.token);

        if (this.data) {
            this.path = this.data;
        } else if (token) {
            this.path = `/${settings.url}/files?path=${this.src}&token=${token}&t=${new Date().getTime()}`;
        }
    }

    ngOnChanges(simpleChanges: SimpleChanges) {
        const settings = this.intercept.settings;
        const token = this.cookie.get(settings.token);

        if (this.data) {
            this.path = this.data;
        } else if (token) {
            this.path = `/${settings.url}/files?path=${this.src}&token=${token}&t=${new Date().getTime()}`;
        }
    }

    ngAfterViewInit() {
        this.renderer.setAttribute(this.image.nativeElement, 'src', this.path);
    }

}
