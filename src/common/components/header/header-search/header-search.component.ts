import { debounceTime } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToggleSidebarService } from '../../../../services/toggle-sidebar/toggle-sidebar.service';
import { Subscription } from 'rxjs/Rx';
import { HttpConfigService } from '../../../../services/http/http.service';
import { SearchService } from '../../../../services/search/search.service';
import { FormControl } from '@angular/forms';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { AuthSettingsService } from '../../../../services/auth/auth-settings.service';

@Component({
    selector: 'app-header-search',
    templateUrl: './header-search.component.html',
    styleUrls: ['./header-search.component.scss']
})
export class HeaderSearchComponent implements OnInit, OnDestroy {
    data: any;
    sidebarSubscription: Subscription;

    searchQueryControl = new FormControl();
    formCtrlSub: Subscription;

    public searchResult: any;
    public searchCategories: Array<any> = [];
    public showSearchDropdown = false;
    public isMobileSearchOpen = false;

    private _mobileQueryQueryListener: (e) => void;

    public mobileQuery: MediaQueryList;

    private profileSubscription: Subscription;
    private url: string;

    constructor(private httpService: HttpConfigService,
                public toggleSidebar: ToggleSidebarService,
                public searchService: SearchService,
                private router: Router,
                private media: MediaMatcher,
                private authSettings: AuthSettingsService) {
        this.sidebarSubscription = this.toggleSidebar.getSidenav().subscribe(data => {
            this.data = data;
        });

        this.url = this.authSettings.path;
    }


    ngOnInit() {

        // toggle mobile menu
        this.mobileQuery = this.media.matchMedia('(max-width: 767px)');

        this._mobileQueryQueryListener = (e) => {

            if (!e.matches && this.isMobileSearchOpen) {
                this.isMobileSearchOpen = false;
            }

        };

        this.mobileQuery.addListener(this._mobileQueryQueryListener);


        // denbounds keyup in search
        this.formCtrlSub = this.searchQueryControl.valueChanges.pipe(
            debounceTime(300))
            .subscribe(newValue => {
                this.searchService.searchQuery = newValue;

                this.searchService.searchQuery && this.search(this.searchService.searchQuery);
            });


        // close search dropdown on click outside
        type HTMLElementEvent<T extends HTMLElement> = Event & {
            target: T;
        }

        document.addEventListener('click', (e: HTMLElementEvent<HTMLTextAreaElement>) => {

            if (!e.target.closest('.dropdown-menu-search') && !e.target.closest('.header-search-input')) {
                this.showSearchDropdown = false;
            }
        });
    }

    toggleNav() {
        this.toggleSidebar.sidenav.toggle();
    }

    public toggleSearch(state: boolean): void {

        if (!this.mobileQuery.matches) return;

        this.isMobileSearchOpen = state;

        if (state === false) {
            this.searchService.cleanPreview();
        }

    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.sidebarSubscription.unsubscribe();
    }


    private search(searchQuery) {
        this.searchService.loading = true;

        this.httpService.getRequest(`search/live?search=${searchQuery}`).subscribe(
            res => {

                this.searchResult = res.payload;

                this.searchCategories = [];

                let categories: Array<any> = [];

                for (var key of Object.keys(res.payload.categories)) {

                    res.payload.categories[key].length && categories.push({
                        slug: key,
                        items: res.payload.categories[key]
                    });

                    this.searchService.previewSearchResult.categories = categories;
                }

                this.searchService.previewTotal = res.payload.total;

                if (categories.length) {
                    this.searchService.searchCategory = categories[0].slug;
                } else {
                    this.searchService.searchCategory = null;
                }

                this.searchService.loading = false;
            }
        );
    }

    public submit(): void {

        const searchQuery = this.searchService.searchQuery;

        if (this.searchService.loading || !searchQuery) return;
        this.showSearchDropdown = false;


        this.searchService.total = this.searchService.previewTotal;

        this.searchService.cleanPreview();

        let url;

        switch (this.authSettings.currentModule) {
            case 'employee':
                url = `${this.url}/search`;
                break;
            default:
                url = `${this.url}/admin/search`;
        }

        this.router.navigate([url], {
            queryParams: {
                query: searchQuery
            },
            queryParamsHandling: 'merge'
        });

        this.isMobileSearchOpen = false;
    }

    onInput(e) {

    }
}
