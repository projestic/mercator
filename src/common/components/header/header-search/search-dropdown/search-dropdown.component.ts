import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SearchService } from '../../../../../services/search/search.service';
import { Router } from '@angular/router';
import { ConstantsService } from '../../../../../services/constants/constants.service';
import { AuthSettingsService } from '../../../../../services/auth/auth-settings.service';

@Component({
  selector: 'mrc-search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['./search-dropdown.component.scss']
})
export class SearchDropdownComponent implements OnInit {
    @Input() items: any;
    @Input() total: number;
    @Output() select = new EventEmitter<any>();

    private url: string;

    public categories: object;

    constructor(
        public searchService: SearchService,
        private router: Router,
        private authSettings: AuthSettingsService,
        private constants: ConstantsService) {
        this.categories = {...this.constants.categories};
        this.url = this.authSettings.path;
    }

    ngOnInit() {

    }

    public goToSearch(): void {
        const searchQuery = this.searchService.searchQuery;

        this.searchService.total = this.searchService.previewTotal;

        this.searchService.cleanPreview();

        let url;

        switch (this.authSettings.currentModule) {
            case 'employee':
                url = `${this.url}/search`;
                break;
            default:
                url = `${this.url}/admin/search`;
        }

        this.router.navigate([url], {
            queryParams: {
                query: searchQuery
            },
            queryParamsHandling: 'merge'
        });

        this.select.emit();
    }

    public goToSingle(item: any, slug: string) {
        let route;

        switch (this.authSettings.currentModule) {
            case 'employee':
                route = [`${this.url}`];
                break;
            default:
                route = [`${this.url}/admin`];
        }

        switch (slug) {
            case 'users':
                if (this.authSettings.currentModule === 'company') return false;

                route.push(slug.slice(0, -1));
                route.push(item.id);
                route.push(item.type);
                route.push('edit');
                break;
            case 'portals':
                route.push(slug.slice(0, -1));
                route.push(item.id);
                route.push('edit');
                break;
            default:
                route.push(slug.slice(0, -1));
                route.push(item.id);
        }

        switch (this.authSettings.currentModule) {
            case 'employee':
                route.push(item.number);
                break;
            case 'supplier':
                route.push(item.number);
                break;
        }

        this.searchService.cleanPreview();

        this.router.navigate(route);

        this.select.emit();
    }

    public getSingleName(item: any, slug: string): string {
        let name: string;

        switch (slug) {
            case 'users':
            case 'portals':
            case 'suppliers':
                name = item.name;
                break;
            default:
                name = item.number;
        }

        return name;
    }
}
