import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from '../../../../services/auth/auth.service';

@Component({
    selector: 'app-header-menu',
    templateUrl: './header-menu.component.html',
    styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent implements OnInit {

    @ViewChild('logoutModal') logoutModal: any;

    constructor(private auth: AuthenticationService) {
    }

    ngOnInit() {
    }

    public logout() {
        this.logoutModal.show();
    }

    public logoutEvent(event) {
        event.preventDefault();
        this.auth.logout();
    }
}
