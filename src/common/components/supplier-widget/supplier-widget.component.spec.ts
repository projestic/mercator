import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierWidgetComponent } from './supplier-widget.component';

describe('SupplierWidgetComponent', () => {
  let component: SupplierWidgetComponent;
  let fixture: ComponentFixture<SupplierWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
