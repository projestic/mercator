import { Component, OnInit, Input, Output, EventEmitter, OnChanges, AfterViewInit } from '@angular/core';

@Component({
    selector: 'mrc-supplier-widget',
    templateUrl: './supplier-widget.component.html',
    styleUrls: ['./supplier-widget.component.scss']
})
export class SupplierWidgetComponent implements OnInit {
    @Input() title;
    @Input() widgetTitles;

    _statuses: Array<string>;

    @Input()
    set statuses(statuses: Array<string>) {
        this._statuses = ['Alle'].concat(statuses);
    }

    get statuses() {
        return this._statuses;
    }

    @Input() preloader: boolean = false;
    @Output() updateWidget = new EventEmitter<any>();

    public currentItem: string;

    constructor() {
    }

    ngOnInit() {
    }

    changeSource(event, item) {
        event.preventDefault();
        this.currentItem = item;
        this.updateWidget.emit(item === 'Alle' ? '' : item);
    }
}
