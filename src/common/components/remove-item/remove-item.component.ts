import { Component, OnInit, Input, Output, EventEmitter, TemplateRef  } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'mrc-remove-item',
    templateUrl: './remove-item.component.html',
    styleUrls: ['./remove-item.component.scss']
})
export class RemoveItemComponent implements OnInit {
    @Input() id: string | number;
    @Input() userType: string;
    @Input() category: string;

    @Output() reloadTable: EventEmitter<any> = new EventEmitter();

    modalRef: BsModalRef;

    public item = '';

    constructor( private httpService: HttpConfigService,
                 private toasterService: ToasterService,
                 private modalService: BsModalService) {
    }

    ngOnInit() {
        switch (this.category) {
            case 'users':
                this.item = 'User';
                break;
            case 'companies':
                this.item = 'Company';
                break;
        }
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    removeItem() {
        let url;
        switch (this.category) {
            case 'users':
                url = `users/${this.id}`;
                if (this.userType === 'portal') {
                    url = `portal-users/${this.id}`;
                }
                break;
            case 'companies':
                url = `companies/${this.id}`;
                break;
        }

        this.httpService.deleteRequest(url)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `${this.item} was removed successfully`);
                    this.reloadTable.emit();
                    this.modalRef.hide();
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

}
