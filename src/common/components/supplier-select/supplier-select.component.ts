import { Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator} from '@angular/forms';
import {HttpConfigService} from '../../../services/http/http.service';

const noop = () => {
};

@Component({
    selector: 'mrc-supplier-select',
    templateUrl: './supplier-select.component.html',
    styleUrls: ['./supplier-select.component.scss'],
    providers: [{
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SupplierSelectComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => SupplierSelectComponent),
            multi: true,
        }]
})
export class SupplierSelectComponent implements OnInit, OnChanges, ControlValueAccessor, Validator {

    @Input() suppliers: Array<any> = [];
    @Input() control: any;
    @Input() title: string;
    @Input() icon: string;
    @Input() searchable: boolean;

    public suppliersList: Array<any> = [];
    public invalid = false;
    public dirty = false;

    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    public currentSuppliers: Array<any>;

    constructor(private httpService: HttpConfigService) {
    }

    ngOnInit() {

    }

    ngOnChanges() {
        this.getSuppliers()
            .then(res => {
                this.currentSuppliers = this.getCurrentSuppliers(res);
            });
    }

    // get accessor
    get value(): any {
        return this.suppliers;
    }

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.suppliers) {
            this.suppliers = v;
            this.onChangeCallback(v);
        }
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.suppliers) {
            this.suppliers = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    public validate(c: FormControl) {
        if (c.value && c.value.length > 0) {
            this.invalid = false;
            return null;
        }

        this.invalid = true;
    }

    getSuppliers() {
        const promise = new Promise((resolve, reject) => {
            const url = 'suppliers/all?status=Aktiv';
            this.httpService.getRequest(url).toPromise().then(
                res => {
                    this.suppliersList = res.payload;
                    resolve(this.suppliersList);
                },
                error => {
                    reject();
                });
        });
        return promise;
    }

    getCurrentSuppliers(suppliers): Array<any> {
        let currentSuppliers: Array<any> = this.suppliers ? [...this.suppliers] : [];
        currentSuppliers = currentSuppliers.map(item => {
            let res;
            suppliers.forEach(i => {
                if (i.id === item) res = i;
            });
            return res;
        });
        return currentSuppliers;
    }

    onSupplierChange() {
        this.currentSuppliers = this.getCurrentSuppliers(this.suppliersList);
    }

    removeSupplier(event, id) {
        event.preventDefault();
        let list = [...this.suppliers];
        list = list.filter(item => item !== id);
        this.value = list;
        this.dirty = true;
        this.currentSuppliers = this.getCurrentSuppliers(this.suppliersList);
    }

}
