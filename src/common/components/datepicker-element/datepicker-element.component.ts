import { Component, OnInit, Input, forwardRef } from '@angular/core';
import {
    ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator, FormControl,
    ValidationErrors
} from '@angular/forms';
import { DatePipe } from '@angular/common';

const noop = () => {};

@Component({
    selector: 'mrc-datepicker-element',
    templateUrl: './datepicker-element.component.html',
    styleUrls: ['./datepicker-element.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatepickerElementComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => DatepickerElementComponent),
            multi: true
        },
        DatePipe
    ]
})
export class DatepickerElementComponent implements OnInit, ControlValueAccessor, Validator {
    @Input() minDate: string | number;
    @Input() maxDate: string | number;
    @Input() titleDatepicker: string;

    public innerValue: string;

    public datePickerValue = '';

    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    get value(): any {
        return this.innerValue;
    }

    set value(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
            this.onChangeCallback(this.datePipe.transform(new Date(value), 'yyyy-MM-dd'));
        }
    }

    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
            if (!value) this.datePickerValue = null;
        }
    }

    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    constructor(private datePipe: DatePipe) {
    }

    ngOnInit() {
    }

    datePickerChange(value: Date) {
        if (value) {
            const date = new Date(new Date(value).getTime() - new Date().getTimezoneOffset() * 60 * 1000);
            const month = this.datePipe.transform(date, 'LLL');
            const year = this.datePipe.transform(date, 'yyyy');

            this.datePickerValue =
                `${month} ${date.getDate()} ${year}`;
        }
    }



    validate(c: FormControl): ValidationErrors | null {
        /*if (!c.value) return {'required': false};*/
        return null;
    }
}
