import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../system-admin-app/material.module';
import { BootstrapModule } from '../system-admin-app/bootstrap.module';
import { ClickOutsideModule } from 'ng4-click-outside';
import { ChartsModule } from 'ng2-charts';
import { AddWidgetComponent } from './components/add-widget/add-widget.component';

//Pipes
import { PipesModule } from './pipes/pipes.module';

//Header

import { HeaderComponent } from './components/header/header.component';
import { HeaderSearchComponent } from './components/header/header-search/header-search.component';
import { SearchDropdownComponent } from './components/header/header-search/search-dropdown/search-dropdown.component';
import { HeaderMenuComponent } from './components/header/header-menu/header-menu.component';

//Components
import { CardComponent } from './components/card/card.component';
import { CardChartComponent } from './components/card/card-chart/card-chart.component';
import { CardListComponent } from './components/card/card-list/card-list.component';
import { FormItemFieldComponent } from './components/forms/form-item-field/form-item-field.component';
import { ImageComponent } from './components/image/image.component';
import { PagerComponent } from './components/table/pager.component';
import { StatusComponent } from './components/status/status.component';
import { TableComponent } from './components/table/table.component';
import { ValidationErrorsComponent } from './components/validation-errors/validation-errors.component';
import { RemoveItemComponent } from './components/remove-item/remove-item.component';
import { FilterComponent } from './components/table/filter.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SidebarMenuComponent } from './components/sidebar/sidebar-menu/sidebar-menu.component';
import { SupplierWidgetComponent } from './components/supplier-widget/supplier-widget.component';
import { SearchComponent } from './pages/search/search.component';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardTemplateComponent } from './components/dashboard-template/dashboard-template.component';
import { DetailComponent } from './components/detail/detail.component';
import { AccountComponent } from './pages/account/account.component';
import { SupplierSelectComponent } from './components/supplier-select/supplier-select.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { Page404Component } from './pages/page-404/page-404.component';
import { LoginAsBtnComponent } from './components/login-as-btn/login-as-btn.component';


//Directives
import { AlphaDashDirective } from './directives/validators/alpha-dash.directive';
import { DomainDirective } from './directives/validators/domain.directive';
import { EmailDirective } from './directives/validators/email.directive';
import { MaxDirective } from './directives/validators/max.directive';
import { MinDirective } from './directives/validators/min.directive';
import { NumberDirective } from './directives/validators/number.directive';
import { EqualValidator } from './directives/validators/equal-validator.directive';
import { PasswordDirective } from './directives/validators/password.directive';
import { RequiredDirective } from './directives/validators/required.directive';
import { UniqueDirective } from './directives/validators/unique.directive';
import { UniqueLeasingCategoryTermsDirective } from './directives/validators/unique-leasing-category-terms.directive';

//Services
import { TableService } from './components/table/table.service';
import { RouterModule } from '@angular/router';
import { HeadContentComponent } from './components/head-content/head-content.component';
import { LeasingSettingsComponent } from './components/leasing-settings/leasing-settings.component';
import { FlatRateDirective } from './directives/validators/flat-rate.directive';
import { PhoneDirective } from './directives/validators/phone.directive';
import { TableActionsComponent } from './components/table/table-actions/table-actions.component';
import { ToasterService } from 'angular5-toaster/dist';
import { DatepickerElementComponent } from './components/datepicker-element/datepicker-element.component';
import { AtLeastOneDirective } from './directives/validators/at-least-one.directive';
import { MenuService } from './components/sidebar/menu.service';
import { SidebarSettingsService } from './components/sidebar/sidebar-settings.service';
import { RegistrationComponent } from './pages/registration/registration.component';
import { CheckRequiredDirective } from './directives/validators/check-required.directive';
import { CompareNumberDirective } from './directives/validators/compare-number.directive';


@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        PipesModule,
        NgxDatatableModule,
        NgSelectModule,
        MaterialModule,
        BootstrapModule,
        RouterModule,
        ClickOutsideModule,
        ChartsModule,
    ],
    declarations: [
        //Components
        AccountComponent,
        AddWidgetComponent,
        CardComponent,
        CardChartComponent,
        CardListComponent,
        FormItemFieldComponent,
        ImageComponent,
        PagerComponent,
        StatusComponent,
        TableComponent,
        ValidationErrorsComponent,
        RemoveItemComponent,
        FilterComponent,
        DatepickerComponent,
        LoginComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        SidebarComponent,
        SidebarMenuComponent,
        SupplierWidgetComponent,
        HeaderComponent,
        SearchDropdownComponent,
        HeaderSearchComponent,
        HeaderMenuComponent,
        HeadContentComponent,
        SearchComponent,
        FooterComponent,
        DashboardTemplateComponent,
        DetailComponent,
        SupplierSelectComponent,
        SettingsComponent,
        TableActionsComponent,
        DatepickerElementComponent,
        RegistrationComponent,
        Page404Component,
        LoginAsBtnComponent,

        //Directives
        AlphaDashDirective,
        DomainDirective,
        EmailDirective,
        NumberDirective,
        MaxDirective,
        MinDirective,
        EqualValidator,
        PasswordDirective,
        RequiredDirective,
        UniqueDirective,
        UniqueLeasingCategoryTermsDirective,
        LeasingSettingsComponent,
        FlatRateDirective,
        PhoneDirective,
        AtLeastOneDirective,
        CheckRequiredDirective,
        CompareNumberDirective,
    ],
    exports: [
        //Components
        AccountComponent,
        AddWidgetComponent,
        CardComponent,
        CardChartComponent,
        CardListComponent,
        FormItemFieldComponent,
        ImageComponent,
        PagerComponent,
        StatusComponent,
        TableComponent,
        ValidationErrorsComponent,
        RemoveItemComponent,
        FilterComponent,
        DatepickerComponent,
        LoginComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        SidebarComponent,
        SidebarMenuComponent,
        SupplierWidgetComponent,
        HeaderComponent,
        SearchDropdownComponent,
        HeaderSearchComponent,
        HeaderMenuComponent,
        HeadContentComponent,
        SearchComponent,
        LeasingSettingsComponent,
        FooterComponent,
        DashboardTemplateComponent,
        DetailComponent,
        SupplierSelectComponent,
        SettingsComponent,
        TableActionsComponent,
        DatepickerElementComponent,
        Page404Component,
        LoginAsBtnComponent,

        //Directives
        AlphaDashDirective,
        DomainDirective,
        EmailDirective,
        MaxDirective,
        MinDirective,
        NumberDirective,
        EqualValidator,
        PasswordDirective,
        RequiredDirective,
        UniqueDirective,
        UniqueLeasingCategoryTermsDirective,
        PhoneDirective,
        AtLeastOneDirective,
        CheckRequiredDirective,
        CompareNumberDirective,

        //modules
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        PipesModule,
        NgxDatatableModule,
        NgSelectModule,
        MaterialModule,
        ClickOutsideModule,
        ChartsModule,
    ]
})

export class AppCommonModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AppCommonModule,
            providers: [
                TableService,
                ToasterService,
                MenuService,
                SidebarSettingsService,
            ]
        };
    }
}
