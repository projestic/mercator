import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[alphaDash][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: AlphaDashDirective,
            multi: true
        }
    ]
})
export class AlphaDashDirective {

    constructor() {
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const alphaDash = control.value;

        if (control.value && !control.value.match(/^[a-zA-Z\d-_]+$/)) {
            return {
                alphaDash: `The slug may only contain letters, numbers, and dashes.`
            };
        }

        return null;
    }
}
