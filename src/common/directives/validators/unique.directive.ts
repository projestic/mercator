import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[unique][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: UniqueDirective,
            multi: true
        }
    ]
})
export class UniqueDirective {
    @Input() match: Array<string>;

    constructor() {
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const unique = control.value;

        if (unique && ~this.match.indexOf(unique)) {
            return {
                unique: `Existiert bereits`
            };
        }

        return null;
    }
}
