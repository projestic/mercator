import { Attribute, Directive, forwardRef } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[mrcAtLeastOne][formControlName],[mrcAtLeastOne][formControl],[mrcAtLeastOne][ngModel]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => AtLeastOneDirective),
        multi: true
    }]
})
export class AtLeastOneDirective {

    constructor(
        @Attribute('mrcAtLeastOne') public mrcAtLeastOne: string,
        @Attribute('reverse') public reverse: string) {
    }

    private get isReverse() {
        if (!this.reverse) return false;
        return this.reverse === 'true' ? true : false;
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const v = control.value;
        const e = control.root.get(this.mrcAtLeastOne);

        if (e && !v && !e.value && !this.isReverse) {
            return {
                mrcAtLeastOne: 'Please enter either an email address or ID number'
            };
        };

        // value equal and reverse
        if (e && (v || e.value) && this.isReverse) {
            if (e.errors) delete e.errors['mrcAtLeastOne'];
            if (e.errors && !Object.keys(e.errors).length) {
                e.setErrors(null);
            };
        }

        // value not equal and reverse
        if (e && (!v && !e.value) && this.isReverse) {
            e.setErrors({ mrcAtLeastOne: `Please enter either an email address or ID number` });
            return {
                mrcAtLeastOne: 'Please enter either an email address or ID number'
            };
        }
        return null;
    }

}
