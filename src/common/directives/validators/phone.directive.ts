import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[phone][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: PhoneDirective,
            multi: true
        }
    ]
})
export class PhoneDirective implements Validator {

    constructor() {
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const number = control.value;

        if (number && !String(number).match(/^(([+]?[0-9]{10,13})|([+][0-9]{2,3}[(][0-9]{3,5}[)][0-9]{7,9})){1}$/)) {
            return {
                phone: `Phone number should contain 10 -13 digits`
            };
        }
        return null;
    }
}
