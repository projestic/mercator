import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[checkRequired][formControlName],[checkRequired][formControl],[checkRequired][ngModel]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: CheckRequiredDirective,
        multi: true
    }]
})
export class CheckRequiredDirective implements Validator {

    constructor() {
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const fieldValue = control.value;

        if (fieldValue === undefined || fieldValue === '' || fieldValue === false) {
            return {
                required: 'Please Accept our Terms and Conditions'
            };
        }

        return null;
    }

}
