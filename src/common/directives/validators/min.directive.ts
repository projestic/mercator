import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[min][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: MinDirective,
            multi: true
        }
    ]
})
export class MinDirective {
    @Input() min: number;

    constructor() {
        
    }

    validate(control: AbstractControl): ValidationErrors | null {

        if (+control.value < +this.min) {
            return {
                max: `Der minimale Wert ist ${this.min}`
            };
        }

        return null;
    }
}
