import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[uniqueLeasingCategoryTerms][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: UniqueLeasingCategoryTermsDirective,
            multi: true
        }
    ]
})
export class UniqueLeasingCategoryTermsDirective {
    @Input() category: number;
    @Input() matches: Array<string>;

    constructor() {
        
    }

    validate(control: AbstractControl): ValidationErrors | null {

        const uniqueLeasingCategoryTerms = this.category.toString() + control.value;

        if (~this.matches.indexOf(uniqueLeasingCategoryTerms)) {
            return {
                uniqueLeasingCategoryTerms: `Doppelte Kategorie und Begriffe`
            };
        }
        return null;
    }
}
