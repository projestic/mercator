import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[number][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: NumberDirective,
            multi: true
        }
    ]
})
export class NumberDirective implements Validator {

    constructor() {
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const number = control.value;

        if (number && !String(number).match(/^([0-9]{1,6}){1}(\.[0-9]{1,2})?$/)) {
            return {
                number: `Das Feld muss eine Zahl sein`
            };
        }
        return null;
    }
}
