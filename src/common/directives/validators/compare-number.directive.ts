import { Attribute, Directive, forwardRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
    selector: '[compareNumber][formControlName],[compareNumber][formControl],[compareNumber][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => CompareNumberDirective),
            multi: true
        }
    ]
})
export class CompareNumberDirective implements Validator, OnChanges {
    private _onChange: () => void;

    @Input('compareNumber') compareNumber: string | number;

    constructor() {
    }

    validate(c: AbstractControl): { [key: string]: any } {
        const v = c.value;

        if (this.compareNumber && +v >= +this.compareNumber) {
            return {
                compareNumber: `Value shouldn't be more then Normal Price`
            };
        }
        return null;
    }

    registerOnValidatorChange(fn: () => void): void { this._onChange = fn; }

    ngOnChanges (changes: SimpleChanges): void {

        if ('compareNumber' in changes) {

            if (this._onChange) this._onChange();
        }
    }

}
