import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { FormControl } from '@angular/forms';

@Directive({
    selector: '[password][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: PasswordDirective,
            multi: true
        }
    ]
})
export class PasswordDirective implements Validator {

    constructor() {
    }

    validate(control: AbstractControl): ValidationErrors | null {
        const password = control.value;

        if (password && !password.match(/^(?=.*\d)(?=.*[^a-zA-Z\d\s:])(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,15}$/)) {
            return {
                password: `Ungültiges Passwort`
            };
        }

        return null;
    }
}
