import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[domain][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: DomainDirective,
            multi: true
        }
    ]
})

export class DomainDirective implements Validator {

    constructor() {}

    validate(control: AbstractControl): ValidationErrors | null {
        const domain = control.value;
        if (domain && !domain.match(/[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/)) {
            return {
                domain: 'Ungültige Domäne'
            };
        }
        return null;
    }
}

