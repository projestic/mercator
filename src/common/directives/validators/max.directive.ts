import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[max][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: MaxDirective,
            multi: true
        }
    ]
})
export class MaxDirective {
    @Input() max: number;

    constructor() {
        
    }

    validate(control: AbstractControl): ValidationErrors | null {

        if (control.value > this.max) {
            return {
                max: `Der maximale Wert ist ${this.max}`
            };
        }

        return null;
    }
}
