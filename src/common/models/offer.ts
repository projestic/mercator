import { OfferInterface } from './offer.interface';
import { ProductInterface } from './product.interface';
import { Product } from './product';

export class Offer implements OfferInterface {
    public accessories_price;
    public frame_id;
    public normal_price;
    public discount_price;
    public product;
    public notes;

    constructor(offer?: OfferInterface) {
        if (offer) {
            this.accessories_price = offer.accessories_price;
            this.product = new Product(offer.product);
            this.frame_id = offer.frame_id;
            this.normal_price = offer.normal_price;
            this.discount_price = offer.discount_price;
            this.notes = offer.notes;
        }
    }
}
