import { SupplierInfoInterface } from './supplier.interface';
import { CityInterface } from './city.interface';

export class SupplierInfo implements SupplierInfoInterface {
    public active_from: string;
    public address: string;
    public admin_email: string;
    public admin_first_name: string;
    public admin_last_name: string;
    public city: CityInterface;
    public code: string;
    public city_id: string | number;
    public companies: Array<object>;
    public employees_count: string | number;
    public id: string | number;
    public max_products_count: string | number;
    public name: string;
    public phone: string | number;
    public portal_id: string | number;
    public portals: Array<object>;
    public products_count: string | number;
    public status: string;
    public statusBool: boolean;
    public updated_at: string;
    public vat: string;
    public option1: boolean;
    public option2: boolean;
    public option3: boolean = true;
    public option4: boolean;
    public option5: boolean;
    public option6: boolean;

    constructor(supplier?: SupplierInfoInterface) {
        if (supplier) {
            this.active_from = supplier.active_from;
            this.address = supplier.address;
            this.admin_email = supplier.admin_email;
            this.admin_first_name = supplier.admin_first_name;
            this.admin_last_name = supplier.admin_last_name;
            this.city = supplier.city;
            this.city_id = supplier.city_id;
            this.code = supplier.code;
            this.companies = supplier.companies;
            this.employees_count = supplier.employees_count;
            this.id = supplier.id;
            this.max_products_count = supplier.max_products_count;
            this.name = supplier.name;
            this.phone = supplier.phone;
            this.portal_id = supplier.portal_id;
            this.portals = supplier.portals;
            this.products_count = supplier.products_count;
            this.status = supplier.status;
            this.updated_at = supplier.updated_at;
            this.vat = supplier.vat;
        }

        this.statusBool = this.statusCheck();
    }

    statusCheck() {
        if (this.status === 'Inaktiv') return false;
        return true;
    }
}
