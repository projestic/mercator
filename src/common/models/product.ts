import { SupplierInfoInterface } from './supplier.interface';
import { SupplierInfo } from './supplier';
import { ProductInterface } from './product.interface';

export class Product implements ProductInterface {
    public brand: object;
    public attributes: Array<any> = [];
    public id: string | number;
    public model;
    public description: string;
    public category: object = {};
    public image: string;
    public supplier: SupplierInfoInterface;

    constructor(orderProduct?: ProductInterface) {
        if (orderProduct) {
            this.brand = orderProduct.brand;
            this.attributes = orderProduct.attributes ? [...orderProduct.attributes] : [];
            this.id = orderProduct.id;
            this.model = orderProduct.model ? {...orderProduct.model} : {};
            this.description = orderProduct.description;
            this.category = orderProduct.category ? {...orderProduct.category} : {};
            this.image = orderProduct.image;
            this.supplier = new SupplierInfo(orderProduct.supplier);
        }
    }
}
