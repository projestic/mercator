export interface ContractInterface {
    start_date: string;
    end_date: string;
    id: string | number;
    number: number;
    product_size: string;
    status: string;
}
