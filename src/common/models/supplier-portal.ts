import { SupplierInfo } from './supplier';
import { SupplierPortalInfoInterface } from './supplier-portal.interface';

export class SupplierPortalInfo extends SupplierInfo implements SupplierPortalInfoInterface {
    public zip: number | string;

    constructor(portalInfo?: SupplierPortalInfoInterface) {
        super(portalInfo);
        if (portalInfo) {
            this.zip = portalInfo.zip;
        }
    }
}
