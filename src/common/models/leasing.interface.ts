export interface LeasingInterface {
    agreed_purchase_price: string | number;
    calculated_residual_value: string | number;
    insurance: string | number;
    leasing_period: string | number;
    leasing_rate: string | number;
}
