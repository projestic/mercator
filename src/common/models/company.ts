import { CompanyInterface } from './company.interface';

export class Company implements CompanyInterface {
    public id: string | number;
    public address: string;
    public city: any;
    public code: string;
    public leasing_factor: string;
    public name: string;
    public env_settings: string;
    public cycles: string;
    public is_active: string | number;
    public zip: string | number;

    constructor(company?: CompanyInterface) {
        if (company) {
            this.id = company.id;
            this.address = company.address;
            this.city = company.city;
            this.leasing_factor = company.leasing_factor;
            this.name = company.name;
            this.env_settings = company.env_settings;
            this.code = company.code;
            this.cycles = company.cycles;
            this.is_active = company.is_active;
            this.zip = company.zip;
        }
    }
}
