import { LeasingInterface } from './leasing.interface';

export class Leasing implements LeasingInterface {
    public agreed_purchase_price: string | number;
    public calculated_residual_value: string | number;
    public insurance: string | number;
    public leasing_period: string | number;
    public leasing_rate: string | number;

    constructor(leasing?: any) {
        if (leasing) {
            this.agreed_purchase_price = leasing.agreed_purchase_price;
            this.calculated_residual_value = leasing.calculated_residual_value;
            this.insurance = leasing.insurance;
            this.leasing_period = leasing.leasing_period;
            this.leasing_rate = leasing.leasing_rate;
        }
    }
}
