export interface ILeasingCondition {
    factor: number;
    id?: string | number;
    insurance: string | number;
    name: string;
    period: string | number;
    product_category_id: string | number;
    residual_value: number;
    service_rate: number | string;
}

