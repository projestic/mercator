export interface OrderInterface {
    number: string;
    status: string;
    date: string;
    agreed_purchase_price: string;
    leasing_rate: string;
    insurance: string;
    calculated_residual_value: string;
    leasing_period: string;
    product_size: string;
}
