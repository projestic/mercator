import { ProfileInterface } from './profile.interface';

export class Profile implements ProfileInterface {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    password_confirmation: string;
    role: string;

    constructor(profile?: ProfileInterface) {
        if (profile) {
            this.first_name = profile.first_name;
            this.last_name = profile.last_name;
            this.email = profile.email;
            this.role = profile.role;
        }
    }
}
