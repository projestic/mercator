import { CityInterface } from './city.interface';

export interface SupplierInfoInterface {
    active_from: string;
    address: string;
    admin_email: string;
    admin_first_name: string;
    admin_last_name: string;
    city: CityInterface;
    city_id: string | number;
    code: string;
    employees_count: string | number;
    id: string | number;
    max_products_count: string | number;
    name: string;
    phone: string | number;
    portal_id: string | number;
    portals?: Array<object>;
    companies?: Array<object>;
    products_count: string | number;
    status: string;
    statusBool: boolean;
    updated_at: string;
    vat: string;
}
