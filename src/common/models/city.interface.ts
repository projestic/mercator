export interface CityInterface {
    id: number;
    name: string;
    lat: string;
    lan: string;
}
