import { SupplierInfoInterface } from './supplier.interface';

export interface ProductInterface {
    brand: object;
    attributes: Array<any>;
    id: string | number;
    model: {
        name: string,
        id: number
    },
    description: string;
    category: object;
    image: string;
    supplier: SupplierInfoInterface;
}
