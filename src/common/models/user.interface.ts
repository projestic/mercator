import { CompanyInterface } from './company.interface';

export interface UserInterface {
    active_contracts: number;
    active_from: string;
    avatar: string;
    code?: string;
    company: CompanyInterface;
    email: string;
    first_name: string;
    id: string | number;
    last_name: string;
    status: string;
    fullname: string;
}
