import { ProductInterface } from './product.interface';

export interface OfferInterface {
    accessories_price: number | string;
    frame_id: number | string;
    normal_price: number | string;
    discount_price?: number | string;
    notes: string;
    product: ProductInterface;
}
