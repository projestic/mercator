export interface ISupplierSettings {
    admin_email: string;
    color: string;
    logo: string;
    name: string;
    phone: string | number;
    shop_name: string;
    vat: string | number;
}
