export interface CompanyInterface {
    id: string | number;
    address: string;
    city: any;
    code: string;
    leasing_factor: string;
    name: string;
    env_settings: string;
    cycles: string;
    is_active: string | number;
    zip: number | string;
}
