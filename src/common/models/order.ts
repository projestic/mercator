import { OrderInterface } from './order.interface';

export class Order implements OrderInterface {
    public number: string;
    public status: string;
    public date: string;
    public agreed_purchase_price: string;
    public leasing_rate: string;
    public insurance: string;
    public calculated_residual_value: string;
    public leasing_period: string;
    public product_size: string;

    constructor(order?: OrderInterface) {
        if (order) {
            this.number = order.number;
            this.status = order.status;
            this.date = order.date;
            this.agreed_purchase_price = order.agreed_purchase_price;
            this.leasing_rate = order.leasing_rate;
            this.insurance = order.insurance;
            this.calculated_residual_value = order.calculated_residual_value;
            this.leasing_period = order.leasing_period;
            this.product_size = order.product_size;
        }
    }
}
