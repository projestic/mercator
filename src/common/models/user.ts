import { UserInterface } from './user.interface';
import { CompanyInterface } from './company.interface';
import { Company } from './company';

export class User implements UserInterface {
    public active_contracts: number;
    public active_from: string;
    public code: string;
    public avatar: string;
    public company: CompanyInterface;
    public email: string;
    public first_name: string;
    public id: string | number;
    public last_name: string;
    public status: string;
    constructor(user?: UserInterface) {
        if (user) {
            this.active_contracts = user.active_contracts;
            this.active_from = user.active_from;
            this.avatar = user.avatar;
            this.active_contracts = user.active_contracts;
            this.code = user.code;
            this.company = new Company(user.company);
            this.email = user.email;
            this.first_name = user.first_name;
            this.id = user.id;
            this.last_name = user.last_name;
            this.status = user.status;
        }
    }


    get fullname(): string {
        let comp: Array<string> = [];
        comp.push(this.first_name);
        comp.push(this.last_name);

        return comp.join(' ');
    }

    get userName() {
        return `${this.first_name} ${this.last_name}`;
    }
}
