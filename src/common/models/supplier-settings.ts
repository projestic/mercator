import { forEach } from 'lodash';
import { ISupplierSettings } from './supplier-settings.interface';


export class SupplierSettings implements ISupplierSettings {
    public admin_email: string;
    public color: string = null;
    public logo: string = null;
    public name: string;
    public phone: string | number;
    public shop_name: string;
    public vat: string | number;

    constructor (settings) {
        if (settings) {
            forEach(settings, (val, key) => {
                this[key] = val || null;
            });
        }
    }
}
