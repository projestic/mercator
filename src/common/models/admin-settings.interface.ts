interface IAdminSettings {
    color: string;
    date_format: string;
    domain: string;
    email: string;
    logo: string;
    time_format: string;
    timezone: string
    week_starts_on: string;
}
