import { forEach } from 'lodash';
import { IPortalSettings } from './portal-settings.interface';
import { LeasingCondition } from './leasing-condition';

export class PortalSettings implements IPortalSettings {
    public admin_email: string;
    public admin_first_name: string;
    public admin_last_name: string;
    public color: string;
    public company_address: string;
    public company_city_id: string;
    public company_name: string;
    public company_vat: string;
    public company_zip: string;
    public domain: string;
    public leasing_settings: Array<any> = [];
    public logo: string;
    public name: string;
    public status: string;
    public statusBool: boolean;

    constructor (settings) {

        if (settings) {

            forEach(settings, (val, key) => {
                if (key === 'leasing_settings') {
                    val.forEach(item => {
                        this.leasing_settings.push(new LeasingCondition(item));
                    });
                } else {
                    this[key] = val;
                }
            });
        }

        this.statusBool = this.statusCheck();
    }

    statusCheck() {
        if (this.status === 'Inaktiv') return false;
        return true;
    }
}
