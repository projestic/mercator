export interface ProfileInterface {
    first_name: string;
    last_name: string;
    email: string;
    old_password?: string;
    password?: string;
    password_confirmation?: string;
    role: string;
}
