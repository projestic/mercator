import { ILeasingCondition} from './leasing-condition.interface';

export class LeasingCondition implements ILeasingCondition {

    public factor: number;
    public id: string | number;
    public insurance: number | string = 0.35; //TODO: fixed
    public key: number | string;
    public name: string;
    public period: string | number = 6;
    public product_category_id: string | number = 1;
    public residual_value = 0;
    public service_rate: number | string;

    constructor(data?: any) {

        if (data) {
            this.factor = data.factor;
            this.id = data.id;
            this.insurance = data.insurance;
            this.key = data.key || data.id;
            this.name = data.name;
            this.period = data.period;
            this.product_category_id = data.product_category_id;
            this.residual_value = data.residual_value;
            this.service_rate = data.service_rate;
        }
    }
}
