import { forEach } from 'lodash';

export class AdminSettings implements IAdminSettings {
    public color: string;
    public email: string;
    public date_format: string;
    public domain: string;
    public logo: string;
    public time_format: string;
    public timezone: string;
    public week_starts_on: string;

    constructor (settings) {

        if (settings) {

            forEach(settings, (val, key) => {
                this[key] = val;
            });
        }
    }
}
