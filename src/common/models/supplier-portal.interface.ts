import { SupplierInfoInterface } from './supplier.interface';

export interface SupplierPortalInfoInterface extends SupplierInfoInterface {
    zip: number | string;
}
