import { ContractInterface } from './contract.interface';

export class Contract implements ContractInterface {
    public start_date: string;
    public end_date: string;
    public id: string | number;
    public number: number;
    public product_size: string;
    public status: string;

    constructor(
        contract?: ContractInterface
    ) {
        if (contract) {
            this.start_date = contract.start_date;
            this.end_date = contract.end_date;
            this.id = contract.id;
            this.number = contract.number;
            this.product_size = contract.product_size;
            this.status = contract.status;
        }
    }
}
