import { Component, DoCheck, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular5-toaster';
import { HttpConfigService } from '../../../services/http/http.service';
import { forIn, find } from 'lodash';
import { SettingsService } from '../../../services/settings/settings.service';
import { IPortalSettings } from '../../models/portal-settings.interface';
import { PortalSettings } from '../../models/portal-settings';
import { ProfileInterface } from '../../models/profile.interface';
import { Profile } from '../../models/profile';
import { Subscription } from 'rxjs';
import { ProfileService } from '../../../services/profile/profile.service';

@Component({
    selector: 'mrc-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy, DoCheck {
    @ViewChild('settingsForm') settingsForm: HTMLFormElement;

    user: ProfileInterface = new Profile();
    public profileSubscriber: Subscription;
    public title = 'Einstellungen';
    public statusLabel = 'Portalstatus:';
    public settings: IPortalSettings = new PortalSettings({});
    public settingsOld: IPortalSettings = new PortalSettings({});
    public selectedColor: string;
    public submitting: boolean = false;

    public invoiceType = ['Net', 'Gross'];
    public insuranceType = ['Fixed', 'Percentage'];
    public rateType = ['Fixed', 'Percentage'];
    public fixed = '<i class="material-icons">euro_symbol</i>';
    public percentage = '<i class="percent-icon">%</i>';

    public cityList: Array<any>;

    public showSubmit: boolean;

    constructor(private http: HttpConfigService,
                public profileService: ProfileService,
                private toasterService: ToasterService,
                private settingsService: SettingsService) {
        this.profileService.getProfile();
        this.profileSubscriber = this.profileService.getProfileObj().subscribe(
            res => {
                this.user = new Profile(res);
                if (this.user.role === 'Company Admin') {
                    this.title = 'Firmen';
                    this.statusLabel = 'Company Status';
                }
            }
        );
    }

    ngOnInit() {
        this.getCities();
        this.getSettings();
    }

    ngDoCheck() {
        if (JSON.stringify(this.settings) !== JSON.stringify(this.settingsOld)) {
            this.showSubmit = true;
        } else {
            this.showSubmit = false;
        }
    }

    ngOnDestroy() {
        this.profileSubscriber.unsubscribe();
    }

    private getSettings() {
        this.http.getRequest(`settings`).subscribe(
            res => {
                this.settings = new PortalSettings(res.payload);

                this.settingsOld = new PortalSettings(res.payload);

                this.selectedColor = res.payload.color;

                this.logo.imageUrl = res.payload.logo;

            },
            error => {
                this.toasterService.pop('error', '', error);
            }
        );
    }

    //Colors
    public colors: Array<string> = [
        '#50e3c2',
        '#6fc213',
        '#f9a110',
        '#4a90e2',
        '#ec4640',
        '#cc56e4',
        '#b8e986',
        '#91b6e0',
        '#ffabb5',
        '#d1935c'
    ];

    getCities() {
        const url = 'geo/cities';
        this.http.getRequest(url).subscribe(res => {
            this.cityList = res.payload;
        });
    }

    public updateColor(): void {
        this.settings.color = this.selectedColor;
    }

    public resetColor(): void {
        this.selectedColor = this.settings.color;
    }

    //Image
    public logo = new ImageUploader(this.toasterService);

    public aktiveToggle() {
        if (this.settings.statusBool) {
            this.settings.status = 'Aktiv';
        } else {
            this.settings.status = 'Inaktiv';
        }
    }

    public update(form: NgForm): void {

        if (form.invalid) {

            Object.keys(form.controls).forEach((key, val) => {
                form.controls[key].markAsDirty();
            });

            return;
        }

        this.aktiveToggle();

        //Reset form validation
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsPristine();
        });

        //Create formData
        let formData = new FormData();

        Object.keys(this.settings).forEach((key, val) => {

            switch (key) {
                case 'logo':

                    if (this.logo.imageData) {
                        formData.append('logo', this.logo.image);
                    } else {
                        formData.append('logo', this.logo.imageUrl);
                    }

                    break;
                case 'leasing_settings':

                    this.settings['leasing_settings'].forEach((item, index) => {
                        for (let i in item) {
                            console.log()
                            formData.append(`${key}[${index}][${i}]`, item[i]);
                        }
                    });

                    break;
                default:
                    if ((typeof this.settings[key]) === 'boolean') {
                        const value = this.settings[key] ? '1' : '0';
                        formData.append(key, value);
                    } else {
                        formData.append(key, this.settings[key]);
                    }

            }
        });

        //send update request
        this.submitting = true;

        this.http.postRequest('settings', formData).subscribe(
            res => {
                this.submitting = false;
                this.toasterService.pop('success', '', 'Settings was updated successfully');

                this.settingsService.settings = res.payload;
                this.settingsService.updateSettings(res.payload);
            },
            error => {
                this.submitting = false;
                this.http.setFormErrors(this.settingsForm, error.error.payload);
            }
        );
    }

    public resetForm(event, form: NgForm) {
        event.preventDefault();
        form.resetForm();
        this.getSettings();
    }
}

class ImageUploader {
    public image;
    public imageUrl;
    public imageData;

    constructor(private toasterService: ToasterService) {

    }

    load(file): void {
        if (file.item(0).type.indexOf('image') === -1) {
            this.toasterService.pop('error', '', 'Please add correct image');
            return;
        };
        this.image = file.item(0);

        const reader = new FileReader();
        reader.onload = (event: any) => {
            this.imageData = event.target.result;
        };
        reader.readAsDataURL(this.image);
    }

    remove(): void {
        this.image = '';
        this.imageUrl = '';
        this.imageData = null;
    }

}
