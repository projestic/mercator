import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from '../../../services/login/login.service';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../services/http/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthSettingsService } from '../../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    public errorMessage: string;
    public logo: string;
    public submitProcess = false;
    public registrationInfo: RegistrationInfo = new RegistrationInfo();
    public urlId: string;

    @ViewChild('registrationForm') registrationForm: HTMLFormElement;

    constructor(public loginService: LoginService,
                public route: ActivatedRoute,
                private router: Router,
                private authSettings: AuthSettingsService,
                public http: HttpConfigService) {
        this.logo = this.loginService.loginLogo;
    }

    ngOnInit() {
    }

    register(form: NgForm) {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;
        const data = form.form.value;
        this.http.postRequest(`registration/register`, data)
            .subscribe(
                res => {
                    this.submitProcess = false;

                    this.errorMessage = `${res.message}!<br>
                            Redirecting to login page in 5`;
                    for (let i = 4; i > 0; i--) {
                        ((index) => {
                            setTimeout(() => {
                                this.errorMessage = `${res.message}!<br>
                                    Redirecting to login page in ${index}`;
                            }, (5 - index) * 1000);
                        })(i);
                    }
                    setTimeout(() => {
                        this.router.navigateByUrl(`${this.authSettings.path}/login`);
                    }, 5000);
                },
                error => {
                    this.http.setFormErrors(this.registrationForm, error.error.payload);
                    this.submitProcess = false;
                }
            );
    }

}

class RegistrationInfo {
    public email: string;
    public first_name: string;
    public last_name: string;
    public password: string;
    public password_confirmation: string;
    public terms: boolean;

    constructor () {}
}
