import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService } from 'angular5-toaster';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../services/http/http.service';
import { Router } from '@angular/router';
import { LoginService } from '../../../services/login/login.service';

@Component({
    selector: 'mrc-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
    @ViewChild('forgotPasswordForm') forgotPasswordForm: HTMLFormElement;
    public logo: string;
    public submitted: boolean = false;
    public title: string = 'Reset password';

    constructor(private toasterService: ToasterService,
                private httpService: HttpConfigService,
                private loginService: LoginService,
                private router: Router) {
        this.logo = this.loginService.loginLogo;
    }

    ngOnInit() {
    }

    forgotPassword(form: NgForm) {
        const data = form.form.value;

        this.httpService.postRequest('password/email', data)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Instructions has been sent. Please check you email');

                    this.submitted = true;
                },
                res => {

                    if (res.status === 422 || res.status === 500) {
                        this.httpService.setFormErrors(this.forgotPasswordForm, res.error.payload);
                    }
                }
            );
    }
}
