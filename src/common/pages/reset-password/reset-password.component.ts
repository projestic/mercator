import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService } from 'angular5-toaster';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../services/http/http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../../services/login/login.service';

@Component({
    selector: 'mrc-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
    @ViewChild('resetPasswordForm') resetPasswordForm: HTMLFormElement;
    private queryParams: object;
    public reset = new Reset();
    public logo;
    public errorMessage: string;

    constructor(private toasterService: ToasterService,
                private httpService: HttpConfigService,
                private router: Router,
                private loginService: LoginService,
                private route: ActivatedRoute) {
        this.logo = this.loginService.loginLogo;
    }

    ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
                this.queryParams = params;
                this.reset = new Reset(this.queryParams);
            });
    }

    resetPassword(form: NgForm) {
        const data = Object.assign(form.form.value, this.queryParams);
        this.httpService.postRequest('password/reset', data)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Passsword has been changed');
                    this.router.navigateByUrl(`${res.payload.redirect_to}/login`);
                },
                error => {
                    if (error.status === 422) {
                        this.httpService.setFormErrors(this.resetPasswordForm, error.error.payload);
                    } else {
                        this.router.navigateByUrl(`${error.error.payload.redirect_to}/password/forgot-password'`);
                    }
                }
            );
    }

}

class Reset {
    public password: string;
    public password_confirmation: string;
    public token: string;
    public email: string;

    constructor( reset? ) {
        if (reset) {
            this.password = reset.password;
            this.password_confirmation = reset.password_confirmation;
            this.token = reset.token;
            this.email = reset.email;
        }
    }
}
