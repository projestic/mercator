import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular5-toaster';
import { ProfileService } from '../../../services/profile/profile.service';
import { Profile } from '../../../common/models/profile';

@Component({
    selector: 'mrc-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
    @ViewChild('account') account: HTMLFormElement;

    public submitProcess = false;
    public hidePassword = true;
    public accountInfo = new Profile();

    constructor(
        private toasterService: ToasterService,
        private httpService: HttpConfigService,
        private profileService: ProfileService) {
        this.getAccountInfo();
    }

    ngOnInit() {
    }

    togglePassword(event) {
        event.preventDefault();
        this.hidePassword = !this.hidePassword;
        if (this.hidePassword) {
            delete this.accountInfo.password;
            delete this.accountInfo.password_confirmation ;
        }
    }

    getAccountInfo() {
        this.httpService.getRequest('profile')
            .subscribe(
                res => {
                    this.accountInfo = new Profile(res.payload);
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;

        this.httpService.postRequest('profile', this.accountInfo)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `Account was update successfully`);
                    form.resetForm();
                    this.getAccountInfo();
                    this.submitProcess = false;
                    this.profileService.profile = res.payload;
                    this.profileService.updateProfile(res.payload);
                },
                res => {
                    if (res.status === 422) {
                        this.httpService.setFormErrors(this.account, res.error.payload);
                    }

                    this.submitProcess = false;
                });
    }
}
