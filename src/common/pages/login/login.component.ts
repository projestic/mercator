import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../../../services/auth/auth.service';
import { NavigationCancel, Router } from '@angular/router';
import { LoginService } from '../../../services/login/login.service';
import { Subscription } from 'rxjs/Rx';
import { AuthSettingsService } from '../../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
    public errorMessage: string;
    public submitProcess = false;
    public logo: string;
    public loginSubscription: Subscription;
    public routerSubscription: Subscription;

    constructor(private authService: AuthenticationService,
                private router: Router,
                public loginService: LoginService,
                private authSettings: AuthSettingsService) {
        this.logo = this.loginService.loginLogo;
    }

    ngOnInit() {
    }

    login(form: NgForm) {
        this.submitProcess = true;
        const data = form.form.value;
        this.loginSubscription = this.authService.login(data)
            .subscribe(res => {
                    this.errorMessage = '';
                    this.submitProcess = false;
                    if (res.payload.user.role === 'Employee') {
                        this.router.navigateByUrl(`${this.authSettings.path}`);
                    } else {
                        this.router.navigateByUrl(`${this.authSettings.path}/admin`);
                    }
                },
                error => {
                    this.errorMessage = error.error.message;
                    this.submitProcess = false;
                    if (error.error.code === 403) {
                        this.errorMessage = `The login details you used are for a different<br> user role,
                            please use the right login form.<br>
                            Redirecting in 5`;
                        for (let i = 4; i > 0; i--) {
                            ((index) => {
                                setTimeout(() => {
                                    this.errorMessage = `The login details you used are for a different<br> user role,
                                    please use the right login form.<br>
                                    Redirecting in ${index}`;
                                }, (5 - index) * 1000);
                            })(i);
                        }
                        let url = error.error.payload.redirect_to;
                        if (!url) url = '/';
                        setTimeout(() => {
                            this.errorMessage = '';
                            window.location.href = location.origin + url;
                        }, 5000);
                    };
                });
    }

    ngOnDestroy() {
        this.errorMessage = '';
    }
}
