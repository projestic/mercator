import { Component, OnInit, Input, Output, ViewChild, TemplateRef, OnChanges, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { SearchService } from '../../../services/search/search.service';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'mrc-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

    public title: string;
    public selectedCategory: any;
    public searchQuery: string;
    public searchCategory: string;

    constructor(
        public searchService: SearchService,
        private route: ActivatedRoute,
        private httpService: HttpConfigService,
    ) {
        this.route.queryParams.subscribe( params => {
            this.searchQuery = params.query;

            if (!this.searchQuery) this.searchService.total = 0;

            if (this.searchService.searchCategory && this.searchService.total) {
                this.searchCategory = this.searchService.searchCategory;
            } else {

                this.httpService.getRequest(`search/live?search=${params.query}`)
                    .pipe(map(
                        (res: any) => {
                            const result = res.payload;
                            return result;
                        }
                        )
                    )
                    .subscribe(
                    res => {
                        if (res.total) {
                            this.searchService.total = res.total;

                            const categories: Array<any> = [];
                            for (let key of Object.keys(res.categories)) {
                                if (res.categories[key].length) {
                                    this.searchCategory = key;
                                    break;
                                }
                            }
                        } else {
                            this.searchCategory = null;
                        }

                    }
                );
            }
        });
    }

    ngOnInit() {

    }

    public getTitle(): string {

        const count = this.searchService.total;
        let title = '';

        if (count == 0) {
            title = `Nothing found ${this.searchQuery ? `for "${this.searchQuery}"` : ''}`;
        } else {
            title = `${count} result ${ count > 1 ? 's' : '' } for "${this.searchQuery}"`;
        }

        return title;
    }
}
