import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CclipboardComponent } from './cclipboard.component';

describe('CclipboardComponent', () => {
  let component: CclipboardComponent;
  let fixture: ComponentFixture<CclipboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CclipboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CclipboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
