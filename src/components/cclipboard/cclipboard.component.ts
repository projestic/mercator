import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mrc-cclipboard',
  templateUrl: './cclipboard.component.html',
  styleUrls: ['./cclipboard.component.scss']
})
export class CclipboardComponent implements OnInit {

  @Input() content: string;
  constructor() { }

  ngOnInit() {
  }

}
