import { Injectable } from '@angular/core';

@Injectable()
export class ConstantsService {

    public categories = {
        contracts: 'Verträge',
        orders: 'Bestellungen',
        offers: 'Offers',
        portals: 'Portale',
        companies: 'Firmen',
        suppliers: 'Lieferanten',
        users: 'Benutzer',
    }

    constructor() {

    }
}
