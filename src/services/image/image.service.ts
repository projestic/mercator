import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable ,  Subscriber } from 'rxjs';

@Injectable()
export class ImageHttpService {
    constructor(
        private http: HttpClient, /* Your favorite Http requester */
    ) {
    }

    get(url: string): Observable<any> {
        return new Observable((observer: Subscriber<any>) => {
            let objectUrl: string = null;

            this.http
                .get(url, {
                    headers: this.getHeaders(),
                    responseType: 'blob'
                })
                .subscribe(m => {
                    objectUrl = URL.createObjectURL(m);
                    observer.next(objectUrl);
                });

            return () => {
                if (objectUrl) {
                    URL.revokeObjectURL(objectUrl);
                    objectUrl = null;
                }
            };
        });
    }

    getHeaders(): HttpHeaders {
        const headers = new HttpHeaders();

        headers.set('Application-Key', '123');

        return headers;
    }
}
