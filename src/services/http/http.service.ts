import {throwError as observableThrowError } from 'rxjs';
import { Observable, Subject } from 'rxjs/Rx';
import {map, catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {
    HttpClient,
    HttpHeaders,
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import fileSaver from 'file-saver';
import { ToasterService } from 'angular5-toaster';
import { InterceptSettingsService } from './intercept-settings.service';


@Injectable()
export class HttpConfigService implements HttpInterceptor {

    constructor(public http: HttpClient,
                public cookie: CookieService,
                public toaster: ToasterService,
                private interceptService: InterceptSettingsService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let apiReq;
        const data = this.interceptService.settings;

        apiReq = req.clone({
            url: req.url.indexOf(`/${data.url}/`) === -1 ? `/${data.url}/${req.url}` : req.url,
            headers: new HttpHeaders({
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.cookie.get(data.token)}`
            })
        });

        if (data.companyName) {
            apiReq = req.clone({
                url: req.url.indexOf(`/${data.url}/`) === -1 ? `/${data.url}/${req.url}` : req.url,
                headers: new HttpHeaders({
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${this.cookie.get(data.token)}`,
                    'Company-Slug': data.companyName
                })
            });
        }

        return next.handle(apiReq);
    }

    public httpRequestStrategy(fn, retryNumb = 0) {
        return fn.pipe(
            retry(retryNumb),
            catchError(this.handleError.bind(this))
        );
    }

    private handleError(error: HttpErrorResponse) {

        let toasterErrorMsg = 'Something bad happened; please try again later.';

        if (error && error.error && error.error.message) {
            toasterErrorMsg = error.error.message;
        }

        return observableThrowError(error || {});
    }


    public setFormErrors(form, errors, prefix = ''): void {

        for (let key in errors) {
            const fieldName = `${prefix}${key}`;
            const msg: Array<string> = [];

            if (typeof errors[key][0] === 'string') {

                if (typeof errors[key] === 'object') {
                    for (let errorText of errors[key]) {
                        msg.push(errorText);
                    }
                } else {
                    msg.push(errors[key]);
                }


                let field = form.controls[fieldName];

                if (field) {
                    field.setErrors({'incorrect': msg.join('<br>')});
                } else {
                    this.toaster.pop('error', '', msg.join('<br>'));
                }
            } else {

                for (let key2 in errors[key]) {
                    this.setFormErrors(form, errors[key][key2], `${fieldName}_${key2}_`);
                }

            }

        }

    }

    getRequest(url, options = {}) {
        const func = this.http.get(url, options);
        return this.httpRequestStrategy(func);
    }

    postRequest(url, data?, options = {}) {
        const func = this.http.post(url, data, options);
        return this.httpRequestStrategy(func);
    }

    deleteRequest(url, options = {}) {
        const func = this.http.delete(url, options);
        return this.httpRequestStrategy(func);
    }

    getFileRequest(url, options = {}, type: string = 'application/csv') {
        return this.http.get(url, options).pipe(map(
            (res: Response) => {
                return {
                    headers: res.headers,
                    body: new Blob([res.body], {type})};
            }));
    }

    public export(route: string, filename: string = 'export') {
        const url = `${route}/export`;

        const options = {
            responseType: 'arraybuffer',
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8',
            },
            observe: 'response'
        };

        const type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

        this.getFileRequest(url, options, type).subscribe(res => {
            filename = filename + '.' + res.headers.get('content-disposition').match(/filename=(.+)/)[1].slice(0, -1).split('.')[1];
            fileSaver.saveAs(res.body, filename);
        });
    }
}
