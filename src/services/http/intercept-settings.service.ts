import { Injectable } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs/Rx';

@Injectable()
export class InterceptSettingsService {
    public settings: {url: string, token: string, companyName?: string};
    public settingsSubject = new ReplaySubject();

    public error: any;

    constructor() {
        this.settingsSubject.subscribe((value: {url: string, token: string, companyName?: string}) => {
            this.settings = value;
        });
    }

    updateSettings(value) {
        this.settingsSubject.next(value);
    }

}
