import { ModuleWithProviders, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpConfigService } from './http.service';
import { InterceptSettingsService } from './intercept-settings.service';

@NgModule({})

export class HttpConfigModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: HttpConfigModule,
            providers: [
                InterceptSettingsService,
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: HttpConfigService,
                    multi: true,
                },
                HttpConfigService,
            ]
        };
    }
}
