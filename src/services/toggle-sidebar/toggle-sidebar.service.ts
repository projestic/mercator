import { Injectable } from '@angular/core';
import { Subject , Observable } from 'rxjs';

@Injectable()
export class ToggleSidebarService {
    public sidenav: any;
    private sidenavChange = new Subject<any>();

    constructor() {
        this.sidenavChange.subscribe(value => {
            this.sidenav = value;
        });
    }

    getSidenav(): Observable<any> {
        return this.sidenavChange.asObservable();
    }

    updateSidenav(data) {
        this.sidenavChange.next(data);
    }

}
