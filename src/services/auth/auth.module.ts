import { NgModule } from '@angular/core';
import {
    AuthModule,
    AUTH_SERVICE
} from 'ngx-auth';

import { TokenStorageService } from './token-storage.service';
import { AuthenticationService } from './auth.service';
import { AuthSettingsService } from './auth-settings.service';

export function factory(authenticationService: AuthenticationService) {
    return authenticationService;
}

@NgModule({
    imports: [ AuthModule ],
    providers: [
        TokenStorageService,
        AuthenticationService,
        {
            provide: AUTH_SERVICE,
            deps: [ AuthenticationService ],
            useFactory: factory
        }
    ]
})
export class AuthenticationModule {}
