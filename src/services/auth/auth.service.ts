import { throwError as observableThrowError, of as observableOf, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, tap } from 'rxjs/operators';
import { Inject, Injectable, Optional } from '@angular/core';
import { AuthService } from 'ngx-auth';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TokenStorageService } from './token-storage.service';
import { CookieService } from 'ngx-cookie-service';
import { ReplaySubject } from 'rxjs/Rx';
import { AuthSettingsService } from './auth-settings.service';
import 'url-search-params-polyfill';

interface AccessData {
    accessToken: string;
    accessTokenExp: string;
    role: string;
    tokenKey: string;
}

interface Res {
    code: number;
    message: string;
    payload: any;
}

@Injectable()
export class AuthenticationService implements AuthService {
    public isAuthSubject = new ReplaySubject<boolean>();
    public tokenKey;

    constructor(private cookie: CookieService,
                private http: HttpClient,
                private tokenStorage: TokenStorageService,
                private router: Router,
                private authSettings: AuthSettingsService,
                @Optional() @Inject('MODULE_TOKEN') private moduleToken?: any) {
    }

    public login(formData): Observable<any> {
        return this.http.post(`login`, formData).pipe(
            tap((res: Res) => {
                const accessToken = res.payload.token.access_token;
                const accessTokenExp = res.payload.token.expires_in;
                const role = res.payload.user.role;
                this.saveAccessData({accessToken, accessTokenExp, role, tokenKey: this.authSettings.token});
            })
        );
    }

    public isAuthorized(): Observable<boolean> {
        const parameters = new URLSearchParams(window.location.search);
        if (parameters.get('access_token')) {
            this.tokenStorage.clear('access_token');
            this.saveAccessData({
                accessToken: parameters.get('access_token'),
                accessTokenExp: parameters.get('expire_in'),
                role: 'Portal Admin',
                tokenKey: 'access_token'}
            );
            this.router.navigateByUrl(`/`);
        }
        const isAuthorized: boolean = this.cookie.check(this.moduleToken);
        this.isAuthSubject.next(isAuthorized);
        return observableOf(isAuthorized);
    }

    public getAccessToken(): Observable<string> {
        return this.tokenStorage.getAccessToken(this.authSettings.token);
    }

    public refreshToken(): Observable<any> {
        const data = {};
        return this.http.post('refresh', data).pipe(
            tap((res: Res) => {
                const accessToken = res.payload.access_token;
                const accessTokenExp = res.payload.expires_in;
                const role = '';
                this.saveAccessData({accessToken, accessTokenExp, role, tokenKey: this.authSettings.token});
            }),
            catchError((err) => {
                this.logout();
                return observableThrowError(err);
            })
        );
    }

    public refreshShouldHappen(response: HttpErrorResponse): boolean {
        if (response.status === 403 && response.error.message === 'Wrong route') {
            this.router.navigateByUrl(`${response.error.payload.redirect_to}`)
        }
        return response.status === 401;
    }

    public verifyTokenRequest(url: string): boolean {
        return url.endsWith('refresh');
    }

    public logout() {
        this.tokenStorage.clear(this.authSettings.token);
        this.router.navigateByUrl(`${this.authSettings.path}/login`);
    }

    private saveAccessData({accessToken, accessTokenExp, role, tokenKey}: AccessData) {
        this.tokenStorage
            .setAccessToken(accessToken, role, tokenKey)
            .setAccessTokenExp(accessTokenExp, role, tokenKey);
    }
}
