import { of as observableOf, Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationService } from './auth.service';
import { AuthSettingsService } from './auth-settings.service';

@Injectable()
export class TokenStorageService {

    constructor(private cookie: CookieService,
                private authSettings: AuthSettingsService) {
    }

    public getAccessToken(tokenKey): Observable<string> {
        const accessToken: string = <string>this.cookie.get(`${tokenKey}`);
        return observableOf(accessToken);
    }

    public getAccessTokenExp(tokenKey): Observable<string> {
        const accessTokenExp: string = <string>this.cookie.get(`${tokenKey}_exp`);
        return observableOf(accessTokenExp);
    }

    public setAccessToken(token: string, role: string, tokenKey: string): TokenStorageService {
        if (role === 'Employee') {
            this.cookie.set(`employee_access_token`, token, 0, '/');
        } else {
            this.cookie.set(`${tokenKey}`, token, 0, '/');
        }
        return this;
    }

    public setAccessTokenExp(token: string, role: string, tokenKey: string): TokenStorageService {
        const expire = Date.now() + (+token * 1000);
        if (role === 'Employee') {
            this.cookie.set(`employee_access_token_exp`, token, new Date(expire), '/');
        } else {
            this.cookie.set(`${tokenKey}_exp`, token, new Date(expire), '/');
        }
        return this;
    }

    public clear(tokenKey) {
        this.cookie.delete(tokenKey, '/');
        this.cookie.delete(`${tokenKey}_exp`, '/');
    }
}
