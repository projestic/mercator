import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/Rx';

@Injectable()
export class AuthSettingsService {
    public path: string;
    public pathSubject = new ReplaySubject<string>();

    public token: string;
    public tokenSubject = new ReplaySubject<string>();

    public currentModule: string;
    public currentModuleSubject = new ReplaySubject<string>();

    constructor() {
        this.pathSubject.subscribe((value) => {
            this.path = value;
        });

        this.tokenSubject.subscribe((value) => {
            this.token = value;
        });

        this.currentModuleSubject.subscribe((value) => {
            this.currentModule = value;
        });
    }

    updatePath(value) {
        this.pathSubject.next(value);
    }

    updateToken(value) {
        this.tokenSubject.next(value);
    }

    updateCurrentModule(value) {
        this.currentModuleSubject.next(value);
    }

}
