import { Injectable } from '@angular/core';
import { HttpConfigService } from '../http/http.service';
import { Subject ,  Observable } from 'rxjs';
import { Profile } from '../../common/models/profile';

@Injectable()
export class ProfileService {
    private _profile;

    public profileObj: Subject<any> = new Subject<any>();

    get profile() {
        return this._profile;
    }

    set profile(opt) {
        this._profile = new Profile(opt);
    }

    constructor(
        public http: HttpConfigService) {

        this.getProfile();

        this.profileObj.subscribe(
            value => {
                this.profile = value;
            },
            error => {
                console.log(error);
            });
    }

    getProfileObj(): Observable<any> {
        return this.profileObj.asObservable();
    }

    updateProfile(data) {
        this.profileObj.next(data);
    }

    getProfile(): void {
        this.http.getRequest(`profile`).subscribe(
            res => {
                this.profile = new Profile(res.payload);
                this.updateProfile(res.payload);
            },
            error => {
                this.profile = null;
            }
        );
    }

}
