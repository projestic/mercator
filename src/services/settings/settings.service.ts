import { Injectable } from '@angular/core';
import { HttpConfigService } from '../http/http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AdminSettings } from '../../common/models/admin-settings';
import { Subject ,  Observable } from 'rxjs/Rx';

@Injectable()
export class SettingsService {
    private _settings;

    public settingObj: Subject<any> = new Subject<any>();

    get settings() {
        return this._settings;
    }

    set settings(opt) {
        this._settings = new AdminSettings(opt);
    }

    constructor(
        private http: HttpConfigService,
        private domSanitizer: DomSanitizer
    ) {
        const DEFAULT_SETTINGS: AdminSettings = new AdminSettings({
            color: '#000',
            date_format: 'F j, Y',
            domain: '',
            email: '',
            logo: '',
            time_format: 'g:i a',
            timezone: 'Europe/London',
            week_starts_on: 'Monday'
        });

        this.settings = new AdminSettings(DEFAULT_SETTINGS);

        if (location.href.indexOf(`admin`) !== -1) this.getSettings();

        this.settingObj.subscribe(value => {
            this.settings = value;
        });
    }

    getSettingsObj(): Observable<any> {
        return this.settingObj.asObservable();
    }

    updateSettings(data) {
        this.settingObj.next(data);
    }

    public getStyles() {
        const styles = this.createStyles(this.settings);
        return this.domSanitizer.bypassSecurityTrustHtml(styles);
    }

    private createStyles(options: AdminSettings): string {
        const color = options.color;

        return `<style>
            a {
                color: ${color};
            }

            a:hover,
            .link-default:hover {
                color: ${this.lightenDarkenColor(color, -10)};
            }

            .text-primary {
                color: ${color} !important;
            }

            .bg-primary {
                background-color: ${color} !important;
            }

            .btn-primary:hover {
                background-color: ${this.lightenDarkenColor(color, 10)};
                border-color: ${this.lightenDarkenColor(color, 10)};
            }

            .btn-primary,
            .btn-primary.disabled,
            .btn-primary:disabled {
                background-color: ${color};
                border-color: ${color};
            }

            .btn-primary:not(:disabled):not(.disabled):active, 
            .btn-primary:not(:disabled):not(.disabled).active, 
            .show > .btn-primary.dropdown-toggle {
                background-color: ${this.lightenDarkenColor(color, -10)};
                border-color: ${this.lightenDarkenColor(color, -10)};
            }

            .btn.btn-secondary:not(:disabled):active:focus, 
            .btn.btn-secondary:not(:disabled):active, 
            .btn.btn-secondary:not(:disabled):focus, 
            .btn.btn-secondary:not(:disabled):hover, 
            .btn.btn-default:not(:disabled):active:focus, 
            .btn.btn-default:not(:disabled):active, 
            .btn.btn-default:not(:disabled):focus, 
            .btn.btn-default:not(:disabled):hover {
                color: ${color} !important;
            }

            .btn.btn-secondary:not(:disabled):not(.disabled):active, 
            .btn.btn-secondary:not(:disabled):not(.disabled).active, 
            .show > .btn.btn-secondary.dropdown-toggle, 
            .btn.btn-default:not(:disabled):not(.disabled):active, 
            .btn.btn-default:not(:disabled):not(.disabled).active, 
            .show > .btn.btn-default.dropdown-toggle {
                background-color: ${this.lightenDarkenColor(color, 70)};
                border-color: ${this.lightenDarkenColor(color, 70)};
            }

            .mat-radio-button.mat-accent .mat-radio-inner-circle {
                background-color: ${color} !important;
            }


            .mat-radio-button.mat-accent.mat-radio-checked  .mat-radio-outer-circle {
                border-color: ${color} !important;
            }

            .mat-radio-button.mat-accent.mat-radio-checked .mat-radio-inner-circle {
                background-color: ${color} !important;
            }

            .mat-radio-button.mat-accent .mat-radio-ripple .mat-ripple-element {
                background-color: ${color} !important;
                opacity: .26;
            }

            .mat-checkbox-indeterminate.mat-accent .mat-checkbox-background, .mat-checkbox-checked.mat-accent .mat-checkbox-background {
                background-color: ${color} !important;
            }

            .mat-checkbox.mat-accent .mat-checkbox-ripple .mat-ripple-element {
                background-color: ${color} !important;
                opacity: .26;
            }


            .ng-option.marked {
                color: ${color} !important;
            }

            .mat-tab-group.mat-primary .mat-ink-bar, 
            .mat-tab-nav-bar.mat-primary .mat-ink-bar {
                background-color: ${color};
            }

            .mat-slide-toggle.mat-accent.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {
                background-color: ${color};
            }

            .mat-slide-toggle .mat-ripple-element {
                background-color: ${this.hexToRgbA(color, 0.12)};
            }

            .ngx-datatable.mrc-datatable .progress-linear .container {
                background-color: ${this.hexToRgbA(color, 0.3)};
            }

            .ngx-datatable.mrc-datatable .progress-linear .container .bar {
                background-color: ${color};
            }

            .sidebar-menu-block-item a:hover,
            .sidebar-menu-block-item a.active {
                color: ${color};
            }

            .ng-star-inserted .select-start:before,
            .ng-star-inserted .in-range:before,
            .ng-star-inserted .select-end:before {
                background-color: ${color} !important;
            }

            .ng-star-inserted .is-highlighted:before {
                background-color: ${this.hexToRgbA(color, 0.3)} !important;
            }

            .ng-star-inserted .is-highlighted {
                color: ${color} !important;
            }

            .ng-star-inserted .select-start:after,
            .ng-star-inserted .select-end:after {
                background-color: ${color} !important;
            }

            .ng-star-inserted .is-highlighted:after {
                background-color: transparent !important;
            }

            .years .is-highlighted {
                border-color: ${color} !important;
            }
            .mat-progress-spinner circle, 
            .mat-spinner circle {
                stroke: ${color} !important;
            }


            input[type=radio]:checked + .chart,
            input[type=radio]:checked + .chart .chart-icon {
                border-color: ${color} !important;
                color: ${color} !important;
            }

            input[type=radio]:checked ~ .chart-label {
                color: ${color} !important;
            }

            .label-source:hover {
                color: ${color} !important;
            }

            .label-source .source-radio:before {
                background: ${color} !important;
            }

            .dropdown-item:active,
            .dropdown-item:hover {
                color: ${color} !important;
                background: transparent !important;
            }

            .preloader i {
                color: ${color} !important;
            }

            .card-icon:hover,
            .card-icon.active {
                color: ${color} !important;
            }

            .flat-rate-message {
                color: ${color} !important;
            }
        </style>`;
    }

    private lightenDarkenColor(col: string, amt: number): string {

        let usePound = false;

        if (col[0] === "#") {
            col = col.slice(1);
            usePound = true;
        }

        const num = parseInt(col,16);

        let r = (num >> 16) + amt;

        if (r > 255) r = 255;
        else if  (r < 0) r = 0;

        let b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) b = 255;
        else if  (b < 0) b = 0;

        let g = (num & 0x0000FF) + amt;

        if (g > 255) g = 255;
        else if (g < 0) g = 0;

        return (usePound ? '#' : '') + ( g | ( b << 8 ) | ( r << 16 )).toString(16);

    }

    private hexToRgbA(hex: string, alpha: number = 1): string {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
            c = hex.substring(1).split('');
            if (c.length== 3){
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',' + alpha + ')';
        }
        throw new Error('Bad Hex');
    }

    public getSettings(): void {
        this.http.getRequest(`settings`).subscribe(
            res => {
                this.settings = new AdminSettings(res.payload);
                this.updateSettings(res.payload);
            },
            error => {
                this.updateSettings(this.settings);
            }
        );
    }
}
