import { Injectable } from '@angular/core';
import { HttpConfigService } from '../http/http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { ToasterService } from 'angular5-toaster/dist';
import { ConstantsService } from '../constants/constants.service';
import { InterceptSettingsService } from '../http/intercept-settings.service';


@Injectable()
export class SearchService extends HttpConfigService {

    public params = new HttpParams()
        .set('page', '1')
        .set('per_page', '10');

    public searchQuery: string = '';
    public total: number = 0;
    public searchCategory: string;
    public loading: boolean =  false;

    public previewSearchResult: any = {};
    public previewTotal: number = 0;

    public categories: object;

    constructor(public http: HttpClient,
                public cookie: CookieService,
                public toaster: ToasterService,
                private interseptService: InterceptSettingsService,
                private constants: ConstantsService) {
        super(http, cookie, toaster, interseptService);

        this.categories = {...this.constants.categories};
    }

    public cleanPreview() {
        this.previewSearchResult = {};
        this.searchQuery = '';
    }
}
