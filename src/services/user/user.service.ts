import {Injectable} from '@angular/core';

@Injectable()
export class UserService {
    public userInfo: object = JSON.parse(localStorage.getItem('userObject'));

    constructor() {
    }

    getUserInfo() {
        return this.userInfo;
    }

}
