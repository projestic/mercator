import { Injectable } from '@angular/core';

export const mainSettings = {
    project: '',
    primaryColor: '',
    applicationKey: '',
    logo: '',
    logoInner: '',
    accountLink: '',
    logoutModal: false
};

export const sidebarMenu: Array<object> = [];

export const tablesColumns = {};

@Injectable()
export class MainSettingsService {
    public tablesColumns: object;
}
