import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpConfigService } from '../../../../services/http/http.service';

@Component({
    selector: 'mrc-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

    constructor(private router: Router,
                public http: HttpConfigService) {
        this.http.getRequest('orders/statuses')
            .subscribe(res => this.statuses = res.payload);
    }

    public statuses: Array<string> = [];

    public filter: Array<any> = [
        {
            name: 'Supplier Name',
            value: 'company'
        },
        {
            name: 'Product Name',
            value: 'product'
        },
        {
            name: 'Employee Name',
            value: 'username'
        }
    ];

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/order', e.row.id]);
        }
    };

    ngOnInit() {}
}
