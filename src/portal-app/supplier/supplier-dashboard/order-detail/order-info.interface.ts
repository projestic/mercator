export interface OrderInfoInterface {
    pickup_code?: string | number;
    code?: string | number;
    email?: string;
    notes?: string | number;
    contract_number?: string | number;
}
