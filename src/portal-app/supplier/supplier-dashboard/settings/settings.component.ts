import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm} from '@angular/forms';
import { ToasterService } from 'angular5-toaster';
import { HttpConfigService } from '../../../../services/http/http.service';
import { forIn, find } from 'lodash';
import { SettingsService } from '../../../../services/settings/settings.service';
import { SupplierSettings } from '../../../../common/models/supplier-settings';

@Component({
    selector: 'mrc-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
    @ViewChild('settingsForm') settingsForm: HTMLFormElement;

    public settings: SupplierSettings = new SupplierSettings({});
    public selectedColor: string;
    public submitting: boolean = false;

    constructor(
        private http: HttpConfigService,
        private toasterService: ToasterService,
        private settingsService: SettingsService
    ) {

    }

    ngOnInit() {
        this.getSettings();
    }

    private getSettings() {
        this.http.getRequest(`settings`).subscribe(
            res => {
                this.settings = new SupplierSettings(res.payload);

                this.selectedColor = res.payload.color;

                this.logo.imageUrl = res.payload.logo;

            },
            error => {
                this.toasterService.pop('error', '', error);
            }
        );
    }

    //Colors
    public colors: Array<string> = [
        '#50e3c2',
        '#6fc213',
        '#f9a110',
        '#4a90e2',
        '#ec4640',
        '#cc56e4',
        '#b8e986',
        '#91b6e0',
        '#ffabb5',
        '#d1935c'
    ];

    public updateColor(): void {
        this.settings.color = this.selectedColor;
    }

    public resetColor(): void {
        this.selectedColor = this.settings.color;
    }

    //Image
    public logo = new ImageUploader(this.toasterService);

    public update(form: NgForm): void {
        console.log(this.settings);
        if (form.invalid) {

            Object.keys(form.controls).forEach((key, val) => {
                form.controls[key].markAsDirty();
            });

            return;
        }

        //Reset form validation
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsPristine();
        });

        //Create formData
        let formData = new FormData();

        Object.keys(this.settings).forEach((key, val) => {

            switch (key) {
                case 'logo':
                    if (this.logo.imageData) {
                        formData.append('logo', this.logo.image);
                    } else {
                        formData.append('logo', this.logo.imageUrl);
                    }

                    break;
                default:
                    formData.append(key, this.settings[key]);
            }
        });

        //send update request
        this.submitting = true;

        this.http.postRequest('settings', formData).subscribe(
            res => {
                this.submitting = false;
                this.toasterService.pop('success', '', 'Settings was updated successfully');

                this.settingsService.settings = res.payload;
                this.settingsService.updateSettings(res.payload);
            },
            res => {
                this.submitting = false;

                if (res.status === 422) {
                    this.http.setFormErrors(this.settingsForm, res.error.payload);
                }
            }
        );
    }
 }

class ImageUploader {
    public image;
    public imageUrl;
    public imageData;

    constructor(private toasterService: ToasterService) {

    }

    load(file): void {
        console.log(file);
        if (file.item(0).type.indexOf('image') === -1) {
            this.toasterService.pop('error', '', 'Please add correct image');
            return;
        };
        this.image = file.item(0);

        const reader = new FileReader();
        reader.onload = (event: any) => {
            this.imageData = event.target.result;
        };
        reader.readAsDataURL(this.image);
    }

    remove(): void {
        this.image = '';
        this.imageUrl = '';
        this.imageData = null;
    }

}
