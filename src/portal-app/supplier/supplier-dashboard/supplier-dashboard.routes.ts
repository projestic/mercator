import { DashboardTemplateComponent } from '../../../common/components/dashboard-template/dashboard-template.component';
import { AccountComponent } from '../../../common/pages/account/account.component';
import { SettingsComponent } from './settings/settings.component';
import { SearchComponent } from '../../../common/pages/search/search.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrdersComponent } from './orders/orders.component';
import { OffersComponent } from './offers/offers.component';
import { UsersSupplierComponent } from './users/users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { CreateOfferComponent } from './create-offer/create-offer.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';

export const routes = [
    {
        path: '',
        component: DashboardTemplateComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'orders',
                component: OrdersComponent
            },
            {
                path: 'order',
                children: [
                    {
                        path: ':id/:number',
                        component: OrderDetailComponent
                    }
                ]
            },
            {
                path: 'offers',
                component: OffersComponent
            },
            {
                path: 'offer/:id/:number',
                component: OfferDetailComponent
            },
            {
                path: 'users',
                component: UsersSupplierComponent
            },
            {
                path: 'add-user',
                component: AddUserComponent
            },
            {
                path: 'account',
                component: AccountComponent
            },
            {
                path: 'settings',
                component: SettingsComponent
            },
            {
                path: 'search',
                component: SearchComponent
            },
            {
                path: 'create-offer',
                component: CreateOfferComponent
            },
        ]
    },
];
