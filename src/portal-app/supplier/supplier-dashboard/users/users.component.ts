import { Component, OnInit } from '@angular/core';
import { TableService } from '../../../../common/components/table/table.service';


@Component({
    selector: 'mrc-users-supplier',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersSupplierComponent implements OnInit {
    public statuses: Array<string> = [
        'Aktiv',
        'Inaktiv'
    ];

    constructor(
        private tableService: TableService) {

    }

    ngOnInit() {

    }

    public print(): void {
        window.print();
    }

    public export(): void {
        this.tableService.export('users', 'Benutzer');
    }
}
