import { Component, OnChanges, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../../services/http/http.service';
import { UserInfo } from './user-info';
import { ToasterService } from 'angular5-toaster';

@Component({
    selector: 'mrc-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit, OnChanges {
    @ViewChild('addUser') addUser: HTMLFormElement;

    public submitProcess = false;

    public userInfo = new UserInfo();

    constructor(private httpService: HttpConfigService,
                private toasterService: ToasterService) {}

    ngOnInit() {
    }

    ngOnChanges() {
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;

        this.httpService.postRequest('users', this.userInfo)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `User was added successfully`);
                    form.resetForm();
                    this.userInfo = new UserInfo();
                    this.submitProcess = false;
                },
                error => {

                    if (error.status === 422) {
                        this.httpService.setFormErrors(this.addUser, error.error.payload);
                    }

                    this.submitProcess = false;
                });
    }
}
