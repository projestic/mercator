import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Product, SupplierOffer, SupplierOfferInterface, User } from './supplier-offer';
import { HttpConfigService } from '../../../../services/http/http.service';
import { ProfileService } from '../../../../services/profile/profile.service';
import { ToasterService } from 'angular5-toaster';

@Component({
    selector: 'mrc-create-offer',
    templateUrl: './create-offer.component.html',
    styleUrls: ['./create-offer.component.scss']
})
export class CreateOfferComponent implements OnInit {
    @ViewChild('createOfferForm') createOfferForm: HTMLFormElement;
    public title = 'Manuelle Anfrage';
    public supplierOffer: SupplierOfferInterface = new SupplierOffer({});

    public submitProcess = false;
    public minDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    public maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1));

    public companiesList: Array<any>;
    public brandList: Array<any>;
    public modelList: Array<any>;
    public attributesList: Array<any> = [];
    public categoriesList = [
        'Fahrrad',
        'Pedelac',
        'S-Pedelac'
    ];

    constructor(private http: HttpConfigService,
                private profileService: ProfileService,
                private toasterService: ToasterService) {
        this.profileService.getProfile();
        this.profileService.getProfileObj().subscribe(res => {
            const user = new User({name: `${res.first_name} ${res.last_name}`});
            this.supplierOffer = {...this.supplierOffer, user};
        });
    }

    ngOnInit() {
        this.getCompaniesList();
        this.getBrandsList();
        this.getAttributesList();
    }

    create(form: NgForm) {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });

        if (form.invalid) return;

        this.submitProcess = true;

        if ((typeof this.supplierOffer.product.brand_id) == 'string') {
            this.supplierOffer.product.brand = this.supplierOffer.product.brand_id;
            delete this.supplierOffer.product.brand_id;
        }

        if ((typeof this.supplierOffer.product.model_id) == 'string') {
            this.supplierOffer.product.model = this.supplierOffer.product.model_id;
            delete this.supplierOffer.product.model_id;
        }

        if (!this.supplierOffer.options.accessories_price) {
            this.supplierOffer.options.notes = null;
        }

        this.http.postRequest('offers', this.supplierOffer)
            .subscribe(
                res => {
                    this.submitProcess = false;
                    this.toasterService.pop('success', '', 'Offer was created successfully');
                    form.resetForm();
                    this.getBrandsList();
                    const attributes = this.attributesList.map(item => {
                        return {
                            id: item.id,
                            value: null
                        };
                    });
                    const product = new Product({attributes});
                    const user = new User({name: `${this.profileService.profile.first_name} ${this.profileService.profile.last_name}`});
                    setTimeout(() => {
                        this.supplierOffer = new SupplierOffer({user, product});
                    }, 0);
                },
                error => {
                    this.submitProcess = false;
                    if (error.status === 422) {
                        const errorObj = {};
                        const res = {...error.error.payload};
                        for (let item in res) {
                            for ( let i in res[item]) {
                                errorObj[i] = res[item][i];
                            }
                        }
                        this.http.setFormErrors(this.createOfferForm, errorObj);
                    }
                }
            );
    }

    public getCompaniesList() {
        this.http.getRequest('companies')
            .subscribe(res => {
                this.companiesList = [{
                    id: null,
                    name: 'No selection'
                }];
                this.companiesList = this.companiesList.concat(res.payload);
            });
    }

    public getBrandsList() {
        this.http.getRequest('product-brands/all')
            .subscribe(res => this.brandList = res.payload);
    }

    public getModelsList(params?: {brand_id?: string | number, category_id: number}) {
        const query = '?' + Object.keys(params).map(key => key + '=' + params[key]).join('&');

        if (params.brand_id === null || params.brand_id === undefined || typeof params.brand_id === 'string') {
            this.modelList = [];
            return false;
        }

        this.http.getRequest(`product-models/all${query}`)
            .subscribe(
                res => this.modelList = res.payload,
                error => this.modelList = []);
    }

    public getAttributesList() {
        this.http.getRequest('product-attributes/all')
            .subscribe(res => {
                const attributes = res.payload.map(item => {
                    return {
                        id: item.id,
                        value: null
                    };
                });
                const product = new Product({attributes});
                this.supplierOffer = {...this.supplierOffer, product};
                this.attributesList = res.payload;
            });
    }

    public addTag(name) {
        return { name: name, tag: true, id: name };
    }

    public addTagAttribute(name) {
        return { value: name, tag: true, label: name };
    }

    public getModelsAfterChanges() {
        this.supplierOffer.product.model_id = null;
        this.getModelsList({ brand_id: this.supplierOffer.product.brand_id, category_id: this.supplierOffer.product.category_id });
    }

    public emailReset() {
        this.createOfferForm.controls.email.setErrors(null);
    }
}
