import { forEach } from 'lodash';

export class SupplierOffer implements SupplierOfferInterface {
    public user: UserInterface = new User({});
    public product: ProductInterface = new Product({});
    public options: OptionsInterface = new Options({});

    constructor({user, product, options}: SupplierOfferInterface) {
        this.user = new User(user || {});
        this.product = new Product(product || {});
        this.options = new Options(options || {});
    }
}

export interface SupplierOfferInterface {
    user?: UserInterface;
    product?: ProductInterface;
    options?: OptionsInterface;
}


export class User implements UserInterface {
    public name?: string;
    public email?: string = null;
    public code?: string = null;
    public company_id?: string | number;

    constructor({name, email, code, company_id}: UserInterface) {
        this.name = name;
        this.email = email || null;
        this.code = code || null;
        this.company_id = company_id || null;
    }
}

interface UserInterface {
    name?: string;
    email?: string;
    code?: string;
    company_id?: string | number;
}

export class Product implements ProductInterface {
    brand: string | number;
    brand_id: string | number;
    category_id = 1;
    model: string | number;
    model_id: string | number;
    attributes: Array<any>;
    description: string = null;

    constructor({brand, brand_id, model, model_id, category_id, attributes, description}: ProductInterface) {
        this.brand = brand;
        this.model = model;
        this.brand_id = brand_id;
        this.model_id = model_id;
        this.category_id = category_id || 1;
        this.attributes = attributes ? [...attributes] : [];
        this.description = description || null;
    }
}

interface ProductInterface {
    brand?: string | number;
    brand_id?: string | number;
    model?: string | number;
    model_id?: string | number;
    category_id?: number;
    attributes?: Array<any>;
    description?: string;
}

class Options implements OptionsInterface {
    normal_price: string | number;
    discount_price: string | number;
    accessories_price: string | number;
    expired_date: string | number = null;
    notes: string = '';

    constructor({normal_price, discount_price, accessories_price, expired_date, notes}: OptionsInterface) {
        this.normal_price = normal_price;
        this.discount_price = discount_price;
        this.accessories_price = accessories_price || null;
        this.expired_date = expired_date || null;
        this.notes = notes || null;
    }
}

interface OptionsInterface {
    normal_price?: string | number;
    discount_price?: string | number;
    accessories_price?: string | number;
    expired_date?: string | number;
    notes?: string;
}
