import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { routes } from './supplier-dashboard.routes';
import { MaterialModule } from '../../material.module';
import { AppCommonModule } from '../../../common/app.common.module';
import { DndModule } from 'ng2-dnd';
import { ColorPickerModule } from 'ngx-color-picker';
import { ClipboardModule } from 'ngx-clipboard';

//Pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrdersComponent } from './orders/orders.component';
import { OffersComponent } from './offers/offers.component';
import { UsersSupplierComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';

//ngx-bootstrap
import { BootstrapModule } from '../../bootstrap.module';
import { SearchService } from '../../../services/search/search.service';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { AddUserComponent } from './add-user/add-user.component';
import { CreateOfferComponent } from './create-offer/create-offer.component';

@NgModule({
    declarations: [
        // Pages
        DashboardComponent,
        OrdersComponent,
        OffersComponent,
        OfferDetailComponent,
        UsersSupplierComponent,
        OrderDetailComponent,
        AddUserComponent,
        SettingsComponent,
        CreateOfferComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        NgSelectModule,
        NgxPaginationModule,
        DndModule.forRoot(),
        ClipboardModule,
        AppCommonModule,
        ColorPickerModule,

        // ngx-bootstrap
        BootstrapModule.forRoot(),
    ],
    providers: [
        SearchService,
    ]
})
export class SupplierDashboardModule {
    public static routes = routes;
}
