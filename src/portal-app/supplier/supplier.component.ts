import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings/settings.service';
import { LoginService } from '../../services/login/login.service';
import { AuthenticationService } from '../../services/auth/auth.service';
import { Subscription } from 'rxjs/Rx';
import {MenuService} from '../../common/components/sidebar/menu.service';
import { TableService } from '../../common/components/table/table.service';
import { CurrencyEuroPipe } from '../../common/datatable-pipes/euro-currency';
import { SidebarSettingsService } from '../../common/components/sidebar/sidebar-settings.service';
import { InterceptSettingsService } from '../../services/http/intercept-settings.service';
import { AuthSettingsService } from '../../services/auth/auth-settings.service';
import { HttpConfigService } from '../../services/http/http.service';

@Component({
    selector: 'mrc-supplier',
    templateUrl: './supplier.component.html',
    styleUrls: ['./supplier.component.scss']
})
export class SupplierModuleComponent implements OnInit, OnDestroy {
    public loginLogo = 'assets/images/logos/supplier_logo.svg';
    public primaryColor = '#61eecc';
    private authSubscription: Subscription;

    private mainMenu = [
        {
            name: 'Allgemein',
            links: [
                {
                    name: 'Dashboard',
                    link: 'dashboard',
                    icon: 'assessment'
                }
            ]
        },
        {
            name: 'Ubersicht',
            links: [
                {
                    name: 'Anfragen',
                    link: 'offers',
                    icon: 'send'
                },
                {
                    name: 'Bestellungen',
                    link: 'orders',
                    icon: 'library_books'
                }
            ]
        },
        {
            name: 'Producte',
            links: [
                {
                    name: 'Angebot erstellen',
                    link: 'create-offer',
                    icon: 'add_circle_outline'
                }
            ]
        },
        {
            name: 'Einstellungen',
            links: [
                {
                    name: 'Benutzer',
                    link: 'users',
                    icon: 'people'
                },
                {
                    name: 'Einstellungen',
                    link: 'settings',
                    icon: 'store'
                },
                {
                    name: 'Mein Konto',
                    link: 'account',
                    icon: 'person'
                }
            ]
        }
    ];

    private tableColumns = {
        // supplier orders
        orders: [
            {
                name: 'ID',
                prop: 'number',
                flexGrow: 20,
            }, {
                name: 'Productname',
                prop: 'product_name',
                flexGrow: 25
            }, {
                name: 'Company',
                prop: 'company_name',
                flexGrow: 25
            }, {
                name: 'Employee',
                prop: 'username',
                flexGrow: 25,
            }, {
                name: 'Status',
                prop: 'status',
                flexGrow: 20
            },
        ],

        // supplier offers
        offers: [
            {
                name: 'ID #',
                prop: 'number',
                flexGrow: 10
            }, {
                name: 'Produktname',
                prop: 'product_name',
                flexGrow: 20
            }, {
                name: 'Vor- und Nachname',
                prop: 'username',
                flexGrow: 25
            }, {
                name: 'Firma',
                prop: 'company_name',
                flexGrow: 20
            }, {
                name: 'Datum',
                prop: 'date',
                flexGrow: 15
            }, {
                name: 'Discount Price',
                prop: 'discount_price',
                flexGrow: 20,
                pipe: new CurrencyEuroPipe('en-US')
            }, {
                name: 'Status',
                prop: 'status',
                flexGrow: 20
            }
        ],

        // supplier users
        users: [{
            name: 'Benutzer-ID #',
            prop: 'code',
            flexGrow: 10
        }, {
            name: 'Vor- und Nachname des Benutzers',
            prop: 'name',
            flexGrow: 20
        }, {
            name: 'E-Mail-Adresse',
            prop: 'email',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 10
        }],
    };

    public sidebarSettings = {
        image: 'assets/images/logos/supplier_inner_logo.svg',
        link: 'account'
    };

    public httpSettings = {
        url: 'supplier-api',
        token: 'supplier_access_token'
    };

    constructor(private settingsService: SettingsService,
                private loginService: LoginService,
                private authService: AuthenticationService,
                private menuService: MenuService,
                private tableService: TableService,
                private sidebarSettingsService: SidebarSettingsService,
                private interseptService: InterceptSettingsService,
                private authSettings: AuthSettingsService) {
        this.authSettings.updatePath('lieferanten');
        this.authSettings.updateToken('supplier_access_token');
        this.authSettings.updateCurrentModule('supplier');
        this.interseptService.updateSettings(this.httpSettings);
        this.loginService.loginLogo = this.loginLogo;
        const settings = this.settingsService.settings;
        this.settingsService.updateSettings({...settings, color: this.primaryColor});
        this.authSubscription = this.authService.isAuthSubject.subscribe(
            res => {
                if (!res) {
                    this.settingsService.updateSettings({...settings, color: this.primaryColor});
                }
            }
        );
        this.menuService.updateMenu(this.mainMenu);
        this.tableService.updateColumns(this.tableColumns);
        this.sidebarSettingsService.updateSettings(this.sidebarSettings);
    }

    ngOnInit() {}

    ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }

}
