import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierModuleComponent } from './supplier.component';
import { routes } from './supplier.routes';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '../../common/app.common.module';
import { AuthenticationModule } from '../../services/auth/auth.module';
import {
    PUBLIC_FALLBACK_PAGE_URI,
    PROTECTED_FALLBACK_PAGE_URI
} from 'ngx-auth';


@NgModule({
    imports: [
        CommonModule,
        AppCommonModule,
        RouterModule.forChild(routes),
        AuthenticationModule,
    ],
    declarations: [SupplierModuleComponent],
    providers: [
        { provide: 'MODULE_TOKEN', useValue: 'supplier_access_token' },
        { provide: PROTECTED_FALLBACK_PAGE_URI, useValue: `/lieferanten` },
        { provide: PUBLIC_FALLBACK_PAGE_URI, useValue: `/lieferanten/login` },
    ]
})
export class SupplierModule {
    public static routes = routes;
}
