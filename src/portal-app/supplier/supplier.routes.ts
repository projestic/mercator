import {LoginComponent} from '../../common/pages/login/login.component';
import {PublicGuard, ProtectedGuard} from 'ngx-auth';
import {ForgotPasswordComponent} from '../../common/pages/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from '../../common/pages/reset-password/reset-password.component';
import {SupplierModuleComponent} from './supplier.component';
import {SupplierDashboardModule} from './supplier-dashboard/supplier-dashboard.module';
import {Page404Component} from '../../common/pages/page-404/page-404.component';

export const routes = [
    {
        path: '',
        component: SupplierModuleComponent,
        children: [
            {
                path: 'admin',
                canActivate: [ProtectedGuard],
                loadChildren: () => SupplierDashboardModule,
                data: {preload: true}
            },
            {
                path: 'login',
                canActivate: [PublicGuard],
                component: LoginComponent
            },
            {
                path: 'password',
                canActivate: [PublicGuard],
                children: [
                    {
                        path: 'forgot-password',
                        canActivate: [PublicGuard],
                        component: ForgotPasswordComponent,
                    },
                    {
                        path: 'reset',
                        canActivate: [PublicGuard],
                        component: ResetPasswordComponent
                    },
                ]
            },
            {
                path: 'register',
                redirectTo: '',
                pathMatch: 'prefix'
            },
            {
                path: '',
                redirectTo: 'admin',
                pathMatch: 'prefix'
            },
        ]
    },
];


