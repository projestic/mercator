import { CompanyComponent } from './company.component';
import { LoginComponent } from '../../common/pages/login/login.component';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';
import { ForgotPasswordComponent } from '../../common/pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../../common/pages/reset-password/reset-password.component';
import { CompanyDashboardModule } from './company-dashboard/company-dashboard.module';
import { RegistrationComponent } from '../../common/pages/registration/registration.component';
import { Page404Component } from '../../common/pages/page-404/page-404.component';

export const routes = [
    {
        path: '',
        component: CompanyComponent,
        children: [
            {
                path: 'admin',
                canActivate: [ProtectedGuard],
                loadChildren: () => CompanyDashboardModule,
                data: {preload: true}
            },
            {
                path: 'login',
                canActivate: [PublicGuard],
                component: LoginComponent
            },
            {
                path: 'password',
                canActivate: [PublicGuard],
                children: [
                    {
                        path: 'forgot-password',
                        canActivate: [PublicGuard],
                        component: ForgotPasswordComponent,
                    },
                    {
                        path: 'reset',
                        canActivate: [PublicGuard],
                        component: ResetPasswordComponent
                    },
                ]
            },
            {
                path: 'register',
                canActivate: [PublicGuard],
                component: RegistrationComponent
            }
        ]
    },
    {
        path: '404',
        component: Page404Component
    }
];


