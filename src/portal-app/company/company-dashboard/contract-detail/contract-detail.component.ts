import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../../services/http/http.service';

import { User } from '../../../../common/models/user';
import { UserInterface } from '../../../../common/models/user.interface';
import { Contract } from '../../../../common/models/contract';
import { ContractInterface } from '../../../../common/models/contract.interface';
import { Leasing } from '../../../../common/models/leasing';
import { LeasingInterface } from '../../../../common/models/leasing.interface';
import { ProductInterface } from '../../../../common/models/product.interface';
import { Product } from '../../../../common/models/product';

@Component({
    selector: 'mrc-contract-detail',
    templateUrl: './contract-detail.component.html',
    styleUrls: ['./contract-detail.component.scss']
})
export class ContractDetailComponent implements OnInit {
    public contractID: number | string;
    public contractTitle: string;

    constructor(
        private route: ActivatedRoute,
        private http: HttpConfigService
    ) {
        this.route.params.subscribe( params => {
            this.contractTitle = 'Vertrag';
            this.getContract(params.id);
        });
    }

    public user: UserInterface;
    public contract: ContractInterface = new Contract();
    public leasing: LeasingInterface;
    public product: ProductInterface;


    ngOnInit() {

    }

    private getContract(id: number) {
        this.http.getRequest(`contracts/${id}`).subscribe(
            res => {
                this.contractID = res.payload.number;
                this.user = new User(res.payload.user);
                this.contract = new Contract(res.payload);
                this.leasing = new Leasing(res.payload);
                this.product = new Product(res.payload.product);
            },
            error => {
                console.log(error);
            }
        );
    }

    public print(): void {
        window.print();
    }

    public export(): void {
        this.http.export(`contracts/${this.contractID}`, `Vertrag #${this.contractID}`);
    }
}
