import { Component, OnInit } from '@angular/core';
import { HttpConfigService } from '../../../../services/http/http.service';
import { TableService } from '../../../../common/components/table/table.service';

@Component({
    selector: 'mrc-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    public statuses: Array<string> = [
        'Offen',
        'Aktiv',
        'Inaktiv'
    ];

    public filter: Array<any> = [
        {
            name: 'Name',
            value: 'name'
        }
    ];

    constructor(private tableService: TableService) {

    }

    public columns = [];

    ngOnInit() {

    }

    public print(): void {
        window.print();
    }

    public export(): void {
        this.tableService.export('users', 'Benutzer');
    }
}
