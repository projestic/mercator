import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'mrc-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {

    constructor(private router: Router) {

    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'Produktname',
            value: 'product'
        },
        {
            name: 'Lieferant',
            value: 'supplier'
        },
        {
            name: 'Status',
            value: 'status',
            type: 'status'
        }
    ];

    public statuses: Array<string> = [
        'Angebot Erhalten',
        'Akzeptiert',
        'Abgelehnt'
    ];

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/offer', e.row.id]);
        }
    };

    ngOnInit() {

    }
}
