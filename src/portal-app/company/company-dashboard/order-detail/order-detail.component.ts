import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../../services/http/http.service';
import { LeasingInterface } from '../../../../common/models/leasing.interface';
import { Leasing } from '../../../../common/models/leasing';
import { ProductInterface } from '../../../../common/models/product.interface';
import { Product } from '../../../../common/models/product';

@Component({
    selector: 'mrc-order-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
    public orderId: string | number;
    public orderTitle = 'Auftrag';
    public orderObj: object;
    public leasing: LeasingInterface = new Leasing();
    public product: ProductInterface = new Product();


    constructor(private route: ActivatedRoute,
                private httpConfig: HttpConfigService) {

        this.route.params.subscribe( params => {
            this.orderId = params.id;
        });
    }

    ngOnInit() {
        if (this.orderId) this.getOrderDetailInfo();
    }

    getOrderDetailInfo() {
        this.httpConfig.getRequest(`orders/${this.orderId}`)
            .subscribe(
                res => {
                    this.orderObj = res.payload;
                    this.leasing = new Leasing(res.payload);
                    this.product = new Product(res.payload.product);
                },
                error => console.log(error)
            );
    }
}
