import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular5-toaster';
import { HttpConfigService } from '../../../../services/http/http.service';

@Component({
    selector: 'mrc-suppliers-list',
    templateUrl: './suppliers-list.component.html',
    styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent implements OnInit {
    @ViewChild('suppliers') suppliers: HTMLFormElement;

    public submitProcess = false;
    public suppliersList = [];

    constructor(
        private toasterService: ToasterService,
        private httpService: HttpConfigService) {
    }

    ngOnInit() {
        this.getSuppliersList();
    }

    getSuppliersList() {
        this.httpService.getRequest('suppliers')
            .subscribe(
                res => {
                    this.suppliersList = res.payload.map(item => item.id);
                    this.suppliers.supplier_ids = this.suppliersList;
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;

        this.httpService.postRequest('suppliers', {ids: this.suppliersList})
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `Suppliers list were update successfully`);
                    form.resetForm();
                    this.getSuppliersList();
                    this.submitProcess = false;
                },
                error => {
                    if (error.status === 422) {
                        this.httpService.setFormErrors(this.suppliers, error.error.payload);
                    }
                    this.submitProcess = false;
                });
    }
}
