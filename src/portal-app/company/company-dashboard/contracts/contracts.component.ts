import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpConfigService } from '../../../../services/http/http.service';

@Component({
  selector: 'mrc-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {
    public statuses: Array<string> = [];

    constructor(private router: Router,
                public http: HttpConfigService) {
        this.http.getRequest('contracts/statuses')
            .subscribe(res => this.statuses = res.payload);
    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'Product Name',
            value: 'product'
        }
    ];

    ngOnInit() {}

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/contract', e.row.id]);
        }
    }
}
