import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../../services/http/http.service';
import { ProductInterface } from '../../../../common/models/product.interface';
import { Product } from '../../../../common/models/product';

@Component({
    selector: 'mrc-offer-detail',
    templateUrl: './offer-detail.component.html',
    styleUrls: ['./offer-detail.component.scss']
})
export class OfferDetailComponent implements OnInit {
    public offerId: string | number;
    public offerTitle = 'Anfrage';
    public offerObj: object;
    public product: ProductInterface = new Product();

    constructor(
        private route: ActivatedRoute,
        private httpConfig: HttpConfigService) {
        this.route.params.subscribe( params => {
            this.offerId = params.id;
        });
    }

    ngOnInit() {
        if (this.offerId) this.getOfferDetailInfo();
    }

    getOfferDetailInfo() {
        this.httpConfig.getRequest(`offers/${this.offerId}`)
            .subscribe(
                res => {
                    this.offerObj = res.payload;
                    this.product = new Product(res.payload.product);
                },
                error => console.log(error)
            );
    }

}
