import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { routes } from './company-dashboard.routes';
import { MaterialModule } from '../../material.module';
import { DndModule } from 'ng2-dnd';
import { ColorPickerModule } from 'ngx-color-picker';
import { ClipboardModule } from 'ngx-clipboard';

//Pages
import { AppCommonModule } from '../../../common/app.common.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { SuppliersListComponent } from './suppliers-list/suppliers-list.component';
import { UsersComponent } from './users/users.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { OffersComponent } from './offers/offers.component';

//ngx-bootstrap
import { BootstrapModule } from '../../bootstrap.module';
import { SearchService } from '../../../services/search/search.service';


@NgModule({
    declarations: [
        //Pages
        DashboardComponent,
        OffersComponent,
        OrdersComponent,
        OrderDetailComponent,
        ContractsComponent,
        ContractDetailComponent,
        SuppliersListComponent,
        UsersComponent,
        OfferDetailComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        NgSelectModule,
        NgxPaginationModule,
        DndModule.forRoot(),
        ClipboardModule,
        AppCommonModule,
        ColorPickerModule,

        //ngx-bootstrap
        BootstrapModule.forRoot(),
    ],
    providers: [
        SearchService,
    ]
})
export class CompanyDashboardModule {
    public static routes = routes;
}
