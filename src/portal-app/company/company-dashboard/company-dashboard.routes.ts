import { DashboardTemplateComponent } from '../../../common/components/dashboard-template/dashboard-template.component';
import { AccountComponent } from '../../../common/pages/account/account.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrdersComponent } from './orders/orders.component';
import { OffersComponent } from './offers/offers.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { SuppliersListComponent } from './suppliers-list/suppliers-list.component';
import { SettingsComponent } from '../../../common/pages/settings/settings.component';
import { UsersComponent } from './users/users.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { SearchComponent } from '../../../common/pages/search/search.component';

export const routes = [
    {
        path: '',
        component: DashboardTemplateComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'account',
                component: AccountComponent
            },
            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'offers',
                component: OffersComponent
            },
            {
                path: 'offer',
                children: [
                    {
                        path: ':id',
                        component: OfferDetailComponent
                    }
                ]
            },
            {
                path: 'orders',
                component: OrdersComponent
            },
            {
                path: 'order',
                children: [
                    {
                        path: ':id',
                        component: OrderDetailComponent
                    }
                ]
            },
            {
                path: 'contracts',
                component: ContractsComponent
            },
            {
                path: 'contract',
                children: [
                    {
                        path: ':id',
                        component: ContractDetailComponent
                    }
                ]
            },
            {
                path: 'suppliers',
                component: SuppliersListComponent
            },
            {
                path: 'settings',
                component: SettingsComponent
            },
            {
                path: 'search',
                component: SearchComponent
            },
        ]
    },
];
