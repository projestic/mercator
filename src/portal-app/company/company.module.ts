import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyComponent } from './company.component';
import { routes } from './company.routes';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '../../common/app.common.module';
import { AuthenticationModule } from '../../services/auth/auth.module';
import {
    PUBLIC_FALLBACK_PAGE_URI,
    PROTECTED_FALLBACK_PAGE_URI
} from 'ngx-auth';

@NgModule({
    imports: [
        CommonModule,
        AppCommonModule.forRoot(),
        RouterModule.forChild(routes),
        AuthenticationModule
    ],
    declarations: [CompanyComponent],
    providers: [
        { provide: 'MODULE_TOKEN', useValue: 'company_access_token' },
        { provide: PROTECTED_FALLBACK_PAGE_URI, useValue: `/${location.pathname.split('/')[1]}/admin/dashboard` },
        { provide: PUBLIC_FALLBACK_PAGE_URI, useValue: `/${location.pathname.split('/')[1]}/login` },
    ]
})

export class CompanyModule {
    public static routes = routes;
}
