import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings/settings.service';
import { LoginService } from '../../services/login/login.service';
import { AuthenticationService } from '../../services/auth/auth.service';
import { Subscription } from 'rxjs/Rx';
import { MenuService } from '../../common/components/sidebar/menu.service';
import { TableService } from '../../common/components/table/table.service';
import { CurrencyEuroPipe } from '../../common/datatable-pipes/euro-currency';
import { SidebarSettingsService } from '../../common/components/sidebar/sidebar-settings.service';
import { HttpConfigService } from '../../services/http/http.service';
import { InterceptSettingsService } from '../../services/http/intercept-settings.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthSettingsService } from '../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnDestroy {
    public loginLogo = 'assets/images/logos/companyadministrator_logo.svg';
    public primaryColor = '#f9a110';
    private authSubscription: Subscription;
    private mainMenu = [
        {
            name: 'Allgemein',
            links: [
                {
                    name: 'Dashboard',
                    link: 'dashboard',
                    icon: 'assessment'
                }
            ]
        },
        {
            name: 'Benutzer',
            links: [
                {
                    name: 'Benutzer Verwalten',
                    link: 'users',
                    icon: 'person'
                }
            ]
        },
        {
            name: 'Verwalten',
            links: [
                {
                    name: 'Anfragen',
                    link: 'offers',
                    icon: 'library_books'
                },
                {
                    name: 'Bestelungen',
                    link: 'orders',
                    icon: 'assignment'
                },
                {
                    name: 'Vertrage',
                    link: 'contracts',
                    icon: 'description'
                }
            ]
        },
        {
            name: 'Einstellungen',
            links: [
                {
                    name: 'Einstellungen',
                    link: 'settings',
                    icon: 'settings'
                },
                {
                    name: 'Lieferanten',
                    link: 'suppliers',
                    icon: 'local_shipping'
                },
                {
                    name: 'Mein Account',
                    link: 'account',
                    icon: 'person'
                }
            ]
        }
    ];

    private tableColumns = {
        // company orders
        orders: [{
            name: 'Bestell-ID',
            prop: 'number',
            flexGrow: 15,
        }, {
            name: 'Benutzername',
            prop: 'username',
            flexGrow: 20
        }, {
            name: 'Productname',
            prop: 'product_name',
            flexGrow: 30
        }, {
            name: 'Lieferant',
            prop: 'supplier_name',
            flexGrow: 20,
            hideOnTablet: true
        }, {
            name: 'Price',
            prop: 'agreed_purchase_price',
            flexGrow: 20,
            pipe: new CurrencyEuroPipe('en-US')
        }, {
            name: 'Leasingrate',
            prop: 'leasing_rate',
            flexGrow: 20,
            pipe: new CurrencyEuroPipe('en-US')
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }, {
            name: 'Abholecode',
            prop: 'pickup_code',
            flexGrow: 15,
            hideOnTablet: true
        }],

        // company contracts
        contracts: [{
            name: 'Vertrag #',
            prop: 'number',
            flexGrow: 10
        }, {
            name: 'Startdatum',
            prop: 'start_date',
            flexGrow: 15
        }, {
            name: 'Enddatum',
            prop: 'end_date',
            flexGrow: 15
        }, {
            name: 'Benutzername',
            prop: 'username',
            flexGrow: 25
        }, {
            name: 'Leaserate',
            prop: 'leasing_rate',
            flexGrow: 20
        }, {
            name: 'Produktname',
            prop: 'product_name',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }],

        // company users
        users: [{
            name: 'ID #',
            prop: 'code',
            flexGrow: 10
        }, {
            name: 'Name',
            prop: 'name',
            flexGrow: 20
        }, {
            name: 'E-Mail-Adresse',
            prop: 'email',
            flexGrow: 20
        }, {
            name: 'Role',
            prop: 'role',
            flexGrow: 20
        }, {
            name: 'Registration date',
            prop: 'created_at',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }, {
            name: 'Actions',
            prop: 'actions',
            flexGrow: 25,
            sortable: false,
        }, {
            cellClass: 'justify-content-center',
            name: ' ',
            prop: 'remove',
            flexGrow: 10,
            sortable: false,
            hideOnTablet: true
        }],

        // company offers
        offers: [{
            name: 'ID #',
            prop: 'number',
            flexGrow: 10
        }, {
            name: 'Benutzer',
            prop: 'username',
            flexGrow: 30
        }, {
            name: 'Produktname',
            prop: 'product_name',
            flexGrow: 20
        }, {
            name: 'Price',
            prop: 'normal_price',
            flexGrow: 10,
            pipe: new CurrencyEuroPipe('en-US')
        }, {
            name: 'Lieferant',
            prop: 'supplier_name',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 15
        }],
    };

    public sidebarSettings = {
        image: 'assets/images/logos/companyadministrator_inner_logo.svg',
        link: 'account'
    };

    public httpSettings = {
        url: 'company-api',
        token: 'company_access_token',
        companyName: ''
    };

    constructor(private settingsService: SettingsService,
                private router: Router,
                private route: ActivatedRoute,
                private loginService: LoginService,
                private authService: AuthenticationService,
                private menuService: MenuService,
                private tableService: TableService,
                public sidebarSettingsService: SidebarSettingsService,
                private interceptService: InterceptSettingsService,
                private authSettings: AuthSettingsService,
                private http: HttpConfigService) {
        this.route.params.subscribe(value => {
            this.httpSettings = {...this.httpSettings, companyName: value.companyName};
            this.interceptService.updateSettings(this.httpSettings);
            this.authSettings.updatePath(value.companyName);
            this.authSettings.updateToken('company_access_token');
            this.authSettings.updateCurrentModule('company');
            this.checkSlug(value.companyName);
        });
        this.loginService.loginLogo = this.loginLogo;
        const settings = this.settingsService.settings;
        this.settingsService.updateSettings({...settings, color: this.primaryColor});
        this.authSubscription = this.authService.isAuthSubject.subscribe(
            res => {
                if (!res) {
                    this.settingsService.updateSettings({...settings, color: this.primaryColor});
                }
            }
        );
        this.menuService.updateMenu(this.mainMenu);
        this.tableService.updateColumns(this.tableColumns);
        this.sidebarSettingsService.updateSettings(this.sidebarSettings);
    }

    checkSlug(slug) {
        this.http.getRequest(`companies/slug-exists?slug=${slug}`)
            .subscribe(res => {
                console.log(res.payload.result);
                if (!res.payload.result) {
                    this.router.navigateByUrl(`404`);
                }
            });
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }
}
