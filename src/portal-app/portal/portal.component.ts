import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings/settings.service';
import { LoginService } from '../../services/login/login.service';
import { AuthenticationService } from '../../services/auth/auth.service';
import { Subscription } from 'rxjs/Rx';
import { MenuService } from '../../common/components/sidebar/menu.service';
import { TableService } from '../../common/components/table/table.service';
import { SidebarSettingsService } from '../../common/components/sidebar/sidebar-settings.service';
import { InterceptSettingsService } from '../../services/http/intercept-settings.service';
import { AuthSettingsService } from '../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-portal',
    templateUrl: './portal.component.html',
    styleUrls: ['./portal.component.scss']
})
export class PortalModuleComponent implements OnInit, OnDestroy {
    public loginLogo = 'assets/images/logos/portal_logo.svg';
    public primaryColor = '#4D91DF';
    private authSubscription: Subscription;

    public sidebarSettings = {
        image: 'assets/images/logos/portal_inner_logo.svg',
        link: 'account'
    };

    private mainMenu = [
        {
            name: 'General',
            links: [
                {
                    name: 'Dashboard',
                    link: 'dashboard',
                    icon: 'assessment'
                }
            ]
        },
        {
            name: 'Firmen',
            links: [
                {
                    name: 'Übersicht',
                    link: 'companies',
                    icon: 'business'
                },
                {
                    name: 'Firma hinzufügen',
                    link: 'create-company',
                    icon: 'add_circle_outline'
                }
            ]
        },
        {
            name: 'Lieferanten',
            links: [
                {
                    name: 'Übersicht',
                    link: 'suppliers',
                    icon: 'store'
                },
                {
                    name: 'Lieferant hinzufügen',
                    link: 'add-supplier',
                    icon: 'add_circle_outline'
                }
            ]
        },
        {
            name: 'Portal',
            links: [
                {
                    name: 'Benutzer Übersicht',
                    link: 'users',
                    icon: 'person'
                },
                {
                    name: 'Einstellungen',
                    link: 'settings',
                    icon: 'settings'
                }
            ]
        },
        {
            name: 'Mein Account',
            links: [
                {
                    name: 'Mein Account',
                    link: 'account',
                    icon: 'person'
                }
            ]
        }
    ];

    private tableColumns = {
        // suppliers
        suppliers: [{
            name: 'ID #',
            prop: 'code',
            flexGrow: 10
        }, {
            name: 'Name',
            prop: 'name',
            flexGrow: 30
        }, {
            name: 'Stadt',
            prop: 'city_name',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 15
        }],

        // companies
        companies: [{
            name: 'Firmen-ID #',
            prop: 'code',
            flexGrow: 15
        }, {
            name: 'Firmenname',
            prop: 'name',
            flexGrow: 20
        }, {
            name: 'Stadt',
            prop: 'city_name',
            flexGrow: 20
        }, {
            name: 'Mitarbeiteranzahl',
            prop: 'employees_count',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 15
        }],

        // users
        users: [{
            name: 'User-ID #',
            prop: 'code',
            flexGrow: 10
        }, {
            name: 'Vor-/Nachname',
            prop: 'name',
            flexGrow: 20
        }, {
            name: 'E-Mail-Adresse',
            prop: 'email',
            flexGrow: 20
        }, {
            name: 'Firma',
            prop: 'company',
            flexGrow: 20
        }, {
            name: 'Benutzerrolle',
            prop: 'role',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }, {
            cellClass: 'justify-content-center',
            name: ' ',
            prop: 'remove',
            flexGrow: 10,
            sortable: false,
            hideOnTablet: true
        }]
    };

    public httpSettings = {
        url: 'api',
        token: 'access_token'
    };

    constructor(private settingsService: SettingsService,
                private loginService: LoginService,
                private authService: AuthenticationService,
                private menuService: MenuService,
                private tableService: TableService,
                private interceptService: InterceptSettingsService,
                private sidebarSettingsService: SidebarSettingsService,
                private authSettings: AuthSettingsService) {
        this.interceptService.updateSettings(this.httpSettings);
        this.authSettings.updatePath('');
        this.authSettings.updateToken('access_token');
        this.authSettings.updateCurrentModule('portal');
        this.loginService.loginLogo = this.loginLogo;
        const settings = this.settingsService.settings;
        this.settingsService.updateSettings({...settings, color: this.primaryColor});
        this.authSubscription = this.authService.isAuthSubject.subscribe(
            res => {
                if (!res) {
                    this.settingsService.updateSettings({...settings, color: this.primaryColor});
                }
            }
        );
        this.menuService.updateMenu(this.mainMenu);
        this.tableService.updateColumns(this.tableColumns);
        this.sidebarSettingsService.updateSettings(this.sidebarSettings);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }
}
