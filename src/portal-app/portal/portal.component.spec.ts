import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalModuleComponent } from './portal.component';

describe('PortalModuleComponent', () => {
  let component: PortalModuleComponent;
  let fixture: ComponentFixture<PortalModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
