import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalModuleComponent } from './portal.component';
import { AppCommonModule } from '../../common/app.common.module';
import { RouterModule } from '@angular/router';
import { routes } from './portal.routes';
import { AuthenticationModule } from '../../services/auth/auth.module';
import {
    PUBLIC_FALLBACK_PAGE_URI,
    PROTECTED_FALLBACK_PAGE_URI
} from 'ngx-auth';

@NgModule({
    imports: [
        CommonModule,
        AppCommonModule.forRoot(),
        RouterModule.forChild(routes),
        AuthenticationModule
    ],
    declarations: [PortalModuleComponent],
    providers: [
        { provide: 'MODULE_TOKEN', useValue: 'access_token' },
        { provide: PROTECTED_FALLBACK_PAGE_URI, useValue: `/` },
        { provide: PUBLIC_FALLBACK_PAGE_URI, useValue: `/login` },
    ]
})
export class PortalModule {
    public static routes = routes;
}
