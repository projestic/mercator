import { PortalModuleComponent } from './portal.component';
import { LoginComponent } from '../../common/pages/login/login.component';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';
import { ForgotPasswordComponent } from '../../common/pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../../common/pages/reset-password/reset-password.component';
import { DashboardTemplateModule } from '../dashboard-template/dashboard-template.module';

export const routes = [
    {
        path: '',
        component: PortalModuleComponent,
        children: [
            {
                path: 'admin',
                canActivate: [ProtectedGuard],
                loadChildren: () => DashboardTemplateModule,
                data: {preload: true}
            },
            {
                path: 'login',
                canActivate: [PublicGuard],
                component: LoginComponent
            },
            {
                path: 'password',
                canActivate: [PublicGuard],
                children: [
                    {
                        path: 'forgot-password',
                        canActivate: [PublicGuard],
                        component: ForgotPasswordComponent,
                    },
                    {
                        path: 'reset',
                        canActivate: [PublicGuard],
                        component: ResetPasswordComponent
                    },
                ]
            },
            {
                path: '',
                redirectTo: 'admin/dashboard',
                pathMatch: 'prefix'
            },
        ]
    },
];
