import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        TooltipModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
    ],
    exports: [
        AlertModule,
        TooltipModule,
        ModalModule,
        BsDropdownModule,
        BsDatepickerModule,
    ]
})
export class BootstrapModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: BootstrapModule,
            providers: [BsModalService]
        };
    }
}

