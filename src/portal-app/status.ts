export class Status {
    create (type) {
        let status;
        if (type === 'active') {
            status = new Active();
        } else if (type === 'inactive') {
            status = new Inactive();
        }
        status.type = type;
    }
}

class Active {
    name: string;
    color: string;
    constructor() {
        this.name = 'Aktiv';
        this.color = 'primary';
    }
}

class Inactive {
    name: string;
    color: string;
    constructor() {
        this.name = 'Inaktiv';
        this.color = 'default';
    }
}
