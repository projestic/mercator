import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TableService } from '../../../common/components/table/table.service';
import { HttpConfigService } from '../../../services/http/http.service';

@Component({
  selector: 'mrc-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {
    public statuses: Array<string> = [];

    public filter: Array<any> = [
        {
            name: 'Product',
            value: 'product'
        }
    ];

    constructor(
        private http: HttpConfigService) {
        this.getStatuses()
    }

    ngOnInit() {}

    getStatuses() {
        this.http.getRequest('contracts/statuses')
            .subscribe(res => this.statuses = res.payload);
    }
}
