import { Component, OnInit } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';

@Component({
    selector: 'mrc-shops',
    templateUrl: './shops.component.html',
    styleUrls: ['./shops.component.scss']
})
export class ShopsComponent implements OnInit {
    public title = 'Fahrradhändler';
    public citiesList: Array<any>;
    public categoriesList: Array<any>;
    public filters: FormGroup;
    public shops: Array<any>;
    public total: number;
    public currentPage: number = 1;
    public defaultLogo = 'assets/images/logos/supplier_logo.svg';

    constructor(private http: HttpConfigService,
                private formBuilder: FormBuilder) {
        this.filters = this.formBuilder.group({
            cities: this.formBuilder.array([]),
            categories: this.formBuilder.array([]),
        });
        this.getCitiesList();
        this.getCategoriesList();
        this.getShopsList(this.currentPage, this.filters.value);
    }

    ngOnInit() {
    }

    getCitiesList() {
        this.http.getRequest('suppliers/cities')
            .subscribe(res => this.citiesList = res.payload);
    }


    getCategoriesList() {
        this.http.getRequest('product-categories/all')
            .subscribe(res => this.categoriesList = res.payload);
    }

    getShopsList(page: number, filters?: {cities: string[], categories: string[] }) {
        let url = `suppliers?page=${page}&per_page=12`;
        let cities = '';
        let categories = '';
        if (filters.cities) {
            cities = filters.cities.map((item, index) => `&cities[${index}]=${item}`).join('');
        }

        if (filters.categories) {
            categories = filters.categories.map((item, index) => `&categories[${index}]=${item}`).join('');
        }

        url = url + cities + categories;

        this.http.getRequest(url)
            .subscribe(res => {
                this.shops = res.payload.data;
                this.total = res.payload.meta.total;
                this.currentPage = res.payload.meta.current_page;
            });
    }

    onCityChange(event) {
        const cities = <FormArray>this.filters.get('cities') as FormArray;

        if (event.checked) {
            cities.push(new FormControl(event.source.value));
        } else {
            const i = cities.controls.findIndex(x => x.value === event.source.value);
            cities.removeAt(i);
        }

        this.getShopsList(this.currentPage, this.filters.value);
    }

    onCategoryChange(event) {
        const categories = <FormArray>this.filters.get('categories') as FormArray;

        if (event.checked) {
            categories.push(new FormControl(event.source.value));
        } else {
            const i = categories.controls.findIndex(x => x.value === event.source.value);
            categories.removeAt(i);
        }

        this.getShopsList(this.currentPage, this.filters.value);
    }

}
