import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { ProductInterface } from '../../../common/models/product.interface';
import { Product } from '../../../common/models/product';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AuthSettingsService } from '../../../services/auth/auth-settings.service';

@Component({
    selector: 'mrc-offer-detail',
    templateUrl: './offer-detail.component.html',
    styleUrls: ['./offer-detail.component.scss']
})
export class OfferDetailComponent implements OnInit {
    public offerId: string | number;
    public offerTitle = 'Anfrage';
    public offerObj: any;
    public product: ProductInterface = new Product();
    public modalRef: BsModalRef;
    public pinned: boolean;
    public rejecting: boolean;
    public accepting: boolean;
    public path: string;

    @ViewChild('reejctOrder') reejctOrder: TemplateRef<any>;
    @ViewChild('acceptOrder') acceptOrder: TemplateRef<any>;

    constructor(
        private route: ActivatedRoute,
        private httpConfig: HttpConfigService,
        private modalService: BsModalService,
        private authSettings: AuthSettingsService,
        private router: Router,
    ) {
        this.route.params.subscribe( params => {
            this.offerId = params.id;
        });

        this.path = this.authSettings.path;
    }

    ngOnInit() {
        if (this.offerId) this.getOfferDetailInfo();
    }

    getOfferDetailInfo() {
        this.httpConfig.getRequest(`offers/${this.offerId}`)
            .subscribe(
                res => {
                    this.offerObj = res.payload;
                    this.product = new Product(res.payload.product);

                    this.pinned = this.offerObj.status === 'Angebot Erhalten';
                },
                error => console.log(error)
            );
    }

    reject(): void {

        this.rejecting = true;

        this.httpConfig.postRequest(`offers/${this.offerId}/reject`)
            .subscribe(
                res => {

                    if ( res.code === 200 ) {
                        this.modalRef = this.modalService.show(this.reejctOrder);
                        this.pinned = false;
                    }

                    this.rejecting = false;
                },
                error => {
                    console.log(error);
                    this.rejecting = false;
                }
            );
    }

    accept(): void {
        this.accepting = true;

        this.httpConfig.postRequest(`offers/${this.offerId}/accept`)
            .subscribe(
                res => {

                    if ( res.code === 200 ) {
                        this.modalRef = this.modalService.show(this.acceptOrder);
                        this.pinned = false;
                    }

                    this.accepting = false;
                },
                error => {
                    console.log(error);
                    this.accepting = false;
                }
            );
    }

    hideModal() {
        this.modalRef.hide();
        this.modalRef = null;
        this.router.navigate(['/', this.path, 'contract-form', this.offerObj.id]);
    }

}
