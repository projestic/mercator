import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';

@Component({
  selector: 'mrc-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {

    public statuses: Array<string> = [];

    constructor(private router: Router,
                public http: HttpConfigService) {
        this.http.getRequest('offers/statuses')
            .subscribe(res => this.statuses = res.payload);
    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'Produktname',
            value: 'product'
        },
        {
            name: 'Lieferant',
            value: 'supplier'
        }
    ];

    public actions = {
        'click': e => {
            this.router.navigate(['offer', e.row.id]);
        }
    };

    ngOnInit() {

    }
}
