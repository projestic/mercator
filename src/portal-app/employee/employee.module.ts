import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeModuleComponent } from './employee.component';
import { routes } from './employee.routes';
import { AppCommonModule } from '../../common/app.common.module';
import { RouterModule } from '@angular/router';
import { AuthenticationModule } from '../../services/auth/auth.module';
import {
    PUBLIC_FALLBACK_PAGE_URI,
    PROTECTED_FALLBACK_PAGE_URI
} from 'ngx-auth';
import { BootstrapModule } from '../bootstrap.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { ClipboardModule } from 'ngx-clipboard';
import { DndModule } from 'ng2-dnd';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule } from '@angular/forms';

import { SearchService } from '../../services/search/search.service';

import { OffersComponent } from './offers/offers.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { OrdersComponent } from './orders/orders.component';
import { ShopsComponent } from './shops/shops.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { ContractFormComponent } from './contract-form/contract-form.component';

@NgModule({
    imports: [
        CommonModule,
        AppCommonModule.forRoot(),
        RouterModule.forChild(routes),
        AuthenticationModule,
        BootstrapModule.forRoot(),
        MaterialModule,
        NgSelectModule,
        NgxPaginationModule,
        DndModule.forRoot(),
        ClipboardModule,
        AppCommonModule,
        ColorPickerModule,
        ReactiveFormsModule,
    ],
    declarations: [
        EmployeeModuleComponent,
        OffersComponent,
        OrdersComponent,
        OfferDetailComponent,
        ShopsComponent,
        OrderDetailComponent,
        ContractsComponent,
        ContractDetailComponent,
        ContractFormComponent,
    ],
    providers: [
        SearchService,
        { provide: 'MODULE_TOKEN', useValue: 'employee_access_token' },
        { provide: PROTECTED_FALLBACK_PAGE_URI, useValue: `/${location.pathname.split('/')[1]}` },
        { provide: PUBLIC_FALLBACK_PAGE_URI, useValue: `/${location.pathname.split('/')[1]}/login` },
    ]
})
export class EmployeeModule {
    public static routes = routes;
}
