import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings/settings.service';
import { LoginService } from '../../services/login/login.service';
import { AuthenticationService } from '../../services/auth/auth.service';
import { Subscription } from 'rxjs/Rx';
import { MenuService } from '../../common/components/sidebar/menu.service';
import { TableService } from '../../common/components/table/table.service';
import { SidebarSettingsService } from '../../common/components/sidebar/sidebar-settings.service';
import { InterceptSettingsService } from '../../services/http/intercept-settings.service';
import { AuthSettingsService } from '../../services/auth/auth-settings.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyEuroPipe } from '../../common/datatable-pipes/euro-currency';
import { HttpConfigService } from '../../services/http/http.service';

@Component({
    selector: 'mrc-employee',
    templateUrl: './employee.component.html',
    styleUrls: ['./employee.component.scss']
})
export class EmployeeModuleComponent implements OnInit, OnDestroy {
    public primaryColor = '#6FC213';
    private authSubscription: Subscription;

    private mainMenu = [
        {
            name: 'Allgemein',
            links: [
                {
                    name: 'Fahrradhändler',
                    link: 'shops',
                    icon: 'store_mall_directory'
                }
            ]
        },
        {
            name: 'Meine Verträge',
            links: [
                {
                    name: 'Meine Anfragen',
                    link: 'offers',
                    icon: 'send'
                },
                {
                    name: 'Meine Bestellungen',
                    link: 'orders',
                    icon: 'assignment'
                },
                {
                    name: 'Meine Verträge',
                    link: 'contracts',
                    icon: 'description'
                }
            ]
        },
        {
            name: 'Mein Account',
            links: [
                {
                    name: 'Mein Account',
                    link: 'account',
                    icon: 'person'
                }
            ]
        }
    ];

    private tableColumns = {

        // employee orders
        orders: [
            {
                name: 'Bestell-ID',
                prop: 'number',
                flexGrow: 20,
            }, {
                name: 'Productname',
                prop: 'product_name',
                flexGrow: 25
            }, {
                name: 'Lieferant',
                prop: 'supplier_name',
                flexGrow: 25
            }, {
                name: 'Price',
                prop: 'agreed_purchase_price',
                pipe: new CurrencyEuroPipe('en-US'),
                flexGrow: 15
            }, {
                name: 'Leasingrate',
                prop: 'leasing_rate',
                pipe: new CurrencyEuroPipe('en-US'),
                flexGrow: 15
            }, {
                name: 'Status',
                prop: 'status',
                flexGrow: 20
            },
        ],

        // employee offers
        offers: [
            {
                name: 'Anfrage-ID',
                prop: 'number',
                flexGrow: 15
            }, {
                name: 'Produktname',
                prop: 'product_name',
                flexGrow: 25
            }, {
                name: 'Lieferant',
                prop: 'supplier_name',
                flexGrow: 25
            }, {
                name: 'Price',
                prop: 'discount_price',
                pipe: new CurrencyEuroPipe('en-US'),
                flexGrow: 15
            }, {
                name: 'Status',
                prop: 'status',
                flexGrow: 20
            }
        ],

        // employee contracts
        contracts: [{
            name: 'Vertrag #',
            prop: 'number',
            flexGrow: 15
        }, {
            name: 'Startdatum',
            prop: 'start_date',
            flexGrow: 15
        }, {
            name: 'Enddatum',
            prop: 'end_date',
            flexGrow: 15
        }, {
            name: 'Leaserate',
            prop: 'leasing_rate',
            flexGrow: 15
        }, {
            name: 'Produktname',
            prop: 'product_name',
            flexGrow: 30
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }],
    };

    public sidebarSettings = {
        image: 'assets/images/logos/employee_inner_logo.svg',
        link: 'account'
    };

    public httpSettings = {
        url: 'employee-api',
        token: 'employee_access_token',
        companyName: ''
    };

    constructor(private settingsService: SettingsService,
                private authService: AuthenticationService,
                private route: ActivatedRoute,
                private menuService: MenuService,
                private tableService: TableService,
                private sidebarSettingsService: SidebarSettingsService,
                private interceptService: InterceptSettingsService,
                private authSettings: AuthSettingsService,
                private router: Router,
                private http: HttpConfigService) {
        this.route.params.subscribe(value => {
            this.httpSettings = {...this.httpSettings, companyName: value.companyName};
            this.interceptService.updateSettings(this.httpSettings);
            this.authSettings.updatePath(value.companyName);
            this.authSettings.updateToken('employee_access_token');
            this.authSettings.updateCurrentModule('employee');
            this.checkSlug(value.companyName)
        });
        const settings = this.settingsService.settings;
        this.settingsService.updateSettings({...settings, color: this.primaryColor});
        this.authSubscription = this.authService.isAuthSubject.subscribe(
            res => {
                if (!res) {
                    this.settingsService.updateSettings({...settings, color: this.primaryColor});
                }
            }
        );
        this.menuService.updateMenu(this.mainMenu);
        this.tableService.updateColumns(this.tableColumns);

        this.sidebarSettingsService.updateSettings(this.sidebarSettings);
    }

    ngOnInit() {
    }

    checkSlug(slug) {
        this.http.getRequest(`companies/slug-exists?slug=${slug}`)
            .subscribe(res => {
                if (!res.payload.result) {
                    this.router.navigateByUrl(`404`);
                }
            });
    }


    ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }

}
