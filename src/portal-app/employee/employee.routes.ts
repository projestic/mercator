import { EmployeeModuleComponent } from './employee.component';
import { ProtectedGuard } from 'ngx-auth';
import { DashboardTemplateComponent } from '../../common/components/dashboard-template/dashboard-template.component';
import { OffersComponent } from './offers/offers.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { OrdersComponent } from './orders/orders.component';
import { AccountComponent } from '../../common/pages/account/account.component';
import { ShopsComponent } from './shops/shops.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { ContractFormComponent } from './contract-form/contract-form.component';
import { SearchComponent } from '../../common/pages/search/search.component';
import { Page404Component } from '../../common/pages/page-404/page-404.component';

export const routes = [
    {
        path: '',
        canActivate: [ProtectedGuard],
        component: EmployeeModuleComponent,
        children: [
            {
                path: '',
                component: DashboardTemplateComponent,
                children: [
                    {
                        path: 'shops',
                        component: ShopsComponent
                    },
                    {
                        path: 'account',
                        component: AccountComponent
                    },
                    {
                        path: 'offers',
                        component: OffersComponent
                    },
                    {
                        path: 'orders',
                        component: OrdersComponent
                    },
                    {
                        path: 'contracts',
                        component: ContractsComponent
                    },
                    {
                        path: 'offer/:id/:number',
                        component: OfferDetailComponent
                    },
                    {
                        path: 'order/:id/:number',
                        component: OrderDetailComponent
                    },
                    {
                        path: 'contract/:id/:number',
                        component: ContractDetailComponent
                    },
                    {
                        path: 'contract-form/:id',
                        component: ContractFormComponent
                    },
                    {
                        path: 'search',
                        component: SearchComponent
                    },
                    {
                        path: '',
                        redirectTo: 'shops',
                        pathMatch: 'prefix'
                    }
                ]
            },
            {
                path: '404',
                component: Page404Component
            },
        ]
    }
];
