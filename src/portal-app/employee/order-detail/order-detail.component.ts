import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { LeasingInterface } from '../../../common/models/leasing.interface';
import { Leasing } from '../../../common/models/leasing';
import { ProductInterface } from '../../../common/models/product.interface';
import { Product } from '../../../common/models/product';
import { User } from '../../../common/models/user';
import { OrderInfo } from './order-info';
import { OrderInfoInterface } from './order-info.interface';

@Component({
    selector: 'mrc-order-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
    @ViewChild('confirmForm') confirmForm: HTMLFormElement;
    public orderId: string | number;
    public orderTitle = 'Vertragsnummer';
    public orderObj: object;
    public leasing: LeasingInterface = new Leasing();
    public product: ProductInterface = new Product();
    public user: User = new User();

    public orderInfo: OrderInfoInterface = new OrderInfo({});

    constructor(private route: ActivatedRoute,
                private httpConfig: HttpConfigService) {

        this.route.params.subscribe( params => {
            this.orderId = params.id;
        });
    }

    ngOnInit() {
        if (this.orderId) this.getOrderDetailInfo();
    }

    getOrderDetailInfo() {
        this.httpConfig.getRequest(`orders/${this.orderId}`)
            .subscribe(
                res => {
                    this.orderObj = res.payload;
                    this.leasing = new Leasing(res.payload);
                    this.product = new Product(res.payload.product);
                    this.user = new User(res.payload.user);
                    this.orderInfo = new OrderInfo({code: this.user.code, email: this.user.email});
                },
                error => console.log(error)
            );
    }
}
