import { OrderInfoInterface } from './order-info.interface';
import { forEach } from 'lodash';
import { CityInterface } from '../../../common/models/city.interface';

export class OrderInfo implements OrderInfoInterface {
    public pickup_code: string | number = null;
    public code: string | number = null;
    public email: string = null;
    public notes: string | number = null;
    public contract_number: string | number = null;
    public city: CityInterface;

    constructor ({pickup_code, code, email, notes, contract_number, city}: OrderInfoInterface) {
        this.pickup_code = pickup_code || null;
        this.code = code || null;
        this.email = email || null;
        this.notes = notes || null;
        this.contract_number = contract_number || null;
        this.city = city || null;
    }
}
