import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatCheckboxChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { AuthSettingsService } from '../../../services/auth/auth-settings.service';

import { User } from '../../../common/models/user';
import { UserInterface } from '../../../common/models/user.interface';
import { Contract } from '../../../common/models/contract';
import { ContractInterface } from '../../../common/models/contract.interface';
import { ILeasingCondition } from '../../../common/models/leasing-condition.interface';
import { ProductInterface } from '../../../common/models/product.interface';
import { Offer } from '../../../common/models/offer';
import { OfferInterface } from '../../../common/models/offer.interface';
import { NgForm } from '@angular/forms';
import fileSaver from 'file-saver';
import { ModalDirective } from 'ngx-bootstrap';
import { ToasterService } from 'angular5-toaster';

@Component({
    selector: 'mrc-contract-form',
    templateUrl: './contract-form.component.html',
    styleUrls: ['./contract-form.component.scss']
})
export class ContractFormComponent implements OnInit {
    @ViewChild('pdfModal') pdfModal: ModalDirective;

    public offerID: number | string;
    public pageTitle: string;
    public submitProcess = false;
    public pdfObject: {
        body: any,
        filename: string
    };
    public step = 0;
    private path: string;
    private offerCode: number | string;
    public errorMessage = '';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private http: HttpConfigService,
        private toasterService: ToasterService,
        private authSettings: AuthSettingsService,
    ) {
        this.route.params.subscribe( params => {
            this.pageTitle = 'Überlassungsvertrag';
            this.offerID = params.id;
            this.getContractData(params.id);
            this.path = this.authSettings.path;
        });
    }

    public user: UserInterface;
    public contract: ContractInterface = new Contract();
    public leasings: Array<ILeasingCondition>;
    public product: ProductInterface;
    public offer: OfferInterface = new Offer();

    public contractFormModel = {
        conditions: {
            employer_grant: null,
            insurance_amount: null,
            insurance_type: 'Percentage',
            leasing_rate_amount: null,
            leasing_rate_type: 'Fixed',
            money_saving: null,
            service_package: {
                biv: {
                    include: false,
                    type: 'Fixed',
                    amount: null
                },
                other: {
                    include: false,
                    name: null,
                    type: 'Fixed',
                    amount: null
                },
                repair: {
                    include: false,
                    type: 'Fixed',
                    amount: null
                }
            },
            total_amount: null
        },
        offer: {
            date: null,
            product: {
                accessories: null,
                color: null,
                frame_id: null,
                model: null,
                normal_price: null,
                discount_price: null,
                period: null,
                size: null,
                supplier_name: null,
                transfer_date: null
            }
        },
        signatures: {
            company: {
                date: null,
                name: null
            },
            user: {
                date: null,
                name: null
            }
        },
        user: {
            address: null,
            city: null,
            company: {
                address: null,
                city: null,
                name: null,
                zip: null,
            },
            name: null,
            zip: null
        }
    };

    public attributes: Object = {};


    ngOnInit() {

    }

    private getContractData(id: number) {
        this.http.getRequest(`offers/${id}/contract-data`).subscribe(
            res => {
                const user = new User(res.payload.user);
                const offer = new Offer(res.payload.offer);

                this.getAttributesList(offer.product.supplier.id);

                this.offerCode = res.payload.offer.code;

                this.leasings = res.payload.user.company.leasing_settings;

                const pipe = new DatePipe('en-US');
                const currentDate = pipe.transform(new Date(), 'dd/MM/yyyy');

                // Set model data

                // company
                this.contractFormModel.user.company.address = user.company.address;
                this.contractFormModel.user.company.city = user.company.city.name;
                this.contractFormModel.user.company.name = user.company.name;
                this.contractFormModel.user.company.zip = user.company.zip;

                // offer
                this.contractFormModel.offer.date = currentDate;

                // product
                this.contractFormModel.offer.product.accessories = offer.notes;
                this.contractFormModel.offer.product.color = this.searchArrayObj('slug', 'color', offer.product.attributes, 'value');
                this.contractFormModel.offer.product.frame_id = 'ID 212313KE23'; // TODO: sample data, will be improved in v2
                this.contractFormModel.offer.product.model = offer.product.model.name;
                this.contractFormModel.offer.product.normal_price = offer.normal_price;
                this.contractFormModel.offer.product.discount_price = offer.discount_price;
                this.contractFormModel.offer.product.transfer_date = currentDate;

                if (this.leasings[0]) {
                    this.contractFormModel.offer.product.period = this.leasings[0].period;
                }

                this.contractFormModel.offer.product.size = this.searchArrayObj('slug', 'size', offer.product.attributes, 'value');
                this.contractFormModel.offer.product.supplier_name = offer.product.supplier.name;

                // user
                this.contractFormModel.user.name = user.fullname;

                // signatures
                this.contractFormModel.signatures.company.name = res.payload.signatures.company_admin;
                this.contractFormModel.signatures.company.date = res.payload.signatures.date;
                this.contractFormModel.signatures.user.date = currentDate;

                this.setPeriod();
            },
            error => {
                console.log(error);
            }
        );
    }

    public createContract(form: NgForm) {

        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
        });

        if (form.invalid) {
            document.querySelector('input.ng-invalid').scrollIntoView();
            return;
        }

        this.submitProcess = true;

        const type = 'application/pdf';
        const options = {
            responseType: 'arraybuffer',
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8',
            },
            observe: 'response'
        };

        this.http.postRequest(`offers/${this.offerID}/generate-contract-pdf`, this.contractFormModel, options)
            .subscribe((res: Response) => {
                const filename = res.headers.get('content-disposition').match(/filename=(.+)/)[1].slice(1, -1);
                this.pdfObject = {
                    body: new Blob([res.body], {type}),
                    filename
                };
                this.pdfModal.show();

                this.submitProcess = false;
            },
            error => {
                console.log(error);
                this.submitProcess = false;
            });
    }

    public downloadPdf() {
        fileSaver.saveAs(this.pdfObject.body, this.pdfObject.filename);
        this.step = 1;
    }

    public sendPdf(file): void {
        if (file.item(0).type.indexOf('pdf') === -1) {
            this.errorMessage = 'Please select PDF format file';
            return;
        }

        this.errorMessage = '';

        const formData = new FormData();

        formData.append('file', file.item(0));

        this.http.postRequest(`offers/${this.offerID}/generate-contract`, formData)
            .subscribe(
                (res: Response) => {
                    this.step = 0;
                    this.pdfModal.hide();
                    this.router.navigate([]);
                    this.router.navigate(['/', this.path, 'offer', this.offerID, this.offerCode]);
                },
                error => {
                    this.step = 0;
                    this.pdfModal.hide();
                }
            );
    }

    public changeLeasingRateType(event: MatCheckboxChange, value: string): void {
        if (event.checked) {
            this.contractFormModel.conditions.leasing_rate_type = value;

            this.setLeasingRate();
        } else {
            event.source.checked = true;
        }
    }

    public changeInsuranceType(event: MatCheckboxChange, value: string): void {
        if (event.checked) {
            this.contractFormModel.conditions.insurance_type = value;

            this.setInsurance();
        } else {
            event.source.checked = true;
        }
    }

    public changeRepairType(event: MatCheckboxChange, value: string): void {
        if (event.checked) {
            this.contractFormModel.conditions.service_package.repair.type = value;
        } else {
            event.source.checked = true;
        }
    }

    public changeBivType(event: MatCheckboxChange, value: string): void {
        if (event.checked) {
            this.contractFormModel.conditions.service_package.biv.type = value;
        } else {
            event.source.checked = true;
        }
    }

    public changeOtherType(event: MatCheckboxChange, value: string): void {
        if (event.checked) {
            this.contractFormModel.conditions.service_package.other.type = value;
        } else {
            event.source.checked = true;
        }
    }

    public setLeasingRate(): void {
        const leasing = this.searchArrayObj('period', this.contractFormModel.offer.product.period, this.leasings);
        const factor = leasing.factor;
        let leasing_rate_amount;

        if (this.contractFormModel.conditions.leasing_rate_type === 'Fixed') {
            leasing_rate_amount = Math.round(this.contractFormModel.offer.product.discount_price * factor) / 100;
        } else {
            leasing_rate_amount = factor;
        }

        this.contractFormModel.conditions.leasing_rate_amount =  leasing_rate_amount;
    }

    public setInsurance(): void {
        const leasing = this.searchArrayObj('period', this.contractFormModel.offer.product.period, this.leasings);
        const insurance = leasing.insurance;
        let insurance_amount;

        if (this.contractFormModel.conditions.insurance_type === 'Fixed') {
            insurance_amount = Math.round(this.contractFormModel.offer.product.discount_price * 0.35) / 100;
        } else {
            insurance_amount = Math.round(this.contractFormModel.offer.product.discount_price * insurance) / 100;
        }

        this.contractFormModel.conditions.insurance_amount = insurance_amount;
    }

    public setPeriod(): void {
        this.setLeasingRate();
        this.setInsurance();
    }

    private getAttributesList(id) {
        this.http.getRequest(`product-attributes/all?supplier_id=${id}`)
            .subscribe(res => {
                res.payload.forEach(attr => {
                    this.attributes[attr.slug] = attr.default_values;
                });
            });
    }

    private addTag(name) {
        return { name: name, tag: true, id: name };
    }

    private addTagAttribute(name) {
        return { value: name, tag: true, label: name };
    }

    private searchArrayObj(slug: string, value: any, arr: Array<any>, returnSlug?: string): any {
        const res = arr.filter(item => item[slug] === value);

        if (returnSlug) {
            return res[0] ? res[0][returnSlug] : undefined;
        }

        return res[0];
    }
}

