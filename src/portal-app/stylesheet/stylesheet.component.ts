import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-stylesheet',
    templateUrl: './stylesheet.component.html',
    styleUrls: ['./stylesheet.component.scss']
})

export class StylesheetComponent implements OnInit {

    public demoSelectOptions: Array<string> = [
        'Dropdown menu item - 1',
        'Dropdown menu item - 2',
        'Dropdown menu item - 3',
        'Dropdown menu item - 4',
        'Dropdown menu item - 5',
        'Dropdown menu item - 6',
        'Dropdown menu item - 7',
        'Dropdown menu item - 8',
    ];

    public demoSelectPager: Array<string | number> = [
        10,
        20,
        50,
        'All'
    ];

    public demoTablePerPage = this.demoSelectPager[0];

    public demoTable: Array<any> = [];

    public title = 'Stylesheet';

    constructor() {


        //TODO: Generate demo data
        let names: Array<string> = [
            'Acme Corporation',
            'Globex Corporation',
            'Initech'
        ];

        let cities: Array<string> = [
            'Munich',
            'Berlin',
            'Frankfurt'
        ];

        let emails: Array<string> = [
            'info@acmecorp.com',
            'info@globex.com',
            'contact@initech-de.com'
        ];

        for (let i = 0; i < 100; i++ ) {
            let random: number = this.getRandomInt(1, 100);

            this.demoTable.push({
                id: i,
                name: names[random%names.length],
                city: cities[random%cities.length],
                email: names[random%emails.length],
                count: random,
                isActive: random%2
            });
        }
    }

    ngOnInit() {
    }

    private getRandomInt(min = 1, max = 100) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
