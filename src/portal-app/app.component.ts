import { Component, AfterViewInit, OnInit } from '@angular/core';
import { ToasterService, Toast } from 'angular5-toaster';
import { SettingsService } from '../services/settings/settings.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterViewInit {

    constructor(public toasterService: ToasterService,
                public settingsService: SettingsService,
                private router: Router) {
        this.router.events.subscribe(val => {
            if ( val instanceof NavigationEnd) {
                if (val.urlAfterRedirects.endsWith('admin')) this.router.navigateByUrl(`${val.urlAfterRedirects}/dashboard`);
            }
        });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
    }
}

