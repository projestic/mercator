import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { CompanyModule } from './company/company.module';
import { SupplierModule } from './supplier/supplier.module';
import { PortalModule } from './portal/portal.module';
import { EmployeeModule } from './employee/employee.module';
import { Page404Component } from '../common/pages/page-404/page-404.component';

const routes: Routes = [
    {
        path: '404',
        component: Page404Component
    },
    {
        path: '',
        loadChildren: () => PortalModule,
        data: {
            preload: true
        }
    },
    {
        path: 'lieferanten',
        loadChildren: () => SupplierModule,
        data: {
            preload: true
        }
    },
    {
        path: ':companyName',
        children: [
            {
                path: '',
                loadChildren: () => EmployeeModule,
                data: {
                    preload: true
                }
            },
            {
                path: '',
                loadChildren: () => CompanyModule,
                data: {
                    preload: true
                }
            }
        ]
    },
    {
        path: '**',
        component: Page404Component
    }

];

export const ROUTES: ModuleWithProviders = RouterModule.forRoot(routes,
    {
        preloadingStrategy: PreloadAllModules,
        onSameUrlNavigation: 'reload'
    });

