import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ROUTES } from './app.routes';
import { DndModule } from 'ng2-dnd';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { ToasterModule, ToasterService } from 'angular5-toaster';
import { MediaMatcher } from '@angular/cdk/layout';
import { ChartsModule } from 'ng2-charts';
import { AppComponent } from './app.component';
import { PipesModule } from '../common/pipes/pipes.module';
import { AppCommonModule } from '../common/app.common.module';
import { BootstrapModule } from './bootstrap.module';
import { NgxPermissionsModule } from 'ngx-permissions';

//Services
import { HttpConfigModule } from '../services/http/http.module';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../services/user/user.service';
import { ImageHttpService } from '../services/image/image.service';
import { ConstantsService } from '../services/constants/constants.service';
import { CurrencyPipe } from '@angular/common';
import { LoginService } from '../services/login/login.service';
import { ProfileService } from '../services/profile/profile.service';
import { SettingsService } from '../services/settings/settings.service';
import { AUTH_SERVICE, AuthModule } from 'ngx-auth';
import { AuthenticationService } from '../services/auth/auth.service';
import { TokenStorageService } from '../services/auth/token-storage.service';
import { AuthSettingsService } from '../services/auth/auth-settings.service';



@NgModule({
    declarations: [
        //Componenets
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        NgSelectModule,
        FormsModule,
        ROUTES,
        NgxPermissionsModule.forRoot(),
        DndModule.forRoot(),
        NgxPaginationModule,
        HttpClientModule,
        PipesModule,
        ToasterModule,
        AppCommonModule.forRoot(),
        HttpConfigModule.forRoot(),
        BootstrapModule.forRoot(),
        ChartsModule,
        AuthModule,
    ],
    providers: [
        ConstantsService,
        CookieService,
        ImageHttpService,
        MediaMatcher,
        ToasterService,
        UserService,
        LoginService,
        CurrencyPipe,
        ProfileService,
        SettingsService,
        {
            provide: AUTH_SERVICE,
            useClass: AuthenticationService
        },
        TokenStorageService,
        AuthSettingsService
    ],
    bootstrap: [AppComponent]
})
export class PortalAppModule {}
