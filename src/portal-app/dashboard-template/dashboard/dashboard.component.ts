import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpConfigService } from '../../../services/http/http.service';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular5-toaster';
import { AddWidgetComponent } from '../../../common/components/add-widget/add-widget.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

const compareNumeric = (a, b) => {
    if (a.position > b.position) return 1;
    if (a.position < b.position) return -1;
}

@Component({
    selector: 'mrc-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [DatePipe]
})

export class DashboardComponent implements OnInit {
    title = 'Dashboard';
    public companies: Array<{id: string | number; name: string}> = [];
    public widgets: Array<any> = [];
    public currentDashboard: CurrentDashboardInterface = new CurrentDashboard();
    public sourceArr: Array<any>;
    public widgetTitles: object;
    public preloader: Array<boolean>;
    @ViewChild('addWidget') addWidget: AddWidgetComponent;
    @ViewChild('hideNoOrderWidgetWarning') hideNoOrderWidgetWarning: TemplateRef<any>;

    constructor(private datePipe: DatePipe,
                private toasterService: ToasterService,
                private httpService: HttpConfigService,
                private modalService: BsModalService
    ) {
        this.getSourcesList();
    }

    public ngOnInit() {
        this.getCompaniesList();
    }

    public getDates(data) {
        this.currentDashboard.dateFrom = (data[0].getTime() / 1000);
        this.currentDashboard.dateTo = (data[1].getTime() / 1000);
        this.getWidgets(this.currentDashboard);
    }

    public companyChange(value) {
        this.getSourcesList();
    }

    public getCompaniesList() {
        this.httpService.getRequest('companies/all')
            .subscribe(
                res => {
                    this.companies[0] = {
                        id: 0,
                        name: 'All Companies'
                    }
                    this.companies = this.companies.concat(res.payload);
                },
                error => console.log(error)
            );
    }

    public modalRef: BsModalRef;

    public getSourcesList() {
        this.httpService.getRequest('widgets/sources')
            .subscribe(
                res => {
                    this.widgetTitles = res.payload;
                    const widgetTitlesProp = Object.keys(this.widgetTitles);
                    this.sourceArr = widgetTitlesProp.map(item => {
                        return {
                            value: item,
                            label: this.widgetTitles[item]
                        };
                    });
                    if (this.currentDashboard.companyId !== 0) {
                        this.sourceArr = this.sourceArr.filter(item => item.value !== 'orders_per_company');
                        delete this.widgetTitles['orders_per_company'];
                    }
                    this.getWidgets(this.currentDashboard);
                },
                error => console.log(error)
            );
    }

    public getCompanyName() {
        let companyName;
        this.companies.forEach(item => {
            if (item.id === this.currentDashboard.companyId) {
                companyName = item.name;
            }
        });
        return companyName;
    }

    public getWidgets({companyId, dateFrom, dateTo}: CurrentDashboardInterface) {
        let url = `widgets?date_from=${dateFrom}&date_to=${dateTo}`;
        if (companyId !== 0) url += `&company_id=${companyId}`;
        this.httpService.getRequest(url)
            .subscribe(
                res => {
                    this.preloader = new Array(res.payload.length);
                    this.widgets = [...res.payload.sort(compareNumeric)];
                    if (this.widgets.length > 0) this.changeSort();
                },
                error => console.log(error)
            );
    }

    public deleteWidget(value) {
        this.httpService.deleteRequest(`widgets/${value}`)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Widget was removed successfully');
                    this.getWidgets(this.currentDashboard);
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

    public updateWidget(value) {
        const data = {...value};
        delete data.id;
        this.preloader[(value.position - 1)] = true;
        this.httpService.postRequest(`widgets/${value.id}`, data)
            .subscribe(
                res => this.getWidgetById(value.id, this.currentDashboard),
                error => {
                    this.preloader[(value.position - 1)] = false;
                    this.addWidget.openErrorModal(error.source);
                }
            );
    }

    public getWidgetById(id, {companyId, dateFrom, dateTo}: CurrentDashboardInterface) {
        let url = `widgets/${id}?date_from=${dateFrom}&date_to=${dateTo}`;
        if (companyId !== 0) url += `&company_id=${companyId}`;
        this.httpService.getRequest(url)
            .subscribe(
                res => {
                    this.widgets = this.widgets.map(item => {
                        if (item.position === res.payload.position) {
                            item = {...res.payload};
                            this.preloader[(res.payload.position - 1)] = false;
                        }
                        return item;
                    });
                },
                error => console.log(error)
            );
    }

    public changeSort() {
        const data = {};
        for ( let i = 0; i < this.widgets.length; i++) {
            data[this.widgets[i].id] = i + 1;
        }
        this.httpService.postRequest('widgets/positions', {positions: data})
            .subscribe(
                res => res,
                error => console.log(error)
            );
    }
}

interface CurrentDashboardInterface {
    companyId?: string | number;
    dateFrom?: string | number;
    dateTo?: string | number;
}

class CurrentDashboard {
    public companyId: string | number = 0;
    public dateFrom: string | number =  Math.floor(new Date(new Date().getFullYear(),
        new Date().getMonth(), 1).getTime() / 1000) - new Date().getTimezoneOffset() * 60;
    public dateTo: string | number = Math.floor((new Date().getTime() / 1000)) - new Date().getTimezoneOffset() * 60;

    constructor(data?: CurrentDashboardInterface) {
        if (data) {
            this.companyId = data.companyId;
            this.dateFrom = data.dateFrom;
            this.dateTo = data.dateTo;
        }
    }
}

