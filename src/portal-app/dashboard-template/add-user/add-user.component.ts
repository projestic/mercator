import { Component, OnChanges, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../services/http/http.service';
import { UserInfo } from './user-info';
import { ToasterService } from 'angular5-toaster';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'mrc-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit, OnChanges {
    @ViewChild('addUser') addUser: HTMLFormElement;

    public userPath: string;
    public userId: string | number;
    public submitProcess = false;
    public message = 'When the user is created he will receive an email...';

    public roles: Array<string> = [
        'Company Admin',
        'Portal Admin'
    ];

    public companies: Array<object>;

    public userInfo = new UserInfo();

    constructor(private httpService: HttpConfigService,
                private toasterService: ToasterService,
                private route: ActivatedRoute) {
        this.route.url.subscribe(params => {
            if (params.length > 1) {
                this.userPath = params[1].path;
            }
        });
        this.route.params.subscribe(params => {
            this.userId = params.id;

            this.getPortalsList();

            if (this.userPath) {
                this.getUserInfo();
            }
        });
    }

    ngOnInit() {
    }

    ngOnChanges() {
    }

    getUserInfo() {
        let url = `users/${this.userId}`;
        if (this.userPath === 'company') url = `company-users/${this.userId}`;

        this.httpService.getRequest(url)
            .subscribe(
                res => {
                    this.userInfo = new UserInfo(res.payload);
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

    getPortalsList() {
        this.httpService.getRequest('companies/all')
            .subscribe(
                res => this.companies = res.payload
            );
    }

    public aktiveToggle() {
        if (this.userInfo.statusBool) {
            this.userInfo.status = 'Aktiv';
        } else {
            this.userInfo.status = 'Inaktiv';
        }
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;
        this.aktiveToggle();
        let url = 'users';
        let message = 'added';
        if (this.userId) {
            url += `/${this.userId}`;
            message = 'update';
        }
        this.httpService.postRequest(url, this.userInfo)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `User was ${message} successfully`);
                    if (this.userId) {
                        this.getUserInfo();
                    } else {
                        form.resetForm();
                        this.userInfo = new UserInfo();
                    };
                    this.submitProcess = false;
                },
                res => {

                    if (res.status === 422) {
                        this.httpService.setFormErrors(this.addUser, res.error.payload);
                    }

                    this.submitProcess = false;
                });
    }
}
