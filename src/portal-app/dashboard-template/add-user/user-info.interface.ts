export interface UserInfoInterface {
    role: string;
    company_id?: string | number;
    first_name: string;
    last_name: string;
    email: string;
    status: string;
    password?: string;
}
