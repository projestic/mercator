import { UserInfoInterface } from './user-info.interface';

export class UserInfo implements UserInfoInterface {
    role: string = 'Company Admin';
    company_id?: string | number = 1;
    first_name: string;
    last_name: string;
    email: string;
    statusBool: boolean;
    status: string = 'Aktive';
    password?: string;

    constructor(user?: UserInfoInterface) {
        if (user) {
            this.first_name = user.first_name;
            this.last_name = user.last_name;
            this.status = user.status;
            this.company_id = user.company_id;
            this.email = user.email;
            this.role = user.role;
        } else {
            this.password = this.randomPassword(Math.floor(Math.random() * 7) + 8);
        }
        this.statusBool = this.statusCheck();
    }

    statusCheck() {
        if (this.status === 'Inaktiv') return false;
        return true;
    }

    randomPassword(length) {
        const chars = 'abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890';
        let pass = '';
        for (let x = 0; x < length; x++) {
            let i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }

        if (pass.match(/^(?=.*\d)(?=.*[^a-zA-Z\d\s:])(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,15}$/g) === null) {
            return this.randomPassword(length);
        }

        return pass;
    }
}
