import { forEach } from 'lodash';
import { LeasingCondition } from '../../../common/models/leasing-condition';

export class CompanyInfo {
    public name: string;
    public vat: string;
    public invoice_type: string = 'Net';
    public admin_first_name: string;
    public admin_last_name: string;
    public admin_email: string;
    public supplier_ids: Array<any> = [];
    public zip: string;
    public city_id: string;
    public address: string;
    public phone: string;
    public max_user_contracts: string;
    public max_user_amount: string;
    public override_insurance_amount: boolean = false;
    public insurance_monthly_amount: string;
    public override_maintenance_amount: boolean = false;
    public insurance_covered: boolean = false;
    public insurance_covered_type: string = 'Fixed';
    public insurance_covered_amount: string;
    public maintenance_covered: boolean = false;
    public leasing_rate: boolean = false;
    public leasing_rate_type: string = 'Fixed';
    public leasing_rate_amount: string;
    public leasing_settings: Array<any> = [];
    public slug: string;
    public status: string;
    public statusBool: boolean = true;

    constructor (company?) {

        if (company) {
            forEach(company, (val, key) => {
                if (key === 'leasing_settings') {
                    val.forEach(item => {
                        this.leasing_settings.push(new LeasingCondition(item));
                    });
                } else {
                    this[key] = val;
                }
            });
        }

        this.statusBool = this.statusCheck();
    }

    statusCheck() {
        if (this.status === 'Inaktiv') return false;
        return true;
    }
}
