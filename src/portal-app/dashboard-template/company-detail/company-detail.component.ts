import { Component, OnInit, ViewChild, DoCheck } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CompanyInfo } from './company-info';
import { CompanyInfoInterface } from './company-info.interface';

@Component({
    selector: 'mrc-company-detail',
    templateUrl: './company-detail.component.html',
    styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent implements OnInit, DoCheck {

    @ViewChild('companyForm') companyForm: HTMLFormElement;

    public companyId: string | number;
    public companyTitle: string | number;
    public cityList: Array<object>;
    public submitProcess = false;
    public companyInfo: CompanyInfoInterface = new CompanyInfo();
    public companyInfoOld: CompanyInfoInterface = new CompanyInfo();
    public invoiceType = ['Net', 'Gross'];
    public insuranceType = ['Fixed', 'Percentage'];
    public rateType = ['Fixed', 'Percentage'];
    public fixed = '<i class="material-icons">euro_symbol</i>';
    public percentage = '<i class="percent-icon">%</i>';
    public showSubmit: boolean;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private httpService: HttpConfigService,
                private toasterService: ToasterService) {
        this.route.params.subscribe(params => {
            this.companyId = params.id;
        });
    }

    ngOnInit() {
        this.getCities();
        if (this.companyId) this.getCompanyInfo();
    }

    ngDoCheck() {
        if (JSON.stringify(this.companyInfo) !== JSON.stringify(this.companyInfoOld)) {
            this.showSubmit = true;
        } else {
            this.showSubmit = false;
        }
    }

    getCities() {
        const url = 'geo/cities';
        this.httpService.getRequest(url).subscribe(res => {
            this.cityList = res.payload;
        });
    }

    getCompanyInfo() {
        this.httpService.getRequest(`companies/${this.companyId}`)
            .subscribe(
                res => {
                    this.companyTitle = res.payload.name;
                    this.companyInfo = new CompanyInfo(res.payload);
                    this.companyInfoOld = {...this.companyInfo};
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }

    public aktiveToggle() {
        if (this.companyInfo.statusBool) {
            this.companyInfo.status = 'Aktiv';
        } else {
            this.companyInfo.status = 'Inaktiv';
        }
    }

    public cancelForm(event, form: NgForm) {
        event.preventDefault();

        if (this.companyId) {
            this.getCompanyInfo();
        } else {
            form.resetForm();
            this.companyInfo = new CompanyInfo();
        }
    }

    public update(form: NgForm): void {

        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        this.aktiveToggle();

        if (form.invalid ) return;

        this.submitProcess = true;

        let url;

        if (this.companyId) {
            url = `companies/${this.companyId}`;
            this.httpService.postRequest(url, this.companyInfo)
                .subscribe(
                    res => {
                        this.toasterService.pop('success', '', 'Company was updated successfully');
                        this.submitProcess = false;
                        form.resetForm();
                        setTimeout(() => {
                            this.companyInfo = new CompanyInfo(res.payload);
                            this.companyInfoOld = new CompanyInfo(res.payload);
                        }, 0);
                    },
                    error => {
                        if (error.status === 422) {
                            this.httpService.setFormErrors(this.companyForm, error.error.payload);
                        }

                        this.submitProcess = false;
                    });
        } else {
            url = `companies`;
            this.httpService.postRequest(url, this.companyInfo)
                .subscribe(
                    res => {
                        this.toasterService.pop('success', '', 'Company was added successfully');
                        this.submitProcess = false;
                        form.resetForm();
                        setTimeout(() => {
                            this.companyInfo = new CompanyInfo();
                        }, 0);
                    },
                    error => {

                        if (error.status === 422) {
                            this.httpService.setFormErrors(this.companyForm, error.error.payload);
                        }

                        this.submitProcess = false;
                    });
        }
    }
}
