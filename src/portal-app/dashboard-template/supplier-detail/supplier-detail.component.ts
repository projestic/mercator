import { Component, OnInit, TemplateRef  } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { SupplierPortalInfo } from '../../../common/models/supplier-portal';
import { SupplierPortalInfoInterface } from '../../../common/models/supplier-portal.interface';

@Component({
    selector: 'mrc-supplier-detail',
    templateUrl: './supplier-detail.component.html',
    styleUrls: ['./supplier-detail.component.scss']
})
export class SupplierDetailComponent implements OnInit {
    modalRef: BsModalRef;

    public supplierId: string | number;
    public supplierInfo: SupplierPortalInfoInterface = new SupplierPortalInfo();
    public viewCompanies = false;
    public orderStatuses = [];
    public offerStatuses = [];

    public orders: Array<any> = [];
    public offers: Array<any> = [];
    public ordersPreload = false;
    public offersPreload = false;

    constructor(private route: ActivatedRoute,
                private modalService: BsModalService,
                private httpService: HttpConfigService,
                private toasterService: ToasterService) {
        this.getOrderStatuses();
        this.getOfferStatuses();
        this.route.params.subscribe(params => {
            this.supplierId = params.id;

            this.getSupplierInfo();
            this.updateOrders();
            this.updateOffers();
        });
    }

    ngOnInit() {

    }

    getOrderStatuses() {
        this.httpService.getRequest(`orders/statuses`)
            .subscribe(
                res => this.orderStatuses = res.payload
            );
    }

    getOfferStatuses() {
        this.httpService.getRequest(`offers/statuses`)
            .subscribe(
                res => this.offerStatuses = res.payload
            );
    }

    getSupplierInfo() {
        this.httpService.getRequest(`suppliers/${this.supplierId}`)
            .subscribe(
                res => {
                    this.supplierInfo = new SupplierPortalInfo(res.payload);
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }

    toggleCompanies(event, template: TemplateRef<any>) {
        event.preventDefault();
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-sm' }));
    }

    updateOrders(item = '') {
        this.ordersPreload = true;
        let query;
        if (item) {
            query = `?status=${item}`;
        } else {
            query = '';
        }
        this.httpService.getRequest(`suppliers/${this.supplierId}/orders${query}`)
            .subscribe(
                res => {
                    this.ordersPreload = false;
                    this.orders = res.payload.map(item => {
                        delete item.id;
                        return item;
                    });
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }

    updateOffers(item = '') {
        this.offersPreload = true;
        let query;
        if (item) {
            query = `?status=${item}`;
        } else {
            query = '';
        }
        this.httpService.getRequest(`suppliers/${this.supplierId}/offers${query}`)
            .subscribe(
                res => {
                    this.offersPreload = false;
                    this.offers = res.payload.map(item => {
                        delete item.id;
                        return item;
                    });
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }
}
