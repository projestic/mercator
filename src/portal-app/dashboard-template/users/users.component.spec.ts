import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponentSupplier } from './users.component';

describe('UsersComponent', () => {
  let component: UsersComponentSupplier;
  let fixture: ComponentFixture<UsersComponentSupplier>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersComponentSupplier ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponentSupplier);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
