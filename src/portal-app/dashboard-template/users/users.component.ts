import { Component, OnInit } from '@angular/core';
import { TableService } from '../../../common/components/table/table.service';

@Component({
    selector: 'mrc-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    public statuses: Array<string> = [
        'Aktiv',
        'Inaktiv'
    ];

    public filter: Array<any> = [
        {
            name: 'Company',
            value: 'company'
        },
        {
            name: 'Role',
            value: 'role',
            type: 'role',
            options: [
                'Company Admin',
                'Portal Admin'
            ]
        }
    ];

    constructor(private tableService: TableService) {}

    public columns = [];

    ngOnInit() {

    }

    public print(): void {
        window.print();
    }

    public export(): void {
        this.tableService.export('users', 'Benutzer');
    }
}
