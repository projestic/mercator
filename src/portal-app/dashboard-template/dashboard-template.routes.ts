import { AccountComponent } from '../../common/pages/account/account.component';
import { AddUserComponent } from './add-user/add-user.component';
import { CompaniesComponent } from './companies/companies.component';
import { CompanyDetailComponent } from './company-detail/company-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardTemplateComponent } from '../../common/components/dashboard-template/dashboard-template.component';
import { SearchComponent } from '../../common/pages/search/search.component';
import { SettingsComponent } from '../../common/pages/settings/settings.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { SupplierDetailComponent } from './supplier-detail/supplier-detail.component';
import { SupplierDetailEditComponent } from './supplier-detail-edit/supplier-detail-edit.component';
import { UsersComponent } from './users/users.component';

export const routes = [
    {
        path: '',
        component: DashboardTemplateComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'companies',
                component: CompaniesComponent
            },
            {
                path: 'create-company',
                component: CompanyDetailComponent
            },
            {
                path: 'company',
                children: [
                    {
                        path: ':id/edit',
                        component: CompanyDetailComponent
                    }
                ]
            },
            {
                path: 'suppliers',
                component: SuppliersComponent
            },
            {
                path: 'add-supplier',
                component: SupplierDetailEditComponent
            },
            {
                path: 'user',
                children: [
                    {
                        path: ':id/edit',
                        component: AddUserComponent
                    }
                ]
            },
            {
                path: 'add-user',
                component: AddUserComponent
            },
            {
                path: 'supplier',
                children: [
                    {
                        path: ':id',
                        component: SupplierDetailComponent
                    },
                    {
                        path: ':id/edit',
                        component: SupplierDetailEditComponent
                    }
                ]
            },
            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'account',
                component: AccountComponent
            },
            {
                path: 'settings',
                component: SettingsComponent
            },
            {
                path: 'search',
                component: SearchComponent
            }
        ]
    },
];
