import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { MaterialModule } from '../material.module';
import { DndModule } from 'ng2-dnd';
import { ColorPickerModule } from 'ngx-color-picker';
import { ClipboardModule } from 'ngx-clipboard';
import { routes } from './dashboard-template.routes';
import { RouterModule } from '@angular/router';

// Pages
import { AppCommonModule } from '../../common/app.common.module';
import { AddUserComponent } from './add-user/add-user.component';
import { CompaniesComponent } from './companies/companies.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { UsersComponent } from './users/users.component';
import { SupplierDetailComponent } from './supplier-detail/supplier-detail.component';
import { SupplierDetailEditComponent } from './supplier-detail-edit/supplier-detail-edit.component';
import { StylesheetComponent } from '../stylesheet/stylesheet.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompanyDetailComponent } from './company-detail/company-detail.component';
import { CclipboardComponent } from '../../components/cclipboard/cclipboard.component';

//ngx-bootstrap
import { BootstrapModule } from '../bootstrap.module';

// Services
import { SearchService } from '../../services/search/search.service';


@NgModule({
    declarations: [
        //Components
        StylesheetComponent,
        DashboardComponent,
        CclipboardComponent,

        //Pages
        CompaniesComponent,
        SuppliersComponent,
        CompanyDetailComponent,
        UsersComponent,
        SupplierDetailComponent,
        SupplierDetailEditComponent,
        AddUserComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        NgSelectModule,
        NgxPaginationModule,
        DndModule.forRoot(),
        ClipboardModule,
        AppCommonModule,
        ColorPickerModule,

        //ngx-bootstrap
        BootstrapModule.forRoot(),
    ],
    providers: [
        SearchService,
    ]
})
export class DashboardTemplateModule {
    public static routes = routes;
}

