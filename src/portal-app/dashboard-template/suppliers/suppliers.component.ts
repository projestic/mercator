import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'mrc-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {

    constructor(private router: Router) {

    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'City',
            value: 'city'
        },
        {
            name: 'Number of Products',
            value: 'number_of_products'
        },
        {
            name: 'Status',
            value: 'status',
            type: 'status'
        }
    ];

    public statuses: Array<string> = [];

    ngOnInit() {

    }
}
