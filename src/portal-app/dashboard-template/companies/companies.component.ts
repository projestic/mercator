import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'mrc-firms',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

    public statuses: Array<string> = [
        'Aktiv',
        'Inaktiv'
    ];

    constructor(private router: Router) {

    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'City',
            value: 'city'
        },
        {
            name: 'Status',
            value: 'status',
            type: 'status'
        }
    ];

    ngOnInit() {

    }

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/portal', e.row.id, 'edit']);
        }
    }

}
