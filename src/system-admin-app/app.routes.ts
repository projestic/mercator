import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from '../common/pages/login/login.component';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';
import { ForgotPasswordComponent } from '../common/pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../common/pages/reset-password/reset-password.component';
import { DashboardTemplateModule } from './dashboard-template/dashboard-template.module';
import { Page404Component } from '../common/pages/page-404/page-404.component';

const routes: Routes = [
    {
        path: 'admin',
        canActivate: [ProtectedGuard],
        loadChildren: () => DashboardTemplateModule,
        data: {preload: true}
    },
    {
        path: 'login',
        canActivate: [PublicGuard],
        component: LoginComponent
    },
    {
        path: 'password',
        canActivate: [PublicGuard],
        children: [
            {
                path: 'forgot-password',
                canActivate: [PublicGuard],
                component: ForgotPasswordComponent,
            },
            {
                path: 'reset',
                canActivate: [PublicGuard],
                component: ResetPasswordComponent
            },
        ]
    },
    {
        path: '',
        redirectTo: '/admin/dashboard',
        pathMatch: 'prefix'
    },
    {
        path: '**',
        component: Page404Component
    }
];

export const ROUTES: ModuleWithProviders = RouterModule.forRoot(routes,
    {
        preloadingStrategy: PreloadAllModules
    });
