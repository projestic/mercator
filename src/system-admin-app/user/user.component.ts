import { Component, OnInit, Input } from '@angular/core';
import { UserInterface } from '../../common/models/user.interface';

@Component({
    selector: 'mrc-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    @Input() user: UserInterface;

    constructor() {
    }

    ngOnInit() {
    }

}
