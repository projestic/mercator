import { Component, AfterViewInit } from '@angular/core';
import { ToasterService, Toast } from 'angular5-toaster';
import { Subscription } from 'rxjs/Rx';
import { MenuService } from '../common/components/sidebar/menu.service';
import { LoginService } from '../services/login/login.service';
import { TableService } from '../common/components/table/table.service';
import { AuthenticationService } from '../services/auth/auth.service';
import { SettingsService } from '../services/settings/settings.service';
import { SidebarSettingsService } from '../common/components/sidebar/sidebar-settings.service';
import { InterceptSettingsService } from '../services/http/intercept-settings.service';
import { AuthSettingsService } from '../services/auth/auth-settings.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements AfterViewInit {
    public loginLogo = 'assets/images/logos/systemadministrator_logo.svg';
    public primaryColor = '#ec4640';
    private authSubscription: Subscription;

    public sidebarSettings = {
        image: 'assets/images/logos/systemadministrator_inner_logo.svg',
        link: ''
    };

    private mainMenu = [
        {
            name: 'Allgemein',
            links: [
                {
                    name: 'Dashboard',
                    link: 'dashboard',
                    icon: 'assessment'
                }
            ]
        },
        {
            name: 'Bestellungen/Verträge',
            links: [
                {
                    name: 'Bestellungen',
                    link: 'orders',
                    icon: 'library_books'
                },
                {
                    name: 'Verträge',
                    link: 'contracts',
                    icon: 'description'
                }
            ]
        },
        {
            name: 'Portale',
            links: [
                {
                    name: 'Übersicht',
                    link: 'portals',
                    icon: 'dvr'
                },
                {
                    name: 'Portal hinzufügen',
                    link: 'create-portal',
                    icon: 'add_to_queue'
                }
            ]
        },
        {
            name: 'Benutzer',
            links: [
                {
                    name: 'Übersicht',
                    link: 'users',
                    icon: 'person'
                }
            ]
        },
        {
            name: 'Lieferanten',
            links: [
                {
                    name: 'Übersicht',
                    link: 'suppliers',
                    icon: 'local_shipping'
                }
            ]
        },
        {
            name: 'Verwalten',
            links: [
                {
                    name: 'Einstellungen',
                    link: 'settings',
                    icon: 'settings'
                },
                {
                    name: 'Feedback',
                    link: 'feedback',
                    icon: 'chat'
                },
                {
                    name: 'Report',
                    link: 'report',
                    icon: 'receipt'
                }
            ]
        }
    ];

    private tableColumns = {
// users
        users: [{
            name: 'UserID #',
            prop: 'code',
            flexGrow: 15
        }, {
            name: 'Vor-/Nachname',
            prop: 'name',
            flexGrow: 20
        }, {
            name: 'E-Mail-Adresse',
            prop: 'email',
            flexGrow: 20
        }, {
            name: 'Firma',
            prop: 'company',
            flexGrow: 20
        }, {
            name: 'Benutzerrolle',
            prop: 'role',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }, {
            cellClass: 'justify-content-center',
            name: ' ',
            prop: 'remove',
            flexGrow: 10,
            sortable: false,
            hideOnTablet: true
        }, {
            cellClass: 'justify-content-center',
            name: ' ',
            prop: 'loginAs',
            flexGrow: 15,
            sortable: false
        }],

        // suppliers
        suppliers: [{
            name: 'ID #',
            prop: 'code',
            flexGrow: 15
        }, {
            name: 'Name',
            prop: 'name',
            flexGrow: 20
        }, {
            name: 'Stadt',
            prop: 'city_name',
            flexGrow: 20
        }, {
            name: 'Firmen-E-Mail',
            prop: 'admin_email',
            flexGrow: 20
        }, {
            name: 'Produkte',
            prop: 'products_count',
            flexGrow: 15
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }],

        // orders
        orders: [{
            name: 'BN #',
            prop: 'number',
            flexGrow: 15,
        }, {
            name: 'Datum',
            prop: 'date',
            flexGrow: 20
        }, {
            name: 'Benutzername',
            prop: 'username',
            flexGrow: 20
        }, {
            name: 'Lieferant',
            prop: 'supplier_name',
            flexGrow: 20,
            hideOnTablet: true
        }, {
            name: 'Portalname',
            prop: 'portal_name',
            flexGrow: 25
        }, {
            name: 'Firma',
            prop: 'company_name',
            flexGrow: 25,
            hideOnTablet: true
        }, {
            name: 'Produktname',
            prop: 'product_name',
            flexGrow: 20,
            hideOnTablet: true
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }],

        // contracts
        contracts: [{
            name: 'Vertrag #',
            prop: 'number',
            flexGrow: 15
        }, {
            name: 'Startdatum',
            prop: 'start_date',
            flexGrow: 15
        }, {
            name: 'Enddatum',
            prop: 'end_date',
            flexGrow: 15
        }, {
            name: 'Vor-/Nachname',
            prop: 'username',
            flexGrow: 20
        }, {
            name: 'Portalname',
            prop: 'portal_name',
            flexGrow: 30
        }, {
            name: 'Produktname',
            prop: 'product_name',
            flexGrow: 20
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }],

        // portals
        portals: [{
            name: 'Firmen-ID #',
            prop: 'id',
            flexGrow: 15
        }, {
            name: 'Portalname',
            prop: 'name',
            flexGrow: 30
        }, {
            name: 'Domain',
            prop: 'domain',
            flexGrow: 30
        }, {
            name: 'Status',
            prop: 'status',
            flexGrow: 20
        }]
    };

    public httpSettings = {
        url: 'api',
        token: 'access_token'
    };

    constructor(
        public toasterService: ToasterService,
        private menuService: MenuService,
        private loginService: LoginService,
        private tableService: TableService,
        private authService: AuthenticationService,
        private settingsService: SettingsService,
        private sidebarSettingsService: SidebarSettingsService,
        private interceptService: InterceptSettingsService,
        private authSettings: AuthSettingsService,
        private router: Router) {
        this.interceptService.updateSettings(this.httpSettings);
        this.authSettings.updatePath('');
        this.authSettings.updateToken('access_token');
        this.authSettings.updateCurrentModule('admin');
        this.loginService.loginLogo = this.loginLogo;
        const settings = this.settingsService.settings;
        this.settingsService.updateSettings({...settings, color: this.primaryColor});
        this.menuService.updateMenu(this.mainMenu);
        this.tableService.updateColumns(this.tableColumns);
        this.sidebarSettingsService.updateSettings(this.sidebarSettings);
        this.router.events.subscribe(
            res => {
                if (res instanceof NavigationEnd) {
                    if (res.url.indexOf('admin') === -1) {
                        this.settingsService.updateSettings({...settings, color: this.primaryColor});
                    }
                }
            }
        );
    }

    ngAfterViewInit() {
    }
}
