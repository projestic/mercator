import { Component, OnInit, Input } from '@angular/core';
import { ProductInterface } from '../../common/models/product.interface';
import { OrderInterface } from '../../common/models/order.interface';

@Component({
    selector: 'mrc-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
    @Input() product: ProductInterface;
    @Input() productSize = '';

    constructor() {
    }

    ngOnInit() {
    }

}
