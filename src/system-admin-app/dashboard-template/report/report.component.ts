import { Component, OnInit } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { Report } from './report';
import { NgForm} from '@angular/forms';

@Component({
    selector: 'mrc-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    public reportCategory: Array<object>;
    public report = new Report();
    public submitProcess = false;

    constructor(private httpService: HttpConfigService,
                private toasterService: ToasterService) {
    }

    ngOnInit() {
        this.getReportCategories();
    }

    getReportCategories() {
        this.httpService.getRequest('report/categories')
            .subscribe(
                res => this.reportCategory = res.payload,
                error => this.toasterService.pop('error', '', error)
            );
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach( (key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;

        this.httpService.postRequest('report', this.report)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Your report was send successfully');
                    this.submitProcess = false;
                    form.resetForm();
                },
                error => {
                    this.toasterService.pop('error', '', error);
                    this.submitProcess = false;
                }
            );

    }

}
