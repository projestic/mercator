import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { routes } from './dashboard-template.routes';
import { MaterialModule } from '../material.module';
import { StylesheetComponent } from '../stylesheet/stylesheet.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DndModule } from 'ng2-dnd';
import { CreatePortalComponent } from './create-portal/create-portal.component';
import { SettingsComponent } from './settings/settings.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { ClipboardModule } from 'ngx-clipboard';
import { CclipboardComponent } from '../../components/cclipboard/cclipboard.component';
import { PortalsComponent } from './portals/portals.component';
import { FeedbackComponent } from './feedback/feedback.component';

//Pages
import { AddUserComponent } from './add-user/add-user.component';
import { ContractsComponent } from './contracts/contracts.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { AppCommonModule } from '../../common/app.common.module';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { SupplierDetailComponent } from './supplier-detail/supplier-detail.component';
import { SupplierDetailEditComponent } from './supplier-detail-edit/supplier-detail-edit.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from '../user/user.component';
import { ProductComponent } from '../product/product.component';


//ngx-bootstrap
import { BootstrapModule } from '../bootstrap.module';

import { OrderDetailEditComponent } from './order-detail-edit/order-detail-edit.component';
import { ReportComponent } from './report/report.component';
import { SearchService } from '../../services/search/search.service';


@NgModule({
    declarations: [
        //Components
        StylesheetComponent,
        DashboardComponent,
        CreatePortalComponent,
        CclipboardComponent,
        SettingsComponent,
        PortalsComponent,

        //Pages
        AddUserComponent,
        ContractsComponent,
        OrdersComponent,
        OrderDetailComponent,
        FeedbackComponent,
        ContractDetailComponent,
        SuppliersComponent,
        SupplierDetailComponent,
        SupplierDetailEditComponent,
        UsersComponent,
        UserComponent,
        ProductComponent,
        OrderDetailEditComponent,
        ReportComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        NgSelectModule,
        NgxPaginationModule,
        DndModule.forRoot(),
        ClipboardModule,
        AppCommonModule,
        ColorPickerModule,

        //ngx-bootstrap
        BootstrapModule.forRoot(),
    ],
    providers: [
        SearchService,
    ]
})
export class DashboardTemplateModule {
    public static routes = routes;
}
