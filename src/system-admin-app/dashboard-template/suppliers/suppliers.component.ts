import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'mrc-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {

    constructor(private router: Router) {

    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'City',
            value: 'city'
        },
        {
            name: 'Name',
            value: 'name'
        }
    ];

    public statuses: Array<string> = [
        'Aktiv',
        'Inaktiv'
    ];

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/supplier', e.row.id]);
        }
    };

    ngOnInit() {

    }
}
