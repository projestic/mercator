import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TableService } from '../../../common/components/table/table.service';

@Component({
  selector: 'mrc-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {

    constructor(
        private router: Router,
        private tableService: TableService
    ) {

    }

    public columns = [];

    public filter: Array<any> = [
        {
            name: 'Portal',
            value: 'portal'
        },
        {
            name: 'Product',
            value: 'product'
        },
        {
            name: 'Startdatum',
            value: 'start_date',
            type: 'datepicker'
        },
        {
            name: 'Enddatum',
            value: 'end_date',
            type: 'datepicker'
        }
    ];

    ngOnInit() {}

    public statuses: Array<string> = [
        'Aktiv',
        'Abgelaufen'
    ];

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/contract', e.row.id]);
        }
    }

    public print(): void {
        window.print();
    }

    public export():void {
        this.tableService.export('contracts', 'Verträge');
    }
}
