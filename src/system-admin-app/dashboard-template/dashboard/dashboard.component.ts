import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { AddWidgetComponent } from '../../../common/components/add-widget/add-widget.component';

const compareNumeric = (a, b) => {
    if (a.position > b.position) return 1;
    if (a.position < b.position) return -1;
}

@Component({
    selector: 'mrc-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [DatePipe]
})

export class DashboardComponent implements OnInit {
    title = 'Dashboard';
    public portals: Array<{id: string | number; name: string}> = [];
    public widgets: Array<any> = [];
    public currentDashboard: CurrentDashboardInterface = new CurrentDashboard();
    bsRangeValue: Date[];
    public sourceArr: Array<any>;
    public widgetTitles: object;
    public preloader: Array<boolean>;
    @ViewChild('addWidget') addWidget: AddWidgetComponent;

    constructor(private datePipe: DatePipe,
                private toasterService: ToasterService,
                private httpService: HttpConfigService) {
        this.getSourcesList();
    }

    public ngOnInit() {
        this.getPortalsList();
    }

    public getDates(data) {
        this.currentDashboard.dateFrom = (new Date(data[0]).getTime() / 1000);
        this.currentDashboard.dateTo = (new Date(data[1]).getTime() / 1000);
        this.getWidgets(this.currentDashboard);
    }

    public portalChange(value) {
        this.getWidgets(this.currentDashboard);
    }

    public getPortalsList() {
        this.httpService.getRequest('portals/all')
            .subscribe(
                res => this.portals = res.payload,
                error => console.log(error)
            );
    }

    public getSourcesList() {
        this.httpService.getRequest('widgets/sources')
            .subscribe(
                res => {
                    this.widgetTitles = res.payload;
                    const widgetTitlesProp = Object.keys(this.widgetTitles);
                    this.sourceArr = widgetTitlesProp.map(item => {
                        return {
                            value: item,
                            label: this.widgetTitles[item]
                        };
                    });
                    this.getWidgets(this.currentDashboard);
                },
                error => console.log(error)
            );
    }

    public getPortalName() {
        let portalName;
        this.portals.forEach(item => {
            if (item.id === this.currentDashboard.portalId) {
                portalName = item.name;
            }
        });
        return portalName;
    }

    public getWidgets({portalId, dateFrom, dateTo}: CurrentDashboardInterface) {
        this.httpService.getRequest(`widgets?portal_id=${portalId}&date_from=${dateFrom}&date_to=${dateTo}`)
            .subscribe(
                res => {
                    this.preloader = new Array(res.payload.length);
                    this.widgets = [...res.payload.sort(compareNumeric)];
                    if (this.widgets.length > 0) this.changeSort();
                },
                error => console.log(error)
            );
    }

    public deleteWidget(value) {
        this.httpService.deleteRequest(`widgets/${value}`)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Widget was removed successfully');
                    this.getWidgets(this.currentDashboard);
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

    public updateWidget(value) {
        const data = {...value};
        delete data.id;
        this.preloader[(value.position - 1)] = true;
        this.httpService.postRequest(`widgets/${value.id}`, data)
            .subscribe(
                res => this.getWidgetById(value.id, this.currentDashboard),
                error => {
                    this.preloader[(value.position - 1)] = false;
                    this.addWidget.openErrorModal(error.source);
                }
            );
    }

    public getWidgetById(id, {portalId, dateFrom, dateTo}: CurrentDashboardInterface) {
        this.httpService.getRequest(`widgets/${id}?portal_id=${portalId}&date_from=${dateFrom}&date_to=${dateTo}`)
            .subscribe(
                res => {
                    this.widgets = this.widgets.map(item => {
                        if (item.position === res.payload.position) {
                            item = {...res.payload};
                            this.preloader[(res.payload.position - 1)] = false;
                        }
                        return item;
                    });
                },
                error => console.log(error)
            );
    }

    public changeSort() {
        const data = {};
        for ( let i = 0; i < this.widgets.length; i++) {
            data[this.widgets[i].id] = i + 1;
        }
        this.httpService.postRequest('widgets/positions', {positions: data})
            .subscribe(
                res => res,
                error => console.log(error)
            );
    }
}

interface CurrentDashboardInterface {
    portalId?: string | number;
    dateFrom?: string | number;
    dateTo?: string | number;
}

class CurrentDashboard {
    public portalId: string | number = 30;
    public dateFrom: string | number =  Math.floor(new Date(new Date().getFullYear(),
        new Date().getMonth(), 1).getTime() / 1000) - new Date().getTimezoneOffset() * 60;
    public dateTo: string | number = Math.floor((new Date().getTime() / 1000)) - new Date().getTimezoneOffset() * 60;

    constructor(data?: CurrentDashboardInterface) {
        if (data) {
            this.portalId = data.portalId;
            this.dateFrom = data.dateFrom;
            this.dateTo = data.dateTo;
        }
    }
}

