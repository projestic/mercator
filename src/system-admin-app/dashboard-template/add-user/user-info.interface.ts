export interface UserInfoInterface {
    role: string | number;
    portal_id?: string | number;
    first_name: string;
    last_name: string;
    email: string;
    status: string;
}
