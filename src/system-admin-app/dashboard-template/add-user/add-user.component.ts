import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../services/http/http.service';
import { UserInfo } from './user-info';
import { ToasterService } from 'angular5-toaster';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'mrc-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
    @ViewChild('addUser') addUser: HTMLFormElement;
    public message = 'When the user is created he will receive an email...';

    public userPath: string;
    public userId: string | number;
    public submitProcess = false;

    public roles: Array<object> = [
        {
            id: 0,
            name: 'System administrator'
        },
        {
            id: 1,
            name: 'Portal administrator'
        }
    ];

    public portals: Array<object>;

    public userInfo = new UserInfo();

    constructor(private httpService: HttpConfigService,
                private toasterService: ToasterService,
                private route: ActivatedRoute) {
        this.route.url.subscribe(params => {
            if (params.length > 1) {
                this.userPath = params[1].path;
            }
        });
        this.route.params.subscribe(params => {
            this.userId = params.id;

            this.getPortalsList();

            if (this.userPath) {
                this.getUserInfo();
            }
        });
    }

    ngOnInit() {

    }

    getUserInfo() {
        let url = `users/${this.userId}`;
        if (this.userPath === 'portal') url = `portal-users/${this.userId}`;

        this.httpService.getRequest(url)
            .subscribe(
                res => {
                    this.userInfo = new UserInfo(res.payload);
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }

    getPortalsList() {
        this.httpService.getRequest('portals/all')
            .subscribe(
                res => this.portals = res.payload
            );
    }

    public aktiveToggle() {
        if (this.userInfo.statusBool) {
            this.userInfo.status = 'Aktiv';
        } else {
            this.userInfo.status = 'Inaktiv';
        }
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;
        this.aktiveToggle();
        let url;
        let data;
        if (this.userInfo.role === 0) {
            url = 'users';
            data = {
                first_name: this.userInfo.first_name,
                last_name: this.userInfo.last_name,
                email: this.userInfo.email,
                status: this.userInfo.status
            };
        } else {
            url = 'portal-users';
            data = {
                first_name: this.userInfo.first_name,
                last_name: this.userInfo.last_name,
                portal_id: this.userInfo.portal_id,
                email: this.userInfo.email,
                status: this.userInfo.status
            };
        }
        let message = 'added';
        if (this.userId) {
            url += `/${this.userId}`;
            message = 'update';
        }
        this.httpService.postRequest(url, data)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `User was ${message} successfully`);
                    if (this.userId) {
                        this.getUserInfo();
                    } else {
                        form.resetForm();
                    }
                    ;
                    this.submitProcess = false;
                },
                res => {

                    if (res.status === 422) {
                        this.httpService.setFormErrors(this.addUser, res.error.payload);
                    }

                    this.submitProcess = false;
                });
    }
}
