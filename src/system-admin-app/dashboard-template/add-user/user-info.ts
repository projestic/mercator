import { UserInfoInterface } from './user-info.interface';

export class UserInfo implements UserInfoInterface {
    role: string | number = 0;
    portal_id?: string | number = 0;
    first_name: string;
    last_name: string;
    email: string;
    statusBool: boolean;
    status: string;

    constructor(user?: UserInfoInterface) {
        if (user) {
            this.first_name = user.first_name;
            this.last_name = user.last_name;
            this.status = user.status;
            this.portal_id = user.portal_id;
            this.email = user.email;
            if (this.portal_id) this.role = 1;
        }
        this.statusBool = this.statusCheck();
    }

    statusCheck() {
        if (this.status === 'Inaktiv') return false;
        return true;
    }
}
