import { Component, OnInit } from '@angular/core';
import { HttpConfigService } from '../../../services/http/http.service';
import { TableService } from '../../../common/components/table/table.service';
import { Router } from '@angular/router';

@Component({
    selector: 'mrc-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    public statuses: Array<string> = [
        'Aktiv',
        'Inaktiv'
    ];

    public filter: Array<any> = [
        {
            name: 'Company',
            value: 'company'
        },
        {
            name: 'Role',
            value: 'role',
            type: 'role',
            options: [
                'Portalbesitzer',
                'Administrator'
            ]
        }
    ];

    public actions = {
        'click': e => {
            if (e.column.prop !== 'remove') {
                this.router.navigate(['/admin/user', e.row.id, e.row.type, 'edit']);
            }
        }
    };

    constructor(private httpService: HttpConfigService,
                private tableService: TableService,
                private router: Router) {

    }

    public columns = [];

    ngOnInit() {

    }

    public print(): void {
        window.print();
    }

    public export(): void {
        this.tableService.export('users', 'Benutzer');
    }
}
