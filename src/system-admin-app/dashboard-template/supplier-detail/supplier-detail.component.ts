import {Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { NgForm} from '@angular/forms';
import { SupplierInfo } from '../../../common/models/supplier';
import { SupplierInfoInterface } from '../../../common/models/supplier.interface';

@Component({
    selector: 'mrc-supplier-detail',
    templateUrl: './supplier-detail.component.html',
    styleUrls: ['./supplier-detail.component.scss']
})
export class SupplierDetailComponent implements OnInit {

    productTitle: Array<string> = ['Lieferant', 'Produkte'];
    public supplierId: string | number;
    public supplierInfo: SupplierInfoInterface = new SupplierInfo();
    public portals: Array<object>;
    public formStatus = false;
    public duplicateSupplierObj = {
        portal_id: null
    };

    product: { name: string, value: string }[] = [
        {
            name: 'Fahrradladen GmbH',
            value: '25'
        },
        {
            name: 'Fahrradladen GmbH',
            value: '31'
        },
        {
            name: 'Fahrradladen GmbH',
            value: '52'
        },
        {
            name: 'Fahrradladen GmbH',
            value: '51'
        }
    ];

    firmaTitle: Array<string> = ['Firma', 'Mitarbeiter'];

    firma: { name: string, value: string }[] = [
        {
            name: 'Musterfirma GmbH',
            value: '12'
        },
        {
            name: 'Musterfirma GmbH',
            value: '5'
        },
        {
            name: 'Musterfirma GmbH',
            value: '27'
        },
        {
            name: 'Musterfirma GmbH',
            value: '51'
        }
    ];

    infoTable: Array<object>;

    constructor(private route: ActivatedRoute,
                private httpService: HttpConfigService,
                private toasterService: ToasterService) {
        this.route.params.subscribe( params => {
            this.supplierId = params.id;

            this.getSupplierInfo();
            this.getPortalsList();
        } );
    }

    ngOnInit() {

    }

    toggleForm() {
        this.formStatus = !this.formStatus;
    }

    getSupplierInfo() {
        this.httpService.getRequest(`suppliers/${this.supplierId}`)
            .subscribe(
                res => {
                    this.supplierInfo = new SupplierInfo(res.payload);
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }

    getPortalsList() {
        this.httpService.getRequest('portals/all')
            .subscribe(
                res => this.portals = res.payload
            );
    }

    duplicate(form: NgForm): void {
        Object.keys(form.controls).forEach( (key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;

        const url = `suppliers/${this.supplierInfo.id}/duplicate`;
        this.httpService.postRequest(url, this.duplicateSupplierObj)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Supplier was duplicate successfully');
                    form.resetForm();
                    this.formStatus = false;
                },
                error => {
                    this.toasterService.pop('error', '', error);
                });
    }
}
