import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'mrc-portals',
    templateUrl: './portals.component.html',
    styleUrls: ['./portals.component.scss'],
})
export class PortalsComponent implements OnInit {

    public statuses: Array<string> = [
        'Aktiv',
        'Inaktiv'
    ];

    constructor(private router: Router) {

    }

    public columns = [];

    ngOnInit() {

    }

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/portal', e.row.id, 'edit']);
        }
    }
}
