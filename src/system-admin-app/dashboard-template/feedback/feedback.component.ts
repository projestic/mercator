import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { UserService } from '../../../services/user/user.service';


@Component({
    selector: 'mrc-feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.scss'],
    providers: [HttpConfigService]
})
export class FeedbackComponent implements OnInit {

    public feedbackCategory: Array<object>;
    public feedback = new Feedback();
    public userInfo: object;
    public submitProcess = false;

    constructor(private httpService: HttpConfigService,
                private toasterService: ToasterService,
                private user: UserService) {

    }

    ngOnInit() {
        this.getFeedbackCategories();
        this.userInfo = this.user.getUserInfo();
    }

    getFeedbackCategories() {
        this.httpService.getRequest('feedback/categories')
            .subscribe(
                res => this.feedbackCategory = res.payload,
                error => this.toasterService.pop('error', '', error)
            );
    }

    create(form: NgForm): void {
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        if (form.invalid) return;
        this.submitProcess = true;

        this.httpService.postRequest('feedback', this.feedback)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Your feedback was send successfully');
                    this.submitProcess = false;
                    form.resetForm();
                },
                error => {
                    this.toasterService.pop('error', '', error);
                    this.submitProcess = false;
                }
            );

    }
}


class Feedback {
    public category_id;
    public body;
}
