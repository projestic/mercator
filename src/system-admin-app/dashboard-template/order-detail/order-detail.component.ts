import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../common/models/user';
import { UserInterface } from '../../../common/models/user.interface';
import { Leasing } from '../../../common/models/leasing';
import { LeasingInterface } from '../../../common/models/leasing.interface';
import { ProductInterface } from '../../../common/models/product.interface';
import { Product } from '../../../common/models/product';
import { HttpConfigService } from '../../../services/http/http.service';
import { OrderInterface } from '../../../common/models/order.interface';
import { Order } from '../../../common/models/order';
import { ToasterService } from 'angular5-toaster';

@Component({
    selector: 'mrc-order-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
    public orderId: string | number;
    public orderTitle = 'Bestellen';
    public user: UserInterface;
    public order: OrderInterface = new Order();
    public leasing: LeasingInterface;
    public product: ProductInterface;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private toasterService: ToasterService,
                private httpConfig: HttpConfigService) {

        this.route.params.subscribe( params => {
            this.orderId = params.id;
        });
    }

    ngOnInit() {
        if (this.orderId) this.getOrderDetailInfo();
    }

    getOrderDetailInfo() {
        this.httpConfig.getRequest(`orders/${this.orderId}`)
            .subscribe(
                res => {
                    this.user = new User(res.payload.user);
                    this.order = new Order(res.payload);
                    this.leasing = new Leasing(res.payload);
                    this.product = new Product(res.payload.product);
                },
                error => console.log(error)
            );
    }

    public print(): void {
        window.print();
    }

    public export(): void {
        this.httpConfig.export(`orders/${this.orderId}`, `Bestellen #${this.orderId}`);
    }

    public convert(): void {
        this.httpConfig.postRequest(`orders/${this.orderId}/convert`, {})
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', `Order was converted to contract successfully`);
                    this.router.navigate(['/admin/contract', res.payload.id]);
                },
                error => this.toasterService.pop('error', '', error)
            );
    }

}
