import { UserInterface } from './user.interface';

export class User {
    public first_name: string;
    public last_name: string;
    public email: string;
    public id: string | number;

    constructor(user?: UserInterface) {
        if (user) {
            this.first_name = user.first_name || '';
            this.last_name = user.last_name || '';
            this.email = user.email || '';
            this.id = user.id || '';
        }
    }
}
