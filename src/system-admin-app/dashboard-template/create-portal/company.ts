import { CompanyInterface } from './company.interface';

export class Company {
    public name: string;
    public city_id: string | number;
    public address: string;
    public vat: string;
    public id: string | number;

    constructor(company?: CompanyInterface) {
        if (company) {
            this.name = company.name || '';
            this.city_id = company.city_id || '';
            this.address = company.address || '';
            this.vat = company.vat || '';
            this.id = company.id || '';
        }
    }
}
