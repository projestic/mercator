import { Component, OnInit, Input, ViewChild, TemplateRef, ElementRef, ChangeDetectorRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { NgForm } from '@angular/forms';
import { Portal } from './portal';
import fileSaver from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import {ToasterService} from 'angular5-toaster';
import { HttpConfigService } from '../../../services/http/http.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { findIndex } from 'lodash';

@Component({
    selector: 'mrc-create-portal',
    templateUrl: './create-portal.component.html',
    styleUrls: ['./create-portal.component.scss']
})
export class CreatePortalComponent implements OnInit {

    @ViewChild('createPortalForm') createPortalForm: HTMLFormElement;

    public mobileQuery: MediaQueryList;

    public portalData: any;
    public portal: Portal = new Portal();
    public portalCreated: boolean = false;
    public companyCityList: Array<object>;

    public portalId: string;

    public error;
    public submitProcess = false;

    constructor(
        private httpService: HttpConfigService,
        private route: ActivatedRoute,
        private toasterService: ToasterService,
        private media: MediaMatcher,
    ) {

        this.route.params.subscribe(params => {

            if (params.hasOwnProperty('id')) {
                this.portalId = params.id;

                this.getPortalInfo(this.portalId);
            }
        });
    }

    ngOnInit() {
        this.getCities();

        this.mobileQuery = this.media.matchMedia('(max-width: 767px)');

        document.addEventListener('sidemenu.link.click', e => this.portalCreated = false);
    }

    getPortalInfo(id) {
        this.httpService.getRequest(`portals/${id}`).subscribe(
            res => {
                this.portalData = res.payload;
                this.portal = new Portal(this.portalData);
            },
            error => this.error = error
        );
    }

    getCities() {
        const url = 'geo/cities';
        this.httpService.getRequest(url).subscribe(res => {
            this.companyCityList = res.payload;
        });
    }


    getPortalPdf(id) {
        const url = `portals/${id}/pdf`;

        const options = {
            responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'application/pdf',
            },
            observe: 'response'
        };

        const type = 'application/pdf';

        this.httpService.getFileRequest(url, options, type).subscribe(res => {
            const filename = res.headers.get('content-disposition').match(/filename=(.+)/)[1].slice(1, -1);
            fileSaver.saveAs(res.body, filename);
        });
    }

    public aktiveToggle() {
        if (this.portal.statusBool) {
            this.portal.status = 'Aktiv';
        } else {
            this.portal.status = 'Inaktiv';
        }
    }

    public create(form: NgForm): void {

        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        this.aktiveToggle();

        if (form.invalid) return;

        this.submitProcess = true;

        let url;

        if (this.portalId) {
            url = `portals/${this.portalId}`;
            this.httpService.postRequest(url, this.portal)
                .subscribe(
                    res => {
                        this.toasterService.pop('success', '', 'Portal was updated successfully');
                        this.submitProcess = false;
                        this.portalData = res.payload;
                        this.resetForm();
                    },
                    res => {

                        if (res.status === 422) {
                            this.httpService.setFormErrors(this.createPortalForm, res.error.payload);
                        }

                        this.submitProcess = false;
                    });
        } else {
            url = `portals`;
            this.httpService.postRequest(url, this.portal)
                .subscribe(
                    res => {
                        this.toasterService.pop('success', '', 'Portal was added successfully');
                        this.portal = new Portal(res.payload);
                        this.portalCreated = true;
                        this.submitProcess = false;
                        form.resetForm();
                    },
                    res => {

                        if (res.status === 422) {
                            this.httpService.setFormErrors(this.createPortalForm, res.error.payload);
                        }

                        this.submitProcess = false;
                    });
        }
    }

    public resetForm(): void {
        this.portal = new Portal(this.portalData);

        let form = this.createPortalForm;

        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsPristine();
        });
    }
}
