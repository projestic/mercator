import { PortalInterface } from './portal.interface';
import { LeasingCondition } from '../../../common/models/leasing-condition';

export class Portal implements PortalInterface {
    public id: number;
    public name: string;
    public domain: string;
    public statusBool: boolean = true;
    public status: string;

    //user
    public admin_email: string;
    public admin_first_name: string;
    public admin_last_name: string;

    //company
    public company_address: string;
    public company_city_id: number;
    public company_name: string;
    public company_vat: string;

    //application
    public application_key: string;

    public maximumUsers: number;
    public option1: boolean;
    public option2: boolean;
    public option3: boolean = true;
    public option4: boolean;
    public option5: boolean;
    public option6: boolean;

    public leasing_settings: Array<LeasingCondition> = [];

    constructor(portal?: PortalInterface) {

        if (portal) {
            this.id = portal.id;
            this.name = portal.name || '';
            this.domain = portal.domain || '';
            this.status = portal.status;

            this.admin_email = portal.admin_email;
            this.admin_first_name = portal.admin_first_name;
            this.admin_last_name =  portal.admin_last_name;

            this.company_address = portal.company_address;
            this.company_city_id = portal.company_city_id;
            this.company_name =  portal.company_name;
            this.company_vat =  portal.company_vat;

            this.application_key = portal.application_key;

            if (portal.leasing_settings.length > 0) {
                portal.leasing_settings.forEach(item => {
                    this.leasing_settings.push(new LeasingCondition(item));
                });
            }
        }

        this.statusBool = this.statusCheck();
    }

    statusCheck() {
        if (this.status === 'Inaktiv') return false;
        return true;
    }
}
