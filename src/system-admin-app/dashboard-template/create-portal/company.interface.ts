export interface CompanyInterface {
    name: string;
    city_id: string | number;
    address: string;
    vat: string;
    id?: string | number;
}
