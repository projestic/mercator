import { LeasingCondition } from '../../../common/models/leasing-condition';

export interface PortalInterface {
    id: number;
    name: string;
    domain: string;
    statusBool: boolean;
    status: string;

    admin_email: string;
    admin_first_name: string;
    admin_last_name: string;

    company_address: string;
    company_city_id: number;
    company_name: string;
    company_vat: string;

    application_key?: string;

    maximumUsers: number;
    option1: boolean;
    option2: boolean;
    option3: boolean;
    option4: boolean;
    option5: boolean;
    option6: boolean;

    leasing_settings: Array<LeasingCondition>;
}
