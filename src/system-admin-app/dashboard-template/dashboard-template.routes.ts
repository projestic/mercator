import { DashboardTemplateComponent} from '../../common/components/dashboard-template/dashboard-template.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StylesheetComponent } from '../stylesheet/stylesheet.component';
import { CreatePortalComponent } from './create-portal/create-portal.component';
import { SettingsComponent } from './settings/settings.component';
import { PortalsComponent } from './portals/portals.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { AddUserComponent } from './add-user/add-user.component';
import { OrdersComponent } from './orders/orders.component';
import { ContractsComponent } from './contracts/contracts.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { SearchComponent } from '../../common/pages/search/search.component';
import { SupplierDetailComponent } from './supplier-detail/supplier-detail.component';
import { SupplierDetailEditComponent } from './supplier-detail-edit/supplier-detail-edit.component';
import { UsersComponent } from './users/users.component';
import { OrderDetailEditComponent } from './order-detail-edit/order-detail-edit.component';
import { ReportComponent } from './report/report.component';

export const routes = [
    {
        path: '',
        component: DashboardTemplateComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'stylesheet',
                component: StylesheetComponent
            },
            {
                path: 'orders',
                children: [
                    {
                        path: '',
                        component: OrdersComponent,
                        pathMatch: 'full'
                    }
                ]
            },
            {
                path: 'order',
                children: [
                    {
                        path: ':id',
                        component: OrderDetailComponent
                    },
                    {
                        path: ':id/edit',
                        component: OrderDetailEditComponent
                    }
                ]
            },
            {
                path: 'contracts',
                children: [
                    {
                        path: '',
                        component: ContractsComponent,
                        pathMatch: 'full'
                    }
                ]
            },
            {
                path: 'contract',
                children: [
                    {
                        path: ':id',
                        component: ContractDetailComponent
                    }
                ]
            },
            {
                path: 'create-portal',
                component: CreatePortalComponent
            },
            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'user',
                children: [
                    {
                        path: ':id/system/edit',
                        component: AddUserComponent
                    },
                    {
                        path: ':id/portal/edit',
                        component: AddUserComponent
                    }
                ]
            },
            {
                path: 'add-user',
                component: AddUserComponent
            },
            {
                path: 'search',
                component: SearchComponent
            },
            {
                path: 'suppliers',
                component: SuppliersComponent
            },
            {
                path: 'add-supplier',
                component: SupplierDetailEditComponent
            },
            {
                path: 'supplier',
                children: [
                    {
                        path: ':id',
                        component: SupplierDetailComponent
                    },
                    {
                        path: ':id/edit',
                        component: SupplierDetailEditComponent
                    }
                ]
            },
            {
                path: 'portals',
                component: PortalsComponent
            },
            {
                path: 'portal',
                children: [
                    {
                        path: ':id/edit',
                        component: CreatePortalComponent
                    }
                ]
            },
            {
                path: 'settings',
                component: SettingsComponent
            },
            {
                path: 'feedback',
                component: FeedbackComponent
            },
            {
                path: 'report',
                component: ReportComponent
            },
            {
                path: '',
                redirectTo: '/admin/dashboard',
                pathMatch: 'full'
            }
        ]
    },
];
