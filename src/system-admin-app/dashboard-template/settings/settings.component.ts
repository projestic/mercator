import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm} from '@angular/forms';
import { EmailValidator } from '@angular/forms';
import { ToasterService } from 'angular5-toaster';
import { HttpConfigService } from '../../../services/http/http.service';
import timezones from 'google-timezones-json';
import { forIn, find } from 'lodash';
import { SettingsService } from '../../../services/settings/settings.service';
import { AdminSettings } from '../../../common/models/admin-settings';

@Component({
    selector: 'mrc-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
    @ViewChild('settingsForm') settingsForm: HTMLFormElement;

    public settings: AdminSettings = new AdminSettings({});
    public selectedColor: string;
    public submitting: boolean = false;

    constructor(
        private http: HttpConfigService,
        private toasterService: ToasterService,
        private settingsService: SettingsService
    ) {

    }

    ngOnInit() {
        this.getSettings();

        //create timezones list
        this.timezones = this.getTimezomesList(timezones);
    }

    private getSettings() {
        this.http.getRequest(`settings`).subscribe(
            res => {
                this.settings = new AdminSettings(res.payload);

                this.selectedColor = res.payload.color;

                this.logo.imageUrl = res.payload.logo;

                //set date format
                let selectedDateFormat = find(this.dataFormats, o => o.value === res.payload.date_format );

                if (selectedDateFormat) {
                    this.selectedDateFormat = selectedDateFormat.value;
                    this.settings.date_format = null;
                } else {
                    this.selectedDateFormat = 'custom';
                }

                //set time format
                let selectedTimeFormat = find(this.timeFormats, o => o.value === res.payload.time_format );

                if (selectedTimeFormat) {
                    this.selectedTimeFormat = selectedTimeFormat.value;
                    this.settings.time_format = null;
                } else {
                    this.selectedTimeFormat = 'custom';
                }

            },
            error => {
                this.toasterService.pop('error', '', error);
            }
        );
    }

    //Colors
    public colors: Array<string> = [
        '#50e3c2',
        '#6fc213',
        '#f9a110',
        '#4a90e2',
        '#ec4640',
        '#cc56e4',
        '#b8e986',
        '#91b6e0',
        '#ffabb5',
        '#d1935c'
    ];

    public updateColor(): void {
        this.settings.color = this.selectedColor;
    }

    public resetColor(): void {
        this.selectedColor = this.settings.color;
    }

    //Image
    public logo = new ImageUploader(this.toasterService);

    //Timezones
    public timezones: Array<any> = [];

    private getTimezomesList(timezones): Array<any> {
        let zones: Array<any> = [];

        forIn(timezones, (value, key) => {
            zones.push({
                key: key,
                value: value
            });
        });

        return zones;
    }

    //Date formats
    public selectedDateFormat: string;

    public dataFormats: Array<object> = [
        {
            name: 'März 22, 2018',
            value: 'F j, Y'
        },
        {
            name: '2018-03-22',
            value: 'Y-m-d'
        },
        {
            name: '03/22/2018',
            value: 'm/d/Y'
        }
    ];

    private getDateFormat(): string {
        return this.selectedDateFormat === 'custom' ?  this.settings.date_format :this.selectedDateFormat;
    }

    //Date formats
    public selectedTimeFormat: string;

    public timeFormats: Array<object> = [
        {
            name: '3:17 pm',
            value: 'g:i a'
        },
        {
            name: '3:17 PM',
            value: 'g:i A'
        },
        {
            name: '15:17',
            value: 'g:i'
        }
    ];

    private getTimeFormat(): string {
        return this.selectedTimeFormat === 'custom' ?  this.settings.time_format :this.selectedTimeFormat;
    }

    //Start day
    public daysOfWeek: Array<any> = [{
        key: 'Monday',
        value: 'Montag'
    },{
        key: 'Tuesday',
        value: 'Dienstag'
    }, {
        key: 'Wednesday',
        value: 'Mittwoch'
    }, {
        key: 'Thursday',
        value: 'Donnerstag'
    }, {
        key: 'Friday',
        value: 'Freitag'
    }, {
        key: 'Saturday',
        value: 'Samstag'
    }, {
        key: 'Sunday',
        value: 'Sonntag'
    }];


    public update(form: NgForm): void {

        if (form.invalid) {

            Object.keys(form.controls).forEach((key, val) => {
                form.controls[key].markAsDirty();
            });

            return;
        }

        //Reset form validation
        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsPristine();
        });

        //Create formData
        let formData = new FormData();

        Object.keys(this.settings).forEach((key, val) => {

            switch (key) {
                case 'logo':

                    if (this.logo.imageData) {
                        formData.append('logo', this.logo.image);
                    } else {
                        formData.append('logo', this.logo.imageUrl);
                    }

                    break;
                case 'date_format':
                    formData.append(key, this.getDateFormat());
                    break;
                case 'time_format':
                    formData.append(key, this.getTimeFormat());
                    break;
                default:
                    formData.append(key, this.settings[key]);
            }
        });

        //send update request
        this.submitting = true;

        this.http.postRequest('settings', formData).subscribe(
            res => {
                this.submitting = false;
                this.toasterService.pop('success', '', 'Settings was updated successfully');

                this.settingsService.settings = res.payload;
                this.settingsService.updateSettings(res.payload);
            },
            res => {
                this.submitting = false;

                if (res.status === 422) {
                    this.http.setFormErrors(this.settingsForm, res.error.payload);
                }
            }
        );
    }
 }

class ImageUploader {
    public image;
    public imageUrl;
    public imageData;

    constructor(private toasterService: ToasterService) {

    }

    load(file): void {
        if (file.item(0).type.indexOf('image') === -1) {
            this.toasterService.pop('error', '', 'Please add correct image');
            return;
        };
        this.image = file.item(0);

        const reader = new FileReader();
        reader.onload = (event: any) => {
            this.imageData = event.target.result;
        };
        reader.readAsDataURL(this.image);
    }

    remove(): void {
        this.image = '';
        this.imageUrl = '';
        this.imageData = null;
    }

}
