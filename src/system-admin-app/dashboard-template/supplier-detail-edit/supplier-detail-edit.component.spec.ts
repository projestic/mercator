import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierDetailEditComponent } from './supplier-detail-edit.component';

describe('SupplierDetailEditComponent', () => {
  let component: SupplierDetailEditComponent;
  let fixture: ComponentFixture<SupplierDetailEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierDetailEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierDetailEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
