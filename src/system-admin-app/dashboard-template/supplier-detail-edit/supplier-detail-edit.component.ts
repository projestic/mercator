import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SupplierInfo } from '../../../common/models/supplier';
import { SupplierInfoInterface } from '../../../common/models/supplier.interface';
import { ActivatedRoute } from '@angular/router';
import { HttpConfigService } from '../../../services/http/http.service';
import { ToasterService } from 'angular5-toaster';
import { Router } from '@angular/router';

@Component({
    selector: 'mrc-supplier-detail-edit',
    templateUrl: './supplier-detail-edit.component.html',
    styleUrls: ['./supplier-detail-edit.component.scss']
})
export class SupplierDetailEditComponent implements OnInit {
    @ViewChild('editSupplier') editSupplier: HTMLFormElement;
    public supplierId: string | number;
    public supplierInfo: SupplierInfoInterface = new SupplierInfo();
    public companyCityList: Array<object>;
    public portals: Array<object>;
    public submitProcess = false;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private httpService: HttpConfigService,
                private toasterService: ToasterService) {
        this.route.params.subscribe( params => {
            this.supplierId = params.id;
        } );
    }

    ngOnInit() {
        if (this.supplierId) this.getSupplierInfo();
        this.getCities();
        this.getPortalsList();
    }

    getSupplierInfo() {
        this.httpService.getRequest(`suppliers/${this.supplierId}`)
            .subscribe(
                res => {
                    this.supplierInfo = new SupplierInfo(res.payload);
                },
                error => {
                    this.toasterService.pop('error', '', error);
                }
            );
    }

    getCities() {
        const url = 'geo/cities';
        this.httpService.getRequest(url).subscribe(res => {
            this.companyCityList = res.payload;
        });
    }

    getPortalsList() {
        this.httpService.getRequest('portals/all')
            .subscribe(
                res => this.portals = res.payload
            );
    }

    public aktiveToggle() {
        if (this.supplierInfo.statusBool) {
            this.supplierInfo.status = 'Aktiv';
        } else {
            this.supplierInfo.status = 'Inaktiv';
        }
    }

    public create(form: NgForm): void {

        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });
        this.aktiveToggle();
        const data = {...this.supplierInfo};
        delete data.city;
        delete data.portals;
        delete data.id;
        delete data.active_from;
        delete data.products_count;
        delete data.statusBool;
        delete data.updated_at;
        if (form.invalid) return;

        this.submitProcess = true;

        if (this.supplierId) {
            this.httpService.postRequest(`suppliers/${this.supplierId}`, data)
                .subscribe(
                    res => {
                        this.toasterService.pop('success', '', `${res.payload.name} was updated successfully`);
                        this.submitProcess = false;
                    },
                    res => {

                        if (res.status === 422) {
                            this.httpService.setFormErrors(this.editSupplier, res.error.payload);
                        }

                        this.submitProcess = false;
                    }
                );
        } else {
            this.httpService.postRequest(`suppliers`, data)
                .subscribe(
                    res => {
                        this.toasterService.pop('success', '', `${res.payload.name} was created successfully`);
                        this.router.navigate(['/admin/supplier', res.payload.id]);
                        this.submitProcess = false;
                    },
                    res => {

                        if (res.status === 422) {
                            this.httpService.setFormErrors(this.editSupplier, res.error.payload);
                        }

                        this.submitProcess = false;
                    }
                );
        }


    }
}
