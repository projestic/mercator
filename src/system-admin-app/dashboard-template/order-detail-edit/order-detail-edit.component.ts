import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../common/models/user';
import { UserInterface } from '../../../common/models/user.interface';
import { ProductInterface } from '../../../common/models/product.interface';
import { Product } from '../../../common/models/product';
import { HttpConfigService } from '../../../services/http/http.service';
import { OrderInterface } from '../../../common/models/order.interface';
import { Order } from '../../../common/models/order';
import { ToasterService } from 'angular5-toaster';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'mrc-order-detail-edit',
    templateUrl: './order-detail-edit.component.html',
    styleUrls: ['./order-detail-edit.component.scss']
})
export class OrderDetailEditComponent implements OnInit {
    public orderId: string | number;
    public orderTitle = 'Bestellen';
    public link: string;
    public user: UserInterface;
    public order: OrderInterface = new Order();
    public product: ProductInterface = new Product();
    public sizeList: Array<string> = [
        '54-56 cm',
        '58-60 cm',
        '62-64 cm',
        '66-68 cm'
    ];
    public leasingList = this.leasingListArr();
    public statusesList: Array<string>;
    public submitProcess = false;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private toasterService: ToasterService,
                private httpConfig: HttpConfigService) {
        this.route.params.subscribe( params => {
            this.orderId = params.id;
            this.link = `../../${this.orderId}`;
        });
    }

    ngOnInit() {
        if (this.orderId) this.getOrderDetailInfo();
        this.getStatuses();
    }

    getStatuses() {
        this.httpConfig.getRequest('orders/statuses')
            .subscribe(
                res => this.statusesList = res.payload
            );
    }

    leasingListArr(): Array<object> {
        const arr = [];
        for ( let i = 1; i <= 48; i++ ) {
            arr.push({
                id: i,
                name: `${i} Monate`
            });
        }
        return arr;
    }

    getOrderDetailInfo() {
        this.httpConfig.getRequest(`orders/${this.orderId}`)
            .subscribe(
                res => {
                    this.user = new User(res.payload.user);
                    this.order = new Order(res.payload);
                    this.product = new Product(res.payload.product);
                },
                error => console.log(error)
            );
    }

    public create(form: NgForm): void {

        Object.keys(form.controls).forEach((key, val) => {
            form.controls[key].markAsDirty();
        });

        if (form.invalid) return;
        this.submitProcess = true;

        const url = `orders/${this.orderId}`;

        this.httpConfig.postRequest(url, this.order)
            .subscribe(
                res => {
                    this.toasterService.pop('success', '', 'Order was updated successfully');
                    this.getOrderDetailInfo();
                    this.router.navigate(['/admin/order', res.payload.id]);
                    this.submitProcess = false;
                },
                error => {
                    this.toasterService.pop('error', '', error);
                    this.submitProcess = false;
                });

    }

}
