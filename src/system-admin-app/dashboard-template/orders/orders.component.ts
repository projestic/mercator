import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'mrc-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

    constructor(private router: Router) {

    }

    public statuses: Array<string> = [
        'Offen',
        'Bereit',
        'Erfolgreich'
    ];

    public filter: Array<any> = [
        {
            name: 'Supplier',
            value: 'supplier'
        },
        {
            name: 'Company',
            value: 'company'
        },
        {
            name: 'Product',
            value: 'product'
        }
    ];

    public actions = {
        'click': e => {
            this.router.navigate(['/admin/order', e.row.id]);
        }
    };

    ngOnInit() {

    }
}
