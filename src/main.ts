import { environment } from './environments/environment';
import { mainSettings } from './main-settings/settings';
import { SystemAdminAppModule } from './system-admin-app/app.module';
import { PortalAppModule } from './portal-app/app.module';

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

if (environment.production) {
    enableProdMode();
}

if (mainSettings.project === 'system') {
    platformBrowserDynamic().bootstrapModule(SystemAdminAppModule)
        .catch(err => console.log(err));
} else if (mainSettings.project === 'portal') {
    platformBrowserDynamic().bootstrapModule(PortalAppModule)
        .catch(err => console.log(err));
};



