const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {AngularCompilerPlugin} = require('@ngtools/webpack');
const postcssPlugins = require('./post-css-plugin');

const config = {
    entry: {
        main: [
            './src\\main.ts'
        ],
        polyfills: [
            './src\\polyfills.ts'
        ],
        styles: [
            './src\\styles.scss'
        ]
    },
    output: {
        path: path.join(process.cwd(), 'system-admin-dist'),
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js',
        crossOriginLoading: false
    },
    module: {
        rules: [
            {
                "include": [
                    path.join(process.cwd(), "src\\styles.scss")
                ],
                "test": /\.scss$|\.sass$/,
                "use": [
                    "style-loader",
                    {
                        "loader": "raw-loader"
                    },
                    {
                        "loader": "postcss-loader",
                        "options": {
                            "ident": "embedded",
                            "plugins": postcssPlugins,
                            "sourceMap": true
                        }
                    },
                    {
                        "loader": "sass-loader",
                        "options": {
                            sourceMap: true,
                            precision: 8,
                            includePaths: []
                        }
                    }
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: './index.html',
            title: 'Mercator Systemadministrator'
        }),
        new AngularCompilerPlugin({
            mainPath: "main.ts",
            platform: 0,
            hostReplacementPaths: {
                "environments\\environment.ts": "environments\\environment.ts",
                "main-settings\\settings.ts": "main-settings\\settings.system.ts"
            },
            sourceMap: true,
            tsConfigPath: "src\\tsconfig.app.json",
            skipCodeGeneration: true,
            compilerOptions: {}
        })
    ],
    devServer: {
        historyApiFallback: true,
        proxy: {
            "/api": {
                "target": "http://mercatorapi.projestic.com/system-api/",
                "pathRewrite": {"^/api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': '123'
                },
            }
        }
    }
};

module.exports = config;
