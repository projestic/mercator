const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {AngularCompilerPlugin} = require('@ngtools/webpack');
const postcssPlugins = require('./post-css-plugin');

const config = {
    entry: {
        main: [
            './src\\main.ts'
        ],
        polyfills: [
            './src\\polyfills.ts'
        ],
        styles: [
            './src\\styles.scss'
        ]
    },
    output: {
        path: path.join(process.cwd(), 'portal-dist'),
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js',
        crossOriginLoading: false
    },
    module: {
        rules: [
            {
                "include": [
                    path.join(process.cwd(), "src\\styles.scss")
                ],
                "test": /\.scss$|\.sass$/,
                "use": [
                    "style-loader",
                    {
                        "loader": "raw-loader"
                    },
                    {
                        "loader": "postcss-loader",
                        "options": {
                            "ident": "embedded",
                            "plugins": postcssPlugins,
                            "sourceMap": true
                        }
                    },
                    {
                        "loader": "sass-loader",
                        "options": {
                            data: '$primary: #4a90e2; $color-primary: #4a90e2;',
                            sourceMap: true,
                            precision: 8,
                            includePaths: []
                        }
                    }
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: './index.html',
            hash: true,
            title: 'Mercator Portal',

        }),
        new AngularCompilerPlugin({
            mainPath: "main.ts",
            platform: 0,
            hostReplacementPaths: {
                "environments\\environment.ts": "environments\\environment.ts",
                "main-settings\\settings.ts": "main-settings\\settings.portal.ts"
            },
            sourceMap: true,
            tsConfigPath: "src\\tsconfig.app.json",
            skipCodeGeneration: true,
            compilerOptions: {}
        })
    ],
    devServer: {
        historyApiFallback: true,
        proxy: {
            "/api": {
                "target": "http://mercatorapi.projestic.com/portal-api/v1/",
                "pathRewrite": {"^/api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': 'c6ecd34ddebe40da93d33c9c77d6dccc'
                },
            },
            "/company-api": {
                "target": "http://mercatorapi.projestic.com/company-api/v1/",
                "pathRewrite": {"^/company-api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': 'c6ecd34ddebe40da93d33c9c77d6dccc'
                },
            },
            "/supplier-api": {
                "target": "http://mercatorapi.projestic.com/supplier-api/v1/",
                "pathRewrite": {"^/supplier-api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': 'c6ecd34ddebe40da93d33c9c77d6dccc'
                },
            },
            "/employee-api": {
                "target": "http://mercatorapi.projestic.com/employee-api/v1/",
                "pathRewrite": {"^/employee-api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': 'c6ecd34ddebe40da93d33c9c77d6dccc'
                },
            }
        }
    }
};

module.exports = config;
