const commonConfig = require( './webpack-config/webpack.common' );
const webpackMerge = require( 'webpack-merge' );

// No need for addons at this time.
module.exports = (env) => {
    const envConfig = require( `./webpack-config/webpack.${env.project}.js` );
    const mergedConfig = webpackMerge( commonConfig, envConfig );

    return mergedConfig;
};

