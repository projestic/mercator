# Mercator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Development server

####System Admin App

Run `npm run start:admin` or `npm run watch:admin` for a System Admin dev server.

####Portal app
Run `npm run start:portal` or `npm run watch:portal` for a Portal dev server.

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

*If you use `npm run watch:admin` or `npm run watch:portal` browser window will open automatically.*

####Webpack settings

Webpack settings are in the `webpack-config` folder.

#####System Admin api settings
```
devServer: {
         historyApiFallback: true,
         proxy: {
             "/api": {
                 "target": "http://mercatorapi.projestic.com/system-api/",
                 "pathRewrite": {"^/api": ""},
                 "secure": false,
                 "logLevel": "debug",
                 "changeOrigin": true,
                 "headers": {
                     'Application-Key': '123'
                 },
             }
         }
     }
```

#####Portal api settings
```
devServer: {
        historyApiFallback: true,
        proxy: {
            "/api": {
                "target": "http://mercatorapi.projestic.com/portal-api/v1/",
                "pathRewrite": {"^/api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': 'c6ecd34ddebe40da93d33c9c77d6dccc'
                },
            },
            "/company-api": {
                "target": "http://mercatorapi.projestic.com/company-api/v1/",
                "pathRewrite": {"^/company-api": ""},
                "secure": false,
                "logLevel": "debug",
                "changeOrigin": true,
                "headers": {
                    'Application-Key': 'c6ecd34ddebe40da93d33c9c77d6dccc'
                },
            }
        }
    }
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

####System Admin App
Run `npm run build:admin` to build the System Admin project.
The build artifacts will be stored in the `system-admin-dist/` directory. 

#####.htaccess

```
#Application key which need to add as "Application-Key" header to all requests to system api.
SetEnv APPLICATION_KEY 123

RewriteEngine  on

RewriteBase /

#Add "Application-Key" header if request uri starts with "/api/"
<If "%{REQUEST_URI} =~ m#^/api/(.*)$#">
    RequestHeader set Application-Key %{APPLICATION_KEY}e
</If>
RewriteRule ^api/(.*)$ http://mercatorapi.projestic.com/system-api/$1 [P,QSA,L]

# If an existing asset or directory is requested go to it as it is
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
RewriteRule ^ - [L]

# If the requested resource doesn't exist, use index.html
RewriteRule ^ /index.html
```

####Portal App
Run `npm run build:portal` to build the Portal project.
The build artifacts will be stored in the `portal-dist/` directory. 

#####.htaccess

```
#Application key which need to add as "Application-Key" header to all requests to system api.
SetEnv APPLICATION_KEY c6ecd34ddebe40da93d33c9c77d6dccc

RewriteEngine  on

RewriteBase /

#Add "Application-Key" header if request uri starts with "/api/"
<If "%{REQUEST_URI} =~ m#^/(?:company-api|api)/(.*)$#">
    RequestHeader set Application-Key %{APPLICATION_KEY}e
</If>

RewriteRule ^company-api/(.*)$ http://mercatorapi.projestic.com/company-api/v1/$1 [P,QSA,L]
RewriteRule ^api/(.*)$ http://mercatorapi.projestic.com/portal-api/v1/$1 [P,QSA,L]

# If an existing asset or directory is requested go to it as it is
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
RewriteRule ^ - [L]

# If the requested resource doesn't exist, use index.html
RewriteRule ^ /index.html
```

Use the `-prod` flag for a production build.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
